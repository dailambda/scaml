(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2022  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* My idea of new encoding *)

open Spotlib.Spot
open SCaml_michelson.Michelson

let int n =
  let rec f n =
    if n < 128 then 1
    else f (n lsr 7) + 1
  in
  f (abs n * 2 (* sign flag *))

(* variable *)
let rec length n =
  if n < 128 then 1
  else length (n lsr 7) + 1

let string_with_length s =
  let len = String.length s in
  len + length len

let list_length xs = length (List.length xs)

let z =
  let z128 = Z.of_int 128 in
  let z2 = Z.of_int 2 in
  fun z ->
    let rec f z =
      if Z.lt z z128 then 1
      else f (Z.div z z128) + 1
    in
    f (Z.mul (Z.abs z) z2 (* sign flag *))

let sum = List.fold_left (+) 0

let size_annots annots =
  let ns =
    List.filter_map (function
        | None -> None
        | Some s -> Some (String.length s + 1 (* '%', ':' *))) annots
  in
  match ns with
  | [] -> 0
  | _ ->
      let len =
        List.fold_left (+) 0 ns (* annotations *)
        + List.length ns - 1 (* separators *)
      in
      len + length len (* CHANGE *)

let tag9' xs annots =
  1 (* tag9 *)
  + 1 (* prim *)
  + list_length xs
  + sum xs
  + 1 (* string *) + let x = size_annots annots in if x = 0 then string_with_length "" else x

let xint n =
  if 0 <= n && n < 16 then 1 (* merged with the tag *)
  else 1 (* int *) + int n

let type_ t =
  let open Type in
  let rec f field {desc; tyannot; _} =
    match desc with
    | TyString
    | TyNat
    | TyInt
    | TyBytes
    | TyBool
    | TyUnit
    | TyMutez
    | TyKeyHash
    | TyTimestamp
    | TyAddress
    | TyChainID
    | TyKey
    | TySignature
    | TyOperation
    | TyNever
    | TyBLS12_381_Fr
    | TyBLS12_381_G1
    | TyBLS12_381_G2
      ->
        let sz_annots = size_annots [field; tyannot] in
        if sz_annots = 0 then
          1 (* CHANGED Prim and tyname merged *)
        else
          1 (* prim *) + 1 (* tyname *)
          + sz_annots
    | TyList t
    | TySet t
    | TyContract t
    | TyTicket t
      -> 1 (* prim *) + 1 (* tyname *)
         + f None t
         + size_annots [field; tyannot]
    | TyMap (t1, t2)
    | TyBigMap (t1, t2)
    | TyLambda (t1, t2)
      -> 1 (* prim *) + 1 (* tyname *)
         + f None t1
         + f None t2
         + size_annots [field; tyannot]
    | TySapling_state n
    | TySapling_transaction n
      -> 1 (* prim *) + 1 (* tyname *)
         + xint n (* CHANGED *)
         + size_annots [tyannot]
    | TyPair (f1,t1,f2,t2)
    | TyOr (f1, t1, f2, t2)
      -> 1 (* prim *) + 1 (* tyname *)
         + f f1 t1
         + f f2 t2
         + size_annots [field; tyannot]
    | TyPairN fts ->
        (* Pairs expanded:
           (pair t1 (pair t2 (pair t3 ..)))  :  2 * ntypes + types

           Contracted
           (pair t1 t2 t3 ..)  : 10 + types

           Contracted is less efficient when ntypes < 5.
        *)
        assert (List.length fts > 2);
        tag9'
          (List.map (fun (fld,t) -> f fld t) fts)
          [field; tyannot]
    | TyOption (f1, t)
      -> 1 (* prim *) + 1 (* tyname *)
         + f f1 t
         + size_annots [field; tyannot]
  in
  f t

let type_ = type_ None

let rec constant t =
  let open Constant in
  let rec f t =
    match t with
    | Unit
    | Bool _ ->
        1 (* prim *) + 1 (* constr *)
    | Int n ->
        begin try xint (Z.to_int n) (* CHANGED *) with _ -> 1 (* int *) + z n end
    | String s ->
        1 (* string *) + string_with_length s
    | Bytes s ->
        1 (* bytes *) +
        + let len =
            match String.sub s 0 2  with
            | "0x" -> String.length s / 2 - 1
            | _ | exception _ -> String.length s / 2
          in
          len + length len
    | Option (Some t) ->
        1 (* prim *) + 1 (* name *)
        + f t
    | Option None ->
        1 (* prim *) + 1 (* name *)
    | List ts
    | Set ts
      -> 1 (* seq *) + list_length ts (* CHANGE *) + sum (List.map f ts)
    | Map tts ->
        1 (* CHANGED map *) + list_length tts (* CHANGE *) + sum (List.map (fun (t1,t2) -> f t1 + f t2) tts)
    | Pair (t1, t2) ->
        1 (* prim *) + 1 (* tyname *)
        + f t1
        + f t2
    | PairN ts ->
        (* ??? PairN is less efficient when ntypes < 5. *)
        assert (List.length ts > 2);
        tag9' (List.map f ts) []
    | Left t
    | Right t
      -> 1 (* prim *) + 1 (* tyname *)
         + f t
    | Timestamp z ->
        (* XXX We print the timestamp in human readable form, which costs a lot *)
        let s =
          match Ptime.of_float_s @@ Z.to_float z with
          | None -> assert false
          | Some t -> Ptime.to_rfc3339 ~space:false ~frac_s:0 t
        in
        1 (* string *) + string_with_length s (* CHANGE *)
    | Code ops ->
        1 (* seq *) + list_length ops (* CHANGE *) + sum (List.map opcode ops)
  in
  f t

and opcode t =
  let open Opcode in
  let rec fs ts = sum (List.map f ts)
  and f t = g t
  and g t =
    match t.desc with
    | DROP
    | DUP
    | SWAP
    | CAR
    | CDR
    | APPLY
    | CONS
    | SOME
    | COMPARE
    | EQ
    | LT
    | LE
    | GT
    | GE
    | NEQ
    | ADD
    | SUB
    | SUB_MUTEZ
    | MUL
    | EDIV
    | ABS
    | ISNAT
    | NEG
    | LSL
    | LSR
    | AND
    | OR
    | XOR
    | NOT
    | EXEC
    | FAILWITH
    | UNIT
    | SIZE
    | MEM
    | UPDATE
    | CONCAT
    | GET
    | PACK
    | SLICE
    | TRANSFER_TOKENS
    | SET_DELEGATE
    | IMPLICIT_ACCOUNT
    | NOW
    | AMOUNT
    | BALANCE
    | CHECK_SIGNATURE
    | BLAKE2B
    | SHA256
    | SHA512
    | HASH_KEY
    | STEPS_TO_QUOTA
    | SOURCE
    | SENDER
    | ADDRESS
    | CHAIN_ID
    | INT
    | LEVEL
    | SELF_ADDRESS
    | UNPAIR
    | PAIRING_CHECK
    | NEVER
    | KECCAK
    | SHA3
    | TICKET
    | READ_TICKET
    | SPLIT_TICKET
    | JOIN_TICKETS
    | SAPLING_VERIFY_UPDATE
    | VOTING_POWER
    | TOTAL_VOTING_POWER
    | GET_AND_UPDATE
    | CREATE_ACCOUNT
      -> 1 (* CHANGE prim and op are merged *)
    | DIG n
    | DUG n
    | DROPn n
    | SAPLING_EMPTY_STATE n
    | GETn n
    | UPDATEn n
    | PAIRn n
    | UNPAIRn n
    | DUPn n
      -> 1 (* prim *) + 1 (* op *)
         + xint n (* CHANGED *)
    | BLOCK ts
      -> 1 (* seq *)
         + list_length ts (* CHANGE length *) + fs ts
    | ITER ts
    | MAP ts
    | LOOP ts
    | LOOP_LEFT ts
      -> 1 (* prim *) + 1 (* op *)
         + 1 (* seq *) + list_length ts (* CHANGE length *) + fs ts
    | IF (ts1, ts2)
    | IF_NONE (ts1, ts2)
    | IF_LEFT (ts1, ts2)
    | IF_CONS (ts1, ts2)
      -> 1 (* prim *) + 1 (* op *)
         + 1 (* seq *) + list_length ts1 (* CHANGE length *) + fs ts1
         + 1 (* seq *) + list_length ts2 (* CHANGE length *) + fs ts2
    | DIP ts ->
        1 (* prim *) + 1 (* op *)
        + 1 (* seq *) + list_length ts (* CHANGE length *) + fs ts
    | DIPn (n, ts) ->
        1 (* prim *) + 1 (* op *)
        + xint n (* CHANGED *)
        + 1 (* seq *) + list_length ts (* CHANGE length *) + fs ts
    | LEFT ty
    | RIGHT ty
    | NIL ty
    | NONE ty
    | EMPTY_SET ty
    | UNPACK ty
    | CAST ty
    | CONTRACT ty
      -> 1 (* prim *) + 1 (* op *)
         + type_ ty
    | LAMBDA (ty1, ty2, ts)
      ->
        (* it has 3 args *)
        tag9' [type_ ty1; type_ ty2; 1 (* seq *) + list_length ts (* CHANGE length *) + fs ts] []
    | PUSH (ty, c) ->
        1 (* prim *)
        + 1 (* op *)
        + type_ ty
        + constant c
    | EMPTY_MAP (ty1, ty2)
    | EMPTY_BIG_MAP (ty1, ty2)
      -> 1 (* prim *) + 1 (* op *)
         + type_ ty1
         + type_ ty2
    | PAIR ss ->
        1 (* prim *) + 1 (* op *)
        + if ss = [] then 0
        else
          let len =
            sum (List.map String.length ss) + List.length ss (* '%' *)
            + List.length ss - 1  (* separator *)
          in
          len + length len (* CHANGE *)
    | SELF None ->
        1 (* prim *) + 1 (* op *)
    | SELF (Some s) ->
        1 (* prim *) + 1 (* op *)
        + string_with_length ("%" ^ s) (* CHANGE *)
    | RENAME (Some s) ->
        1 (* prim *) + 1 (* op *)
        + string_with_length ("@" ^ s) (* CHANGE *)
    | RENAME None ->
        1 (* prim *) + 1 (* op *)
    | CONTRACT' (ty, s) ->
        1 (* prim *) + 1 (* op *)
        + type_ ty
        + string_with_length ("@" ^ s) (* CHANGE *)
    | VIEW (s, ty) ->
        1 (* prim *) + 1 (* op *)
        + 1 (* string *) + string_with_length s (* CHANGE *)
        + type_ ty
    | CREATE_CONTRACT m ->
        1 (* prim *) +  1 (* op *)
        + module_ m
  in
  f t

and module_ m =
  let open Module in
  1 (* seq *) + length (3 + List.length m.views) (* length *)
  + 2 (* parameter *) + type_ m.parameter
  + 2 (* storage *) + type_ m.storage
  + 2 (* code *) + 1 (* seq *) + list_length m.code (* CHANGE *) + List.fold_left (+) 0 (List.map opcode m.code)
  + List.fold_left (+) 0
    (List.map (fun v ->
         (* it has 4 args *)
         tag9' [ string_with_length v.View.name (* CHANGE *);
                 type_ v.ty_param;
                 type_ v.ty_return;
                 1 (* seq *) + list_length v.code (* CHANGE length *) + List.fold_left (+) 0 (List.map opcode v.code) ] []) m.views)
