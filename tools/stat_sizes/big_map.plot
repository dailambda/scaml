set terminal png
set output '_data/big_map.png'
set key left top
set xlabel "original (bytes)"
set ylabel "optimized (bytes)"
f(x)=a*x
set title "big map optimization"
fit f(x) "_data/big_map.data" u 5:11 via a
plot "_data/big_map.data" using 5:11 title "size", \
     x title "100%", \
     f(x) title "82.98%"

