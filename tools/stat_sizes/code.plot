set terminal png
set output '_data/code.png'
set key left top
set xlabel "original (bytes)"
set ylabel "optimized (bytes)"
f(x)=a*x
set title "code optimization"
fit f(x) "_data/code.data" u 3:9 via a
plot "_data/code.data" using 3:9 title "size", \
     x title "100%", \
     f(x) title "60.21%"

