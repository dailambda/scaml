open SCaml_tools
open SCaml_michelson
open New_size

(*
   big_map/1234
   big_map/1234.type
*)

let enc =
  Tezos_micheline.Micheline_encoding.canonical_encoding ~variant:"dummy"
    Michelson_v1_primitives.prim_encoding

let f fn =
  Format.printf "%s@." fn;
  let fn_type = fn ^ ".type" in
  let s = from_Ok @@ File.to_string fn_type in
  let m = from_Ok @@ Micheline.Parse.parse_expression_string ~check_indentation:false s in
  let m' = Micheline.Parse.of_parsed m in
  Format.eprintf "type: %a@." Micheline.pp m';
  let ty = from_Ok @@ Michelson.Type.of_micheline m in
  let s = from_Ok @@ Spotlib.File.to_string fn in
  let parsed =
    match Micheline.Parse.parse_expression_string s with
    | Ok parsed -> parsed
    | Error etrace ->
        Format.eprintf "%a@." Tzerror.pp etrace;
        assert false
  in
  let c =
    let ty = Michelson.Type.tyList ty in
    match Michelson.Constant.of_micheline ty parsed with
    | Ok c -> c
    | Error (_loc, s) -> prerr_endline s; assert false
  in
  let len, cs =
    match c with
    | List cs -> List.length cs, cs
    | _ -> assert false
  in
  Format.printf "%s values: %d size: %d size2: %d size3: %d size4: %d@."
    fn
    len
    (List.fold_left (fun a c -> a + Size.constant c) 0 cs)
    (List.fold_left (fun a c -> a + New_size2.constant c) 0 cs)
    (List.fold_left (fun a c -> a + New_size3.constant c) 0 cs)
    (List.fold_left (fun a c -> a + New_size4.constant c) 0 cs)

let () =
  let rec loop = function
    | x when x = 167239 -> ()
    | x ->
        let fn = Printf.sprintf "_big_map/%d" x in
        if Sys.file_exists fn then f fn;
        loop (x+1)
  in
  loop 0
