open SCaml_michelson
open New_size

let () =
  match Michelson.Module.parse_file ~check_indentation:false Sys.argv.(1) with
  | Error e ->
      Format.eprintf "Error: %a@." Tzerror.pp e;
      exit 1
  | Ok m ->
      Format.eprintf "%s: %d %d %d %d@." Sys.argv.(1) (Size.module_ m) (New_size2.module_ m) (New_size3.module_ m) (New_size4.module_ m)
