open Js_of_ocaml
open Js_of_ocaml_lwt
open Lwt.Syntax
open Jstools
open SCaml_michelson
module M = Monaco_jsoo

let parse s =
  match Michelson.Module.parse_string ~check_indentation:true s with
  | Ok m -> Ok (m, None)
  | Error es ->
      (* Retry ignoring indentation errors *)
      match Michelson.Module.parse_string ~check_indentation:false s with
      | Ok m -> Ok (m, Some es)
      | Error e -> Error e

let parser_errors es =
  List.filter_map (function
      | Michelson.Parser_error (loc, s) ->
          Some ( (loc.start.line, loc.start.column, loc.stop.line, loc.stop.column) , s)
      | _ -> None) es

module Worker_ = struct
  let logger s = Worker.post_message @@ `Log s

  let () = SCaml_tools.Debug.set_by_pattern "michelson.optimize"

  let onmessage (s, multiplier) =
    match parse s with
    | Error es ->
        Worker.post_message
          (`Error (Format.kasprintf (fun s -> s) "%a" Tzerror.pp es))
    | Ok (m, _) ->
        let module Optimize =
            SCaml_michelson.Optimize.Make(struct
              let logger = logger
              let do_dijkstra = false
              let search_space_multiplier = multiplier
              let use_lcs_for_astar_score = true
            end)
        in
        (* We have an issue of receiving Z.t from the worker thread.
           To avoid the issue, we text it *)
        logger (Printf.sprintf "Optimizing... (multiplier %f)" multiplier);
        let t1 = (new%js Js.date_now)##getTime in
        let m' = Optimize.module_ m in
        let t2 = (new%js Js.date_now)##getTime in
        Format.kasprintf
          (fun s -> Worker.post_message (`Done (t2 -. t1, s)))
          "%a" Michelson.Module.pp m'
end

let go_and_stop
    ~button (* button to go and stop the job *)
    ~f_ready (* called when the state is ready to go *)
    ~text_go (* button text for "ready to go" *)
    ~text_stop (* button text for "stop the current job" *)
    ~f_go (* function to return a worker and the initial message to trigger the start *)
    ~f_step (* callback to handle the message from the job worker *)
    ~f_stop (* callback called when the worker is stopped *)
  =
  let rec ready () =
    button##.innerText := text_go;
    button##.onclick := Html.handler go;
    f_ready ()
  and go _ev =
    (match f_go () with
    | None -> ready ()
    | Some (w, s) ->
        button##.innerText := text_stop;
        button##.onclick := Html.handler (stop w);
        w##.onmessage := Html.handler (fun ev ->
            (match f_step ev##.data with
             | `Continue -> ()
             | `Terminate ->
                 w##terminate; (* w may already exit but we do not care *)
                 ready ());
            Js._false);
        w##postMessage s);
    Js._false;
  and stop w _ev =
    w##terminate;
    f_stop ();
    ready ();
    Js._false;
  in
  ready ()

let init_language () =
  let open M.Languages in
  let lang = register "michelson" in
  let+ frame = XmlHttpRequest.get "./token_provider.js" in
  (match frame.code with
   | 200 ->
       setMonarchTokensProvider lang
         (Js.Unsafe.inject
            (Js.Unsafe.eval_string ("(" ^ frame.content ^ ")")))
   | _ ->
       Console.log "failed to load token_provider.js");
  lang

let run ex =
  let+ lang = init_language () in
  let control = getById Html.CoerceTo.div "control" in
  let input =
    let input = getById Html.CoerceTo.div "input" in
    M.Editor.(create input Option.[|language lang;
                                    theme "vs";
                                    automaticLayout true;
                                    value ex |])
  in
  let input_model = M.Editor.getModel input in
  let output =
    let output = getById Html.CoerceTo.div "output" in
    M.Editor.(create output Option.[|language lang;
                                     theme "vs";
                                     readOnly true;
                                     automaticLayout true |])
  in
  let output_model = M.Editor.getModel output in
  let log = getById Html.CoerceTo.textarea "log" in
  let clear_output () = M.TextModel.setValue output_model !$"" in
  let clear_log () =
    log##.value := !$ "";
    log##.scrollTop := 0;
  in
  let logf fmt =
    Format.kasprintf (fun s ->
        let js = log##.value in
        log##.value := js##concat (!$ s);
        log##.scrollTop := log##.scrollHeight
      ) fmt
  in
  let printf fmt =
    Format.kasprintf (fun s ->
        let js = M.TextModel.getValue output_model in
        M.TextModel.setValue output_model (js##concat (!$ s))
      ) fmt
  in
  let number_multiplier = getById Html.CoerceTo.input "number-multiplier" in
  let button = Html.document##createElement !$"button" in

  let mref = ref (None : Michelson.Module.t option) in
  let input_decorations = ref [||] in

  let set_readOnly b =  M.Editor.(updateOptions input Option.[| readOnly b |]) in

  let f_ready () = set_readOnly false in

  let f_go () =
    set_readOnly true;
    let multiplier =
      let n =
        try Float.of_string (Js.to_string number_multiplier##.value)
        with _ -> 1.0
      in
      if n < 0.1 then 0.1 else if n > 100.0 then 100.0 else n
    in
    clear_output ();
    clear_log ();
    let logf fmt =
      Format.kasprintf (fun s ->
          let js = log##.value in
          log##.value := js##concat (!$ s)
        ) fmt
    in
    let s = Js.to_string @@ M.TextModel.getValue input_model in
    input_decorations := M.Editor.deltaDecorations
        ~old: !input_decorations
        ~new_: [| |]
        input;
    match parse s with
    | Error es ->
        logf "%a" Tzerror.pp es;
        let es = parser_errors es in
        let ds = Array.of_list @@ List.map (fun ((sl,sc,el,ec),_) ->
            Console.log (sl,sc,el,ec);
            M.Decoration.(deltaDecoration
                            (M.Range.create sl (sc+1) el (ec+1))
                            [| Option.inlineClassName "error_location" |])) es
        in
        input_decorations := M.Editor.deltaDecorations
            ~old: [||]
            ~new_: ds
            input;
        None
    | Ok (m, indent_errors) ->
        (match indent_errors with
         | None -> ()
         | Some es ->
             logf "@[<2>Ignoring indentation error:@ %a@]" Tzerror.pp es);
        mref := Some m;
        (* We must initialize a worker for each time *)
        let w = Worker.create "./optz.bc.js" in
        Some (w, (s, multiplier))
  in

  let f_step = function
    | `Log s -> logf "%s@." s; `Continue
    | `Error mes ->
        logf "%s" mes;
        `Terminate
    | `Done (msecs, m') ->
        match parse m' with
        | Error e ->
            logf "%a" Tzerror.pp e;
            `Terminate
        | Ok (m', _) ->
            clear_output ();
            let sz = Size.module_ (match !mref with Some m -> m | None -> assert false) in
            let sz' = Size.module_ m' in
            logf "Optimized in %.2f@." (msecs /. 1000.);
            logf "Code size change: %d => %d@." sz sz';
            printf "%a@." Michelson.Module.pp m';
            `Terminate
  in

  let f_stop () = logf "Aborted@." in

  go_and_stop
    ~button
    ~f_ready
    ~text_go:!$"Optimize"
    ~text_stop:!$"Abort"
    ~f_go
    ~f_step
    ~f_stop;

  Dom.appendChild control button;
  logf "Javascript module loaded."

let () =
  if is_worker () then
    Worker.set_onmessage Worker_.onmessage
  else
    ignore (
      let+ frame = XmlHttpRequest.get "./example.tz" in
      let ex =
        match frame.code with
        | 200 -> frame.content
        | _ -> ""
      in
      M.Monaco.run "vs" (fun () -> ignore @@ run ex)
    )
