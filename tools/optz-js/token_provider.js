{

    // Set defaultToken to invalid to see what you do not tokenize yet
    // defaultToken: 'invalid',

    keywords: [
	'parameter', 'storage', 'code', 'view'
    ],

//  operators: [
//    '=', '>', '<', '<=', '>=', '=>', '+', '-', '*', '/',
//  ],
//
//  builtins: [
//    'mod', 'div', 'rem', '^', 'to_real', 'and', 'or', 'not', 'distinct',
//    'to_int', 'is_int', '~', 'xor', 'if', 'ite', 'true', 'false', 'root-obj',
//    'sat', 'unsat', 'const', 'map', 'store', 'select', 'sat', 'unsat',
//    'bit1', 'bit0', 'bvneg', 'bvadd', 'bvsub', 'bvmul', 'bvsdiv', 'bvudiv', 'bvsrem',
//    'bvurem', 'bvsmod',  'bvule', 'bvsle', 'bvuge', 'bvsge', 'bvult',
//    'bvslt', 'bvugt', 'bvsgt', 'bvand', 'bvor', 'bvnot', 'bvxor', 'bvnand',
//    'bvnor', 'bvxnor', 'concat', 'sign_extend', 'zero_extend', 'extract',
//    'repeat', 'bvredor', 'bvredand', 'bvcomp', 'bvshl', 'bvlshr', 'bvashr',
//    'rotate_left', 'rotate_right', 'get-assertions'
//  ],

    brackets: [
	['(',')','delimiter.parenthesis'],
	['{','}','delimiter.curly'],
	['[',']','delimiter.square']
    ],

    // we include these common regular expressions
    symbols:  /[=><~&|+\-*\/%@#]+/,

    // C# style strings
    escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,

    // The main tokenizer for our languages
    tokenizer: {
	root: [
	    [/0x[0-9a-fA-F]+/, 'number'],
	    [/[0-9]+/, 'number'],

	    // [/[@:$&%!?][a-zA-Z0-9_.%@]+/, 'keyword'],
	    [/[%@:&%!?][a-zA-Z_0-9]+/, 'variable'],

	    [/Elt/, 'keyword'],

	    [/#.*$/, 'comment'],

	    // identifiers and keywords
	    [/[a-z][\w\-\.']*/, { cases: { '@keywords': 'keyword',
					   '@default': 'type' } }],

            [/"([^"\\]|\\.)*$/, 'string.invalid' ],  // non-teminated string
	    [/"/,  { token: 'string.quote', bracket: '@open', next: '@string' } ],

	],

	string: [
	    [/[^\\"]+/,  'string'],
	    [/@escapes/, 'string.escape'],
	    [/\\./,      'string.escape.invalid'],
	    [/"/,        { token: 'string.quote', bracket: '@close', next: '@pop' } ]
	],

	whitespace: [
	    [/[ \t\r\n]+/, 'white'],
	    [/#.*$/,    'comment'],
	],
    }
}
