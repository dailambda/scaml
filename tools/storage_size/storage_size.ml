open SCaml_tools
open SCaml_michelson
open New_size

let () =
  let m = from_Ok & Michelson.Module.parse_file ~check_indentation:false Sys.argv.(1) in
  let c =
    match Michelson.Constant.parse_file ~check_indentation:false m.storage Sys.argv.(2) with
    | Ok c -> c
    | Error es ->
        Format.eprintf "%a@." Tzerror.pp es;
        assert false
  in
  Format.printf "%s: %d %d %d %d@." Sys.argv.(2) (Size.constant c) (New_size2.constant c) (New_size3.constant c) (New_size4.constant c);
(*
  let bms = Michelson.Constant.big_map_ids m.storage c in
  List.iter (fun (z,(ty1,ty2)) ->
      Format.eprintf "big_map %d : @[%a@] -> @[%a@]@."
        (Z.to_int z)
        Michelson.Type.pp ty1
        Michelson.Type.pp ty2) bms;
   *)
  ()
