(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

module Compile_common = struct
  open Misc
  open Compenv

  type info = {
    source_file : string;
    module_name : string;
    output_prefix : string;
    env : Env.t;
    ppf_dump : Format.formatter;
    tool_name : string;
    native : bool;
  }

  let cmx i = i.output_prefix ^ ".cmx"
  let obj i = i.output_prefix ^ Config.ext_obj
  let cmo i = i.output_prefix ^ ".cmo"
  let annot i = i.output_prefix ^ ".annot"

  let with_info ~native ~tool_name ~source_file ~output_prefix ~dump_ext k =
    Compmisc.init_path () ;
    let module_name = module_of_filename source_file output_prefix in
    Env.set_unit_name module_name ;
    let env = Compmisc.initial_env () in
    let dump_file = String.concat "." [output_prefix; dump_ext] in
    Compmisc.with_ppf_dump ~file_prefix:dump_file @@ fun ppf_dump ->
    k
      {
        module_name;
        output_prefix;
        env;
        source_file;
        ppf_dump;
        tool_name;
        native;
      }

  (** Compile a .mli file *)

  let parse_intf i =
    Pparse.parse_interface ~tool_name:i.tool_name i.source_file
    |> print_if i.ppf_dump Clflags.dump_parsetree Printast.interface
    |> print_if i.ppf_dump Clflags.dump_source Pprintast.signature

  let typecheck_intf info ast =
    Profile.(record_call typing) @@ fun () ->
    let tsg =
      ast
      |> Typemod.type_interface info.env
      |> print_if info.ppf_dump Clflags.dump_typedtree Printtyped.interface
    in
    let sg = tsg.Typedtree.sig_type in
    if !Clflags.print_types then
      Printtyp.wrap_printing_env ~error:false info.env (fun () ->
          Format.(fprintf std_formatter)
            "%a@."
            (Printtyp.printed_signature info.source_file)
            sg) ;
    ignore (Includemod.signatures info.env ~mark:Mark_both sg sg) ;
    Typecore.force_delayed_checks () ;
    Warnings.check_fatal () ;
    tsg

  let emit_signature info ast tsg =
    let sg =
      let alerts = Builtin_attributes.alerts_of_sig ast in
      Env.save_signature
        ~alerts
        tsg.Typedtree.sig_type
        info.module_name
        (info.output_prefix ^ ".cmi")
    in
    Typemod.save_signature
      info.module_name
      tsg
      info.output_prefix
      info.source_file
      info.env
      sg

  let interface info =
    Profile.record_call info.source_file @@ fun () ->
    let ast = parse_intf info in
    if Clflags.(should_stop_after Compiler_pass.Parsing) then ()
    else
      let tsg = typecheck_intf info ast in
      if not !Clflags.print_types then emit_signature info ast tsg

  (** Frontend for a .ml file *)

  let parse_impl i =
    Pparse.parse_implementation ~tool_name:i.tool_name i.source_file
    |> print_if i.ppf_dump Clflags.dump_parsetree Printast.implementation
    |> print_if i.ppf_dump Clflags.dump_source Pprintast.structure

  let typecheck_impl i parsetree =
    parsetree
    |> Profile.(record typing)
         (Typemod.type_implementation
            i.source_file
            i.output_prefix
            i.module_name
            i.env)
    |> print_if
         i.ppf_dump
         Clflags.dump_typedtree
         Printtyped.implementation_with_coercion

  let implementation info ~backend =
    Profile.record_call info.source_file @@ fun () ->
    let exceptionally () =
      let sufs = if info.native then [cmx; obj] else [cmo] in
      List.iter (fun suf -> remove_file (suf info)) sufs
    in
    Misc.try_finally ?always:None ~exceptionally (fun () ->
        let parsed = parse_impl info in

        (* SCaml special handling *)
        let m = SCamlc.Preprocess.mapper in
        let parsed = m.Ast_mapper.structure m parsed in

        (if Clflags.(should_stop_after Compiler_pass.Parsing) then ()
        else
          let typed = typecheck_impl info parsed in
          if Clflags.(should_stop_after Compiler_pass.Typing) then ()
          else backend info typed) ;
        Warnings.check_fatal ())
end

module Compile = struct
open Compile_common

let tool_name = "scamlc"

let with_info =
  Compile_common.with_info ~native:false ~tool_name

let interface ~source_file:_ ~output_prefix:_ = assert false

let implementation ~start_from ~source_file ~output_prefix =
  let backend info typed =
    SCamlc.SCamlComp.compile info.source_file info.output_prefix info.module_name typed
  in
  with_info ~source_file ~output_prefix ~dump_ext:"cmo" @@ fun info ->
  match (start_from : Clflags.Compiler_pass.t) with
  | Parsing -> Compile_common.implementation info ~backend
  | _ -> Misc.fatal_errorf "Cannot start from %s"
           (Clflags.Compiler_pass.to_string start_from)
end

let scaml_conf_opt = ref SCamlc.Conf.none

let scaml_options =
  let open SCamlc in
  [ ( "--scaml-debug",
      Arg.String (fun s -> SCaml_tools.Debug.set_by_pattern s),
      "Set debug switches" );
    ( "--scaml-debug-list",
      Arg.Unit (fun () ->
          List.iter (fun (ns,_) ->
              print_endline (String.concat "." ns))
            (SCaml_tools.Debug.registered ());
          exit 0),
      "List registered debug switches" );
    ( "--scaml-convert",
      Arg.Unit (fun () -> Conf.(scaml_conf_opt := {!scaml_conf_opt with op_mode = Some ConvertAll})),
      "Convert types and values, instead of compling a smart contract" );
    ( "--scaml-convert-value",
      Arg.String
        (fun s ->
          Conf.(scaml_conf_opt := {!scaml_conf_opt with op_mode = Some (ConvertSingleValue s)})),
      "<ident> Convert a single value, instead of compling a smart contract" );
    ( "--scaml-convert-type",
      Arg.String
        (fun s ->
          Conf.(scaml_conf_opt := {!scaml_conf_opt with op_mode = Some (ConvertSingleType s)})),
      "<ident> Convert a single type, instead of compling a smart contract" );
    ( "--scaml-revert",
      Arg.String
        (fun s -> Conf.(scaml_conf_opt := {!scaml_conf_opt with op_mode = Some (Revert s)})),
      "<string> Revert values, instead of compling a smart contract" );
    ( "--scaml-noscamlib",
      Arg.Unit (fun () -> Conf.(scaml_conf_opt := {!scaml_conf_opt with op_noscamlib = Some true})),
      "Do not add default directory for SCamlib to the list of include \
       directories" );
    ( "--scaml-version",
      Arg.Unit
        (fun () ->
          print_string Version.scaml ;
          print_newline () ;
          exit 0),
      "Print SCaml version and exit" );
    ( "--scaml-dump-iml",
      Arg.Unit (fun () -> Conf.(scaml_conf_opt := {!scaml_conf_opt with op_dump_iml = Some true})),
      "Dump the final IML code to .iml file" );
    ( "--scaml-optimize",
      Arg.Bool
        (fun b -> Conf.(scaml_conf_opt := {!scaml_conf_opt with op_iml_optimization = Some b})),
      "<bool> Turn on/off the ML level optimization.  Default is true.  \
       False may break some compilation." );
    ( "--scaml-michelson-optimize",
      Arg.Bool
        (fun b ->
          Conf.(scaml_conf_opt := {!scaml_conf_opt with op_michelson_optimization = Some b})),
      "<bool> Turn on/off the Michelson level optimization.  Default is true."
    );
    ( "--scaml-michelson-optimize-do-dijkstra",
      Arg.Bool
        (fun b ->
          Conf.(scaml_conf_opt := {!scaml_conf_opt with op_michelson_optimization_do_dijkstra = Some b})),
      "<bool> Turn on/off slow Dijkstra search in Michleson optimization.  Default is false."
    );
    ( "--scaml-michelson-optimize-search-space-multiplier",
      Arg.Float
        (fun f ->
          Conf.(scaml_conf_opt := {!scaml_conf_opt with op_michelson_optimization_search_space_multiplier = Some f})),
      "<float> Tweak Michelson optimization search space limit.  Default is 1.0 ."
    );
    ( "--scaml-protocol",
      Arg.String
        (fun s ->
          match Protocol.parse s with
          | Ok v -> Conf.(scaml_conf_opt := {!scaml_conf_opt with op_protocol = Some v})
          | Error s -> failwith s),
      Printf.sprintf
        "<string> Set Tezos protocol version (default: %s)"
        Protocol.(to_string default) );
  ]

module Maindriver = struct
open Clflags

let usage = "Usage: ocamlc <options> <files>\nOptions are:"

module Options = Main_args.Make_bytecomp_options (Main_args.Default.Main)

let main argv ppf =
  Clflags.add_arguments __LOC__ Options.list;
  Clflags.add_arguments __LOC__
    ["-depend", Arg.Unit Makedepend.main_from_option,
     "<options> Compute dependencies (use 'ocamlc -depend -help' for details)"];
  Clflags.add_arguments __LOC__ scaml_options;
  match
    Compenv.readenv ppf Before_args;
    Compenv.parse_arguments (ref argv) Compenv.anonymous usage;
    SCamlc.Conf.with_opt !scaml_conf_opt @@ fun () ->
    SCaml_tools.Debug.set_by_env ();
    SCamlc.Scamlib.init () ;
    Compmisc.read_clflags_from_env ();
    if !Clflags.plugin then
      Compenv.fatal "-plugin is only supported up to OCaml 4.08.0";
    (* SCaml Micheline printer *)
    SCaml_michelson.Micheline.use_pp_custom := true;
    begin try
      Compenv.process_deferred_actions
        (ppf,
         Compile.implementation,
         Compile.interface,
         ".cmo",
         ".cma");
    with Arg.Bad msg ->
      begin
        prerr_endline msg;
        Clflags.print_arguments usage;
        exit 2
      end
    end;
    Compenv.readenv ppf Before_link;
    if
      List.length
        (List.filter (fun x -> !x)
           [make_archive;make_package;Compenv.stop_early;output_c_object])
        > 1
    then begin
      let module P = Clflags.Compiler_pass in
      match !stop_after with
      | None ->
          Compenv.fatal
            "Please specify at most one of -pack, -a, -c, -output-obj";
      | Some ((P.Parsing | P.Typing) as p) ->
        assert (P.is_compilation_pass p);
        Printf.ksprintf Compenv.fatal
          "Options -i and -stop-after (%s) \
           are  incompatible with -pack, -a, -output-obj"
          (String.concat "|"
             (P.available_pass_names ~filter:(fun _ -> true) ~native:false))
      | Some (P.Scheduling | P.Emit) -> assert false (* native only *)
    end;
    if !make_archive then assert false
    else if !make_package then assert false
    else if not !Compenv.stop_early && !objfiles <> [] then begin
      let translated = SCamlc.SCamlComp.get_translated_modules () in
      (* --scaml-convert and --scaml-revert do not produce compiled modules *)
      if translated <> [] then begin
        Format.eprintf
          "Linking %s@."
          (String.concat
             ", "
             (List.map (fun x -> x.SCamlc.SCamlComp.Module.name) translated)) ;
        let outputdir =
          match !Clflags.output_name with
          | None -> "."
          | Some x -> Filename.dirname x
        in
        SCamlc.SCamlComp.link outputdir translated
      end
      else Format.eprintf "Nothing to link...@." ;
      Warnings.check_fatal ()
    end;
  with
  | exception (Compenv.Exit_with_status n) ->
    n
  | exception x ->
    Location.report_exception ppf x;
    2
  | () ->
    Profile.print Format.std_formatter !Clflags.profile_columns;
    0
end

module Main = struct
let () =
  exit (Maindriver.main Sys.argv Format.err_formatter)
end
