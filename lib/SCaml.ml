(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2019  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* The actual implementation of these API functions are given in
   primitives.ml as Michelson code.

   This .ml file gives OCaml semantics of SCaml primitives.
*)

open Typerep_lib.Std_internal

type ocaml_int = int
type ocaml_string = string

type 'a literal = 'a [@@deriving typerep]

module Internal = struct
  module Z = struct
    include Z

    include Typerep_lib.Type_abstract.Make0(struct
        type nonrec t = t
        let name = "z"
      end)
  end

  module Set = struct
    include PSet

    include Typerep_lib.Type_abstract.Make1(struct
        type nonrec 'a t = 'a t
        let name = "set"
      end)
  end

  module Map = struct
    include PMap

    include Typerep_lib.Type_abstract.Make2(struct
        type nonrec ('a, 'b) t = ('a, 'b) t
        let name = "map"
      end)
  end
end

type nat = ZNat of Internal.Z.t [@@deriving typerep]

type int = ZInt of Internal.Z.t [@@deriving typerep]

type tz = ZMutez of Internal.Z.t [@@deriving typerep]

type nonrec 'a option = 'a option =
  | None
  | Some of 'a
[@@deriving typerep]

module Option = struct
  type 'a t = 'a option [@@deriving typerep]

  let value o a = match o with Some a -> a | None -> a

  let get = function Some a -> a | None -> invalid_arg "Option.get"

  let map f = function Some a -> Some (f a) | None -> None
end

type ('a, 'b) sum = Left of 'a | Right of 'b [@@deriving typerep]

module Sum = struct
  type ('a, 'b) t = ('a, 'b) sum = Left of 'a | Right of 'b
  [@@deriving typerep]

  let get_left = function Left a -> a | _ -> invalid_arg "Sum.get_left"

  let get_right = function Right a -> a | _ -> invalid_arg "Sum.get_right"
end

(* So far we use int instead of Z *)

exception Overflow
exception Negative

let z_of_int (ZInt z) = z
let int_of_z z = ZInt z

let z_of_nat (ZNat z) = z
let nat_of_z z = ZNat z

let bin_int op a b = int_of_z (op (z_of_int a) (z_of_int b))
let bin_nat op a b = nat_of_z (op (z_of_nat a) (z_of_nat b))

let mutez_of_tz (ZMutez z) = z

let tz_of_mutez =
  let max_mutez = Z.of_int64 Int64.max_int in
  fun z ->
    if z < Z.zero then raise Negative;
    if max_mutez < z then raise Overflow;
    ZMutez z

let tz_of_mutez_opt =
  let max_mutez = Z.of_int64 Int64.max_int in
  fun z ->
    if z < Z.zero then raise Negative;
    if max_mutez < z then raise Overflow;
    Some (ZMutez z)

let bin_tz op a b = tz_of_mutez (op (mutez_of_tz a) (mutez_of_tz b))

let ( + ) = bin_int Z.( + )
let ( +^ ) = bin_nat Z.( + )
let ( +$ ) = bin_tz Z.( + )

let ( - ) = bin_int Z.( - )
let ( -^ ) a b = int_of_z Z.(z_of_nat a - z_of_nat b)
let ( -$ ) a b = tz_of_mutez Z.(mutez_of_tz a - mutez_of_tz b)
let ( -$? ) a b = tz_of_mutez_opt Z.(mutez_of_tz a - mutez_of_tz b)

let ( * ) = bin_int Z.( * )
let ( *^ ) = bin_nat Z.( * )
let ( *$ ) a b = tz_of_mutez Z.(mutez_of_tz a * z_of_nat b)

let ( ~- ) a = int_of_z Z.(~-(z_of_int a))
let ( ~-^ ) a = int_of_z Z.(~-(z_of_nat a))

exception Division_by_zero

let ediv_int_int a b =
  if b = ZInt Z.zero then None
  else
    let z1, z2 = Z.ediv_rem (z_of_int a) (z_of_int b) in
    Some (int_of_z z1, nat_of_z z2)

let ediv_int_nat a b =
  if b = ZNat Z.zero then None
  else
    let z1, z2 = Z.ediv_rem (z_of_int a) (z_of_nat b) in
    Some (int_of_z z1, nat_of_z z2)

let ediv_nat_int a b =
  if b = ZInt Z.zero then None
  else
    let z1, z2 = Z.ediv_rem (z_of_nat a) (z_of_int b) in
    Some (int_of_z z1, nat_of_z z2)

let ediv_nat_nat a b =
  if b = ZNat Z.zero then None
  else
    let z1, z2 = Z.ediv_rem (z_of_nat a) (z_of_nat b) in
    Some (nat_of_z z1, nat_of_z z2)

let ediv_tz_tz a b =
  if b = ZMutez Z.zero then None
  else
    let z1, z2 = Z.ediv_rem (mutez_of_tz a) (mutez_of_tz b) in
    Some (nat_of_z z1, tz_of_mutez z2)

let ediv_tz_nat a b =
  if b = ZNat Z.zero then None
  else
    let z1, z2 = Z.ediv_rem (mutez_of_tz a) (z_of_nat b) in
    Some (tz_of_mutez z1, tz_of_mutez z2)

let ediv_rem_int = ediv_int_int
let ediv_rem_nat = ediv_nat_nat
let ediv_rem_tz = ediv_tz_tz
let ediv_rem_tz_nat = ediv_tz_nat

let ( / ) a b =
  match ediv_int_int a b with
  | None -> raise Division_by_zero
  | Some (a, _) -> a

let ( // ) = ( / )

let ( /^ ) a b =
  match ediv_nat_nat a b with
  | None -> raise Division_by_zero
  | Some (a, _) -> a

let ( /$ ) a b =
  match ediv_tz_tz a b with None -> raise Division_by_zero | Some (a, _) -> a

let ( /$^ ) a b =
  match ediv_tz_nat a b with None -> raise Division_by_zero | Some (a, _) -> a

let ( %% ) a b =
  match ediv_rem_int a b with
  | None -> raise Division_by_zero
  | Some (_, r) -> r

let ( mod ) = ( %% )

let ( %^ ) a b =
  match ediv_rem_nat a b with
  | None -> raise Division_by_zero
  | Some (_, r) -> r

let ( %$ ) a b =
  match ediv_rem_tz a b with
  | None -> raise Division_by_zero
  | Some (_, r) -> r

let ( %$^ ) a b =
  match ediv_rem_tz_nat a b with
  | None -> raise Division_by_zero
  | Some (_, r) -> r

let ( lsl ) n1 (ZNat i2) = nat_of_z Z.(z_of_nat n1 lsl (Z.to_int i2))
let ( lsr ) n1 (ZNat i2) = nat_of_z Z.(z_of_nat n1 asr (Z.to_int i2)) (* XXX correct? *)

let ( lor ) n1 n2 = nat_of_z Z.(z_of_nat n1 lor z_of_nat n2)
let ( land ) n1 n2 = nat_of_z Z.(z_of_nat n1 land z_of_nat n2)
let land_int_nat i1 n2 = nat_of_z Z.(z_of_int i1 land z_of_nat n2)
let ( lxor ) n1 n2 = nat_of_z Z.(z_of_nat n1 lxor z_of_nat n2)
let lnot_nat n1 = int_of_z Z.(~!(z_of_nat n1))
let lnot i1 = int_of_z Z.(~!(z_of_int i1))

let abs a = nat_of_z (Z.abs (z_of_int a))
let isnat (ZInt i as a) = if i < Z.zero then None else Some (abs a)
let int_of_nat (ZNat z) = ZInt z

let nat_of_int (ZInt i as a) = if i < Z.zero then raise Negative else abs a

let mutez_of_nat (ZNat n) = ZMutez n
let mutez_of_int (ZInt i) = if i < Z.zero then raise Negative else ZMutez i

let int_of_mutez (ZMutez x) = ZInt x

let nat_of_mutez (ZMutez x) = ZNat x

let fst = fst
let snd = snd
let compare x y = ZInt (Z.of_int @@ compare x y)
let ( = ) = ( = )
let ( <> ) = ( <> )
let ( < ) = ( < )
let ( <= ) = ( <= )
let ( > ) = ( > )
let ( >= ) = ( >= )
external ( && ) : bool -> bool -> bool = "%sequand"
external ( || ) : bool -> bool -> bool = "%sequor"
let band = ( && )
let bor = ( || )
let xor b1 b2 = (not b1) = b2
let not = not

exception Fail (* XXX We cannot carry the argument of failwith... *)

let failwith : 'a -> 'b = fun _ -> raise Fail

let raise = raise

type never

exception Never

let never _ = raise Never

module List = struct
  type 'a t = 'a list [@@deriving typerep]
  let length xs = ZNat (Z.of_int @@ List.length xs)
  let map = List.map
  let fold_left = List.fold_left
  let fold_left' f acc xs = List.fold_left (fun x y -> f (x, y)) acc xs
  let rev = List.rev
  let rev_append = List.rev_append
end

type 'a set = PSet of 'a Internal.Set.t [@@deriving typerep]

module Set = struct
  type 'a t = 'a set [@@deriving typerep]

  open Internal.Set

  let empty = PSet empty
  let is_empty (PSet s) = is_empty s
  let length (PSet s) = ZNat (Z.of_int @@ cardinal s)
  let mem x (PSet s) = mem x s
  let add x (PSet s) = PSet (add x s)
  let remove x (PSet s) = PSet (remove x s)
  let update x b s = if b then add x s else remove x s
  let fold f (PSet s) i = fold f s i
  let fold' f = fold (fun x acc -> f (x,acc))
end

type ('k, 'v) map = PMap of ('k, 'v) Internal.Map.t [@@deriving typerep]

module Map = struct
  type ('k, 'v) t = ('k, 'v) map [@@deriving typerep]

  open Internal.Map

  let empty = PMap empty
  let is_empty (PMap m) = is_empty m
  let length (PMap m) = ZNat (Z.of_int @@ cardinal m)
  let map f (PMap m) = PMap (mapi f m)
  let map' f (PMap m) = PMap (mapi (fun k v -> f (k,v)) m)
  let get k (PMap m) = find_opt k m
  let find = get
  let get_with_default k m d =
    match find k m with
    | None -> d
    | Some v -> v
  let find_with_default = get_with_default
  let mem k (PMap m) = mem k m
  let update k vo (PMap m) = PMap (update k (fun _ -> vo) m)
  let add k v (PMap m) = PMap (add k v m)
  let remove k (PMap m) = PMap (remove k m)
  let fold f (PMap m) acc = fold f m acc
  let fold' f m acc = fold (fun acc k v -> f (acc, k, v)) m acc
  let get_and_update k vo (PMap m) =
    (* not tested... *)
    let old_vo = ref None in
    let m = PMap.update k (fun ovo -> old_vo := ovo; vo) m in
    !old_vo, PMap m
end

type ('k, 'v) big_map = PBigMap of ('k, 'v) Internal.Map.t [@@deriving typerep]

module BigMap = struct
  type ('k, 'v) t = ('k, 'v) big_map [@@deriving typerep]

  open Internal.Map

  let empty = PBigMap empty
(*
  let is_empty (PBigMap m) = is_empty m
  let length (PBigMap m) = ZNat (Z.of_int @@ cardinal m)
  let map f (PBigMap m) = PBigMap (mapi f m)
  let map' f (PBigMap m) = PBigMap (mapi (fun k v -> f (k,v)) m)
*)
  let get k (PBigMap m) = find_opt k m
  let find = get
  let get_with_default k m d =
    match find k m with
    | None -> d
    | Some v -> v
  let find_with_default = get_with_default
  let mem k (PBigMap m) = mem k m
  let update k vo (PBigMap m) = PBigMap (update k (fun _ -> vo) m)
  let add k v (PBigMap m) = PBigMap (add k v m)
  let remove k (PBigMap m) = PBigMap (remove k m)
(*
  let fold f (PBigMap m) acc = foldi f m acc
  let fold' f m acc = fold (fun acc k v -> f (acc, k, v)) m acc
*)
  let get_and_update k vo (PBigMap m) =
    (* not tested... *)
    let old_vo = ref None in
    let m = PMap.update k (fun ovo -> old_vo := ovo; vo) m in
    !old_vo, PBigMap m
end

module Loop = struct
  let rec left f a = match f a with Left a -> left f a | Right b -> b
end

module String = struct
  let concat = ( ^ )
  let cat = ( ^ )
  let slice (ZNat a) (ZNat b) s =
    try Some (String.sub s (Z.to_int a) (Z.to_int b)) with _ -> None
  let length s = ZNat (Z.of_int @@ String.length s)
end

let ( ^ ) = String.concat

type bytes = Bytes of string literal [@@deriving typerep]

module Bytes = struct
  type t = bytes [@@deriving typerep]

  let of_ocaml_string s =
    let (`Hex hs) = Hex.of_string s in
    Bytes ("0x" ^ hs)

  let to_ocaml_string (Bytes hs) =
    let open Stdlib in
    assert (String.sub hs 0 2 = "0x");
    let hs = String.(sub hs 2 (length hs - 2)) in
    Hex.to_string (`Hex hs)

  let concat a b =
    of_ocaml_string @@ to_ocaml_string a ^ to_ocaml_string b

  let cat = concat (* XXX cannot be called *)

  let slice (ZNat a) (ZNat b) bs =
    let bs = to_ocaml_string bs in
    match Stdlib.String.sub bs (Z.to_int a) (Z.to_int b) with
    | exception _ -> None
    | s -> Some (of_ocaml_string s)

  let length (Bytes a) =
    let open Stdlib in
    let len = String.length a in
    assert (len >= 2);
    ZNat (Z.of_int @@ len / 2 - 1)

  let of_string = of_ocaml_string
end

type address = Address of string literal [@@deriving typerep]
module Address = struct
  type t = address [@@deriving typerep]
end

type key_hash = Key_hash of string literal [@@deriving typerep]
module Key_hash = struct
  type t = key_hash [@@deriving typerep]
end

type 'a contract =
  | Self : 'a contract
  | Of_address : address -> 'a contract
  | Implicit_account : key_hash -> unit contract

type source = File of string | Code of string

type operation =
  | Transfer_tokens : 'a * tz * 'a contract -> operation
  | Set_delegate : key_hash option -> operation
  | Originate : source * key_hash option * tz * 'storage -> operation

type operations = operation list

type ('param, 'storage) entry = 'param -> 'storage -> operations * 'storage

module Contract : sig
  type 'a t = 'a contract
  val self : 'a t
  val self_address : address
  val contract : address -> 'a t option
  val contract' : address -> string literal -> 'a t option
  val implicit_account : key_hash -> unit t
  val address : 'a t -> address

  (** Raw interface for CREATE_CONTRACT.

      Michelson code must be given as a string LITERAL.
      In Tezos you cannot generate contract code programically in a contract.

      The types of the contract and the initial storage are NOT checked
      by SCaml.
  *)
  val create_from_tz_code :
    string literal (* michelson code *) ->
    key_hash option (* delegat *) ->
    tz (* initial tz *) ->
    'storage (* initial storage *) ->
    operation * address

  (** Same as [create_from_tz_code] *)
  val create_raw :
    string literal -> key_hash option -> tz -> 'storage -> operation * address

  (** CREATE_CONTRACT from a michelson source file.

      Michelson file name must be given as a string literal.
      In Tezos you cannot generate contract code programically in a contract.

      The types of the contract and the initial storage are NOT checked
      by SCaml.
  *)
  val create_from_tz_file :
    string literal -> key_hash option -> tz -> 'storage -> operation * address

  module type MODULE = sig
    type storage
  end
  type 'a module_ = (module MODULE with type storage = 'a)
  val create :
    'storage module_ literal ->
    key_hash option ->
    tz ->
    'storage ->
    operation * address
end = struct
  type 'a t = 'a contract
  let self = Self
  let self_address = Address "*self*"
  let contract a = Some (Of_address a) (* always succeeds in OCaml *)
  let contract' _a _s = assert false (* XXX *)
  let implicit_account kh = Implicit_account kh
  let address (type a) (c : a contract) =
    match c with
    | Of_address a -> a
    | Implicit_account _kh -> assert false (* XXX *)
    | Self -> assert false
  (* XXX *)

  let create_raw code kho tz storage =
    Originate (Code code, kho, tz, storage), Address (assert false)
  let create_from_tz_code = create_raw
  let create_from_tz_file file kho tz storage =
    Originate (File file, kho, tz, storage), Address (assert false)

  module type MODULE = sig
    type storage
  end
  type 'a module_ = (module MODULE with type storage = 'a)
  let create _mod _kho _tz _storage = assert false
end

module Operation = struct
  type t = operation
  let transfer_tokens a tz c = Transfer_tokens (a, tz, c)
  let set_delegate kho = Set_delegate kho
end

type timestamp = Timestamp of string literal [@@deriving typerep]

module Timestamp = struct
  type t = timestamp [@@deriving typerep]

  let add (Timestamp t) (ZInt i) =
    match Ptime.of_rfc3339 t with
    | Error _ -> assert false
    | Ok (utc, _, _) -> (
        match Ptime.add_span utc (Ptime.Span.of_int_s (Z.to_int i)) with
        | None -> assert false
        | Some pt -> Timestamp (Ptime.to_rfc3339 pt))

  let sub (Timestamp t) (ZInt i) =
    match Ptime.of_rfc3339 t with
    | Error _ -> assert false
    | Ok (utc, _, _) -> (
        match Ptime.sub_span utc (Ptime.Span.of_int_s (Z.to_int i)) with
        | None -> assert false
        | Some pt -> Timestamp (Ptime.to_rfc3339 pt))

  let diff (Timestamp t1) (Timestamp t2) =
    let t1 =
      match Ptime.of_rfc3339 t1 with
      | Error _ -> assert false
      | Ok (utc, _, _) -> utc
    in
    let t2 =
      match Ptime.of_rfc3339 t2 with
      | Error _ -> assert false
      | Ok (utc, _, _) -> utc
    in
    (* XXX overflow? *)
    ZInt (Z.of_float (Ptime.Span.to_float_s (Ptime.diff t1 t2)))
end

type chain_id = Chain_id of string literal [@@deriving typerep]

module Chain_id = struct
  type t = chain_id [@@deriving typerep]
end

(* XXX Should be moved into BLS12_381 *)
type bls12_381_g1 =
  | G1Bytes of string literal (* byte sequence *)
  | G1Point of string literal * string literal
(* in nats *)

type bls12_381_g2 =
  | G2Bytes of string literal (* byte sequence *)
  | G2Point of (string * string) literal * (string * string) literal
(* in nats *)

type bls12_381_fr =
  | FrBytes of string literal (* byte sequence, nat in little endian *)
  | Fr of string literal
(* nat *)

module BLS12_381 = struct
  type g1 = bls12_381_g1
  type g2 = bls12_381_g2
  type fr = bls12_381_fr

  module G1 = struct
    type t = g1
    let ( + ) : t -> t -> t = fun _ -> assert false
    let ( * ) : t -> fr -> t = fun _ -> assert false
    let ( ~- ) : t -> t = fun _ -> assert false
  end

  module G2 = struct
    type t = g2
    let ( + ) : t -> t -> t = fun _ -> assert false
    let ( * ) : t -> fr -> t = fun _ -> assert false
    let ( ~- ) : t -> t = fun _ -> assert false
  end

  module Fr = struct
    type t = fr
    let ( + ) : t -> t -> t = fun _ -> assert false
    let ( * ) : t -> t -> t = fun _ -> assert false
    let to_int : t -> int = fun _ -> assert false
    let mul_int : int -> t -> t = fun _ -> assert false
    let mul_nat : int -> t -> t = fun _ -> assert false
    let ( ~- ) : t -> t = fun _ -> assert false
  end

  let pairing_check : (g1 * g2) list -> bool = fun _ -> assert false
end

module Env = struct
  type t = {
    now : timestamp;
    amount : tz;
    balance : tz;
    source : address;
    sender : address;
    chain_id : chain_id;
    level : nat;
  }

  let env = ref (None : t option)
  let get () = Option.get !env

  let now env = env.now
  let amount env = env.amount
  let balance env = env.balance
  let source env = env.source
  let sender env = env.sender
  let chain_id env = env.chain_id
  let level env = env.level

  let voting_power _ = assert false
  let total_voting_power _ = assert false
end

(* maybe the place is not good *)
module Global : sig
  val get_now : unit -> timestamp
  val get_amount : unit -> tz
  val get_balance : unit -> tz
  val get_source : unit -> address
  val get_sender : unit -> address
  val get_chain_id : unit -> chain_id

  val get_level : unit -> nat

  val get_voting_power : unit -> nat
  val get_total_voting_power : unit -> nat
end = struct
  open Env
  let get_now () = (get ()).now
  let get_amount () = (get ()).amount
  let get_balance () = (get ()).balance
  let get_source () = (get ()).source
  let get_sender () = (get ()).sender
  let get_chain_id () = (get ()).chain_id

  let get_level () = (get ()).level

  let get_voting_power () = assert false
  let get_total_voting_power () = assert false
end

type key = Key of string literal [@@deriving typerep]

module Key = struct
  type t = key [@@deriving typerep]
end

type signature = Signature of string literal [@@deriving typerep]

module Signature = struct
  type t = signature [@@deriving typerep]
end

module Obj = struct
  let pack_string s =
    let open Tezos_micheline in
    let s = Micheline.strip_locations (Micheline.String ((), s)) in
    let expr_encoding =
      Micheline_encoding.canonical_encoding_v1
        ~variant:"michelson_v1"
        Data_encoding.Encoding.string
    in
    match Data_encoding.(Binary.to_bytes expr_encoding s) with
    | Error _ -> assert false
    | Ok bs -> Bytes.of_ocaml_string ("\005" ^ Stdlib.Bytes.to_string bs)

  let pack : 'a -> bytes = fun _ -> assert false
  let unpack : bytes -> 'a option = fun _ -> assert false

  module Internal = struct
    module type TypeSafePack = sig
      val pack' : 'a Typerep.t -> 'a -> ocaml_string
      val unpack' : 'a Typerep.t -> ocaml_string -> 'a option
    end

    let type_safe_pack = ref (None : (module TypeSafePack) option)
  end

  module TypeSafe = struct
    let pack rep a =
      let (module M : Internal.TypeSafePack) =
        Option.get !Internal.type_safe_pack
      in
      Bytes.of_ocaml_string (M.pack' rep a)

    let unpack rep b =
      let (module M : Internal.TypeSafePack) =
        Option.get !Internal.type_safe_pack
      in
      M.unpack' rep (Bytes.to_ocaml_string b)
  end
end

module Crypto = struct
  (* XXX These functions should have polymorphic versions *)

  open Tezos_crypto

  let check_signature (Key k) (Signature s) bs =
    let open Signature in
    let k =
      match Public_key.of_b58check_opt k with
      | Some k -> k
      | None -> assert false
    in
    let s = match of_b58check_opt s with Some s -> s | None -> assert false in
    let bs = Bytes.to_ocaml_string bs in
    check k s (Stdlib.Bytes.of_string bs)

  let test_check_signature () =
    assert (
      check_signature
        (Key "edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav")
        (Signature
           "edsigu3QszDjUpeqYqbvhyRxMpVFamEnvm9FYnt7YiiNt9nmjYfh8ZTbsybZ5WnBkhA7zfHsRVyuTnRsGLR6fNHt1Up1FxgyRtF")
        (Obj.TypeSafe.pack typerep_of_string "hello")
        (* SCaml.Obj.Internal.type_safe_pack must be filled beforehand *))

  module Blake2B(A : sig
      val bytes : ocaml_int
    end) = struct
    module Bytes = Stdlib.Bytes
    open Hacl_star

    let key = Bytes.create 0

    let hash_bytes inbuf =
      (* HACL* doesn't yet provide a multiplexing interface for Blake2b so we
       * perform this check here and use the faster version if possible *)
      if AutoConfig2.(has_feature VEC256) then
        Hacl.Blake2b_256.hash ~key inbuf A.bytes
      else Hacl.Blake2b_32.hash ~key inbuf A.bytes

    let hash_string inbuf =
      Bytes.unsafe_to_string @@ hash_bytes @@ Bytes.unsafe_of_string inbuf

    let _hash_strings ss = hash_string (String.concat "" ss)
  end

  module Blake2B_32 = Blake2B(struct let bytes = 32 end)

  let blake2b bs =
    let bs =
      Blake2B_32.hash_string @@ Bytes.to_ocaml_string bs
    in
    Bytes.of_ocaml_string bs

  (* test *)
  let test_blake2b () =
    let (Bytes s) = blake2b (Obj.pack_string "foobar") in
    assert (
      s = "0xc5b7e76c15ce98128a840b54c38f462125766d2ed3a6bff0e76f7f3eb415df04")

  let sha256 bs =
    Bytes.of_ocaml_string @@ Stdlib.Bytes.to_string @@ Hacl.Hash.SHA256.digest
    @@ Stdlib.Bytes.of_string @@ Bytes.to_ocaml_string bs

  (* test *)
  let test_sha256 () =
    assert (
      sha256 (Bytes "0x0123456789ABCDEF")
      = Bytes "0x55c53f5d490297900cefa825d0c8e8e9532ee8a118abe7d8570762cd38be9818")

  let sha512 bs =
    Bytes.of_ocaml_string @@ Stdlib.Bytes.to_string @@ Hacl.Hash.SHA512.digest
    @@ Stdlib.Bytes.of_string @@ Bytes.to_ocaml_string bs

  (* test *)
  let test_sha512 () =
    assert (
      sha512 (Bytes "0x0123456789ABCDEF")
      = Bytes
          "0x650161856da7d9f818e6047cf6b2092bc7aa3767d3495cfbefe2b710ed684a43ba933ea8286ef67d975e64e0482e5ebe0701788989396545b6badb3b0a136f19")

  let keccak _ = assert false (* XXX *)

  let sha3 _ = assert false

  let hash_key (Key k) =
    let open Tezos_crypto.Signature in
    match Public_key.of_b58check_opt k with
    | None -> assert false (* must succeed *)
    | Some k -> Key_hash (Public_key_hash.to_b58check @@ Public_key.hash k)

  let test_hash_key () =
    assert (
      hash_key (Key "p2pk66uq221795tFxT7jfNmXtBMdjMf6RAaxRTwv1dbuSHbH6yfqGwz")
      = Key_hash "tz3eMVQA1K4yw47g9nHxM37BJNmDyWtpGB3T")

  module Internal = struct
    let test () =
      test_blake2b () ;
      test_sha256 () ;
      test_sha512 () ;
      test_hash_key () ;
      test_check_signature ()
  end
end

type 'a ticket

module Ticket = struct
  type 'a t = 'a ticket
  let create _ = assert false
  let read _ = assert false
  let split _ = assert false
  let join _ = assert false
end

type 'ms sapling_state
type 'ms sapling_transaction

module Sapling = struct
  type 'ms state = 'ms sapling_state
  type 'ms transaction = 'ms sapling_transaction
  let empty_state _ = assert false
  let verify_update _ = assert false
end

let view _address _name = assert false

let _Nat x = ZNat (Z.of_string x)
let _Int x = ZInt (Z.of_string x)
let _Mutez x = ZMutez (Z.of_string x)
let _Set xs =
  let open Internal.Set in
  PSet (List.fold_left (fun acc x -> add x acc) empty xs)
let _Map kvs =
  let open Internal.Map in
  PMap (List.fold_left (fun acc (k,v) -> add k v acc) empty kvs)
let _BigMap kvs =
  let open Internal.Map in
  PBigMap (List.fold_left (fun acc (k,v) -> add k v acc) empty kvs)

let z_of_int (ZInt z) = z
let z_of_nat (ZNat z) = z
let z_of_mutez (ZMutez z) =z
