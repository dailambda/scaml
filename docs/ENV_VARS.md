# Env variables

If the following env vars are not empty:

`SCAMLIB`
:    Override the default library directory carries sCaml.cmi and others.

`SCAML_DEBUG`
:    Print noisy messages to stderr

`SCAML_DUMP_IML`
:    Dumps InterMediate Language

