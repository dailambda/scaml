source code text
   |
   | OCaml parser
   |
parsetree
   | 
   | SCamlc.Preprocess
   |
   | OCaml typechecker
   |
typedtree
   |
   | backend = SCamlComp.compile
   |
   | entering SCamlComp.translate_and_optimize
   |
   | Contract.implementation
   |
IML 
   |
   | Linear
   |
   | Contract.link
   |
   | SCamlComp.optimize
   |
   | end of SCamlComp.translate_and_optimize
   |
Module.t
   |
   | back to driver/main.ml
   |
   | SCamlComp.link
   |
   | link_contract
   |
Michelson
   |
   | optimize_michelson
   |
save .tz




 
