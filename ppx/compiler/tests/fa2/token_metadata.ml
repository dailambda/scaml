[@@@SCaml]

open SCaml

(* token metadata *)

type t = (string, bytes) map

(* TZIP-012 predefined keys

   "" (empty-string): should correspond to a TZIP-016 URI which points to a JSON
                      representation of the token metadata.

   "name": should be a UTF-8 string giving a “display name” to the token.

   "symbol": should be a UTF-8 string for the short identifier of the token
             (e.g. XTZ, EUR, …).

   "decimals": should be an integer (converted to a UTF-8 string in decimal)
               which defines the position of the decimal point in token balances for display
               purposes.
*)
let key_tzip_016_uri = ""

let key_name = "name"

let key_suymobl = "symbol"

let key_decimals = "decimals"
