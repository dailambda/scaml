let () = prerr_endline "multi"
module Out1 = struct
  [@@@SCaml contract = "out1"]
  let[@entry] main () () = [], ()
end

module Out2 = struct
  [@@@SCaml contract = "out2"]
  let[@entry] main () () = [], ()
end
