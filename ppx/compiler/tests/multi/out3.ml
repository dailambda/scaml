let () = prerr_endline "out3"
module Out3 = struct
  [@@@SCaml contract = "out3"]
  let[@entry] main () () = [], ()
end
