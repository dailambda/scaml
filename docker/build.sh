#!/bin/bash

# ./build.sh <GIT_COMMIT> <DOCKER_TAG> options..
# ex ./build.sh  b33327414c69c1cc0a58fc4aefdc567c023857bd master --squash
set -e

if [ -z "$SCAML_DOCKER_TAG" ]; then
    echo  'SCAML_DOCKER_TAG required'
    exit 1
fi

if [ -z "$COMMIT" ]; then
    echo  'COMMIT required'
    exit 1
fi

if [ -z "$OCAML_VERSION" ]; then
    echo  'OCAML_VERSION required'
    exit 1
fi

if [ -z "$TEZOS_TAG" ]; then
    echo  'TEZOS_TAG required'
    exit 1
fi

sed -e "s/@COMMIT@/$COMMIT/g" \
    -e "s%@OPAM_PINS@%$OPAM_PINS%g" \
    -e "s/@OCAML_VERSION@/$OCAML_VERSION/g" \
    -e "s/@TEZOS_TAG@/$TEZOS_TAG/g" \
    Dockerfile.in > Dockerfile

rm -rf _build
mkdir _build
(cd _build; git clone https://gitlab.com/dailambda/scaml; cd scaml; git checkout $COMMIT)

echo docker build "$@" -t dailambda/scaml:$SCAML_DOCKER_TAG .
docker build "$@" -t dailambda/scaml:$SCAML_DOCKER_TAG .
cp _build/scaml/tests/app_vote.ml _build

cat > scamlc <<EOF
#!/bin/sh
docker run --rm		 \
  -v `pwd`:/work	 \
  dailambda/scaml:$SCAML_DOCKER_TAG \
  /home/opam/.opam/$OCAML_VERSION/bin/scamlc "\$@"
EOF
chmod +x ./scamlc

cat > optz <<EOF
 #!/bin/sh
docker run --rm		 \
  --env OPTZ_DEBUG=y     \
  -v `pwd`:/work	 \
  dailambda/scaml:$SCAML_DOCKER_TAG \
  /home/opam/.opam/$OCAML_VERSION/bin/optz "\$@"
EOF
chmod +x ./optz

cat > tezos-client <<EOF
 #!/bin/sh
docker run --rm		 \
  -v `pwd`:/work	 \
  dailambda/scaml:$SCAML_DOCKER_TAG \
  /usr/bin/tezos-client "\$@"
EOF
chmod +x ./tezos-client

./scamlc _build/app_vote.ml -o _build/app_vote.tz
./optz _build/app_vote.tz
mkdir -p _build/mock
TEZOS_CLIENT="./tezos-client --protocol Psithaca2MLR --mode mockup --base-dir _build/mock"
$TEZOS_CLIENT create mockup 
$TEZOS_CLIENT config init
$TEZOS_CLIENT typecheck script _build/app_vote.tz
echo test done

docker run -it --rm dailambda/scaml:$SCAML_DOCKER_TAG bash
