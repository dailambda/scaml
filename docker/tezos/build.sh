docker build "$@" -t dailambda/tezos:v12.3 .
echo docker build done
docker run --rm dailambda/tezos:v12.3 tezos-client --version
docker run -it --rm dailambda/tezos:v12.3 bash
