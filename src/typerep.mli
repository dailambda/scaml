open Typerep_lib.Std

exception Overflow

(** Between OCaml and Micheline *)

val of_micheline : 'a Typerep.t -> ('loc, string) Tezos_micheline.Micheline.node -> 'a option
val to_micheline : 'a Typerep.t -> 'a -> SCaml_michelson.Micheline.t
(** Between OCaml and SCaml represetation of Michelson *)
val to_michelson : 'a Typerep.t -> 'a -> Michelson.Constant.t

val of_michelson : 'a Typerep.t -> Michelson.Constant.t -> 'a option

(** Between Micheline and SCaml representation of Michelson *)
val michelson_of_micheline :
  'a Typerep.t -> ('loc, string) Tezos_micheline.Micheline.node -> Michelson.Constant.t option

val micheline_of_michelson : Michelson.Constant.t -> SCaml_michelson.Micheline.t

(** Conversion from OCaml/SCaml type to Michelson type *)
val to_michelson_type : 'a Typerep.t -> Michelson.Type.t
