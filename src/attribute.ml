(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Untyped
open Parsetree
open Typedtree
open Tools

(* attributes *)

type t = {
  ident : Longident.t;
  loc : Location.t;
  value : [`Bool of bool | `Constant of Ppxlib.Parsetree.constant];
}

let parse_options_in_payload ~loc name payload =
  match payload with
  | PStr str ->
      let parse {pstr_desc; pstr_loc = loc} =
        match pstr_desc with
        | Pstr_eval (_, _ :: _) ->
            Error.raisef
              Attribute
              ~loc
              "%s attribute expression cannot take attributes"
              name
        | Pstr_eval (e, []) ->
            let rec parse {pexp_desc; pexp_loc = loc} =
              match pexp_desc with
              | Pexp_sequence (e1, e2) -> parse e1 @ parse e2
              | Pexp_tuple es -> List.concat_map parse es
              | Pexp_apply
                  ( {pexp_desc = Pexp_ident {txt = Longident.Lident "="}},
                    [(Nolabel, e1); (Nolabel, e2)] ) ->
                  let key =
                    match e1.pexp_desc with
                    | Pexp_ident lloc -> lloc
                    | _ ->
                        Error.raisef
                          Attribute
                          ~loc:e1.pexp_loc
                          "%s attribute item key must be an identifier"
                          name
                  in
                  let value =
                    match e2.pexp_desc with
                    | Pexp_constant c -> `Constant c
                    | Pexp_construct ({txt = Longident.Lident "true"}, None) ->
                        `Bool true
                    | Pexp_construct ({txt = Longident.Lident "false"}, None) ->
                        `Bool false
                    | _ ->
                        Error.raisef
                          Attribute
                          ~loc:e2.pexp_loc
                          "%s attribute item value must be a constant or a bool"
                          name
                  in
                  let loc = key.loc in
                  let ident = key.txt in
                  [{loc; ident; value}]
              | _ ->
                  Error.raisef
                    Attribute
                    ~loc
                    "%s attribute item must have a form key=value"
                    name
            in
            parse e
        | _ ->
            Error.raisef
              Attribute
              ~loc
              "%s attribute must have a form of key=value; ..; key_value"
              name
      in
      List.concat (List.map parse str)
  | PSig _ | PTyp _ | PPat _ ->
      Error.raisef Attribute ~loc "%s attribute requires a structure" name

let get_scaml_toplevel_attributes str =
  let structure_item (st, at_the_head) {str_desc; _} =
    match str_desc with
    | Tstr_attribute {attr_name = {txt = "SCaml"; loc}; attr_payload = payload}
      ->
        if not at_the_head then
          Error.raisef
            Attribute
            ~loc
            "SCaml attributes must appear at the head of a structure." ;
        (* We use OCaml 4.11.2 but Ppxlib uses 4.10 *)
        let payload = Migrate.copy_payload payload in
        (loc, parse_options_in_payload ~loc "SCaml" payload) :: st, true
    | Tstr_attribute {attr_name = {txt = s; loc}} -> (
        let s_low = String.lowercase_ascii s in
        match s_low with
        | "scaml" -> Error.raisef Attribute ~loc "SCaml, not %s." s
        | "scam" -> Error.raisef Attribute ~loc "SCaml, it's not a scam."
        | _ -> st, true
        (* We may have other attributes before SCaml *))
    | _ -> st, false
    (* we cannot have SCaml attributes later *)
  in
  let structure {str_items = sitems} =
    List.rev & fst & List.fold_left structure_item ([], true) sitems
  in
  structure str

let reject_SCaml_attribute_in_complex_structure str =
  let open Typedtree in
  let open Tast_iterator in
  let bad loc =
    Error.raisef
      Attribute
      ~loc
      "SCaml attributes cannot appear in let module, functors, functor \
       applications and packed modules"
  in

  let module Reject = struct
    let structure iter str =
      match get_scaml_toplevel_attributes str with
      | [] -> default_iterator.structure iter str
      | (loc, _) :: _ -> bad loc
    let iter = {default_iterator with structure}
  end in
  let module Check = struct
    let expr iter expr =
      match expr.exp_desc with
      | Texp_letmodule (_, _, _, me, e) ->
          Reject.iter.module_expr Reject.iter me ;
          iter.expr iter e
      | Texp_pack me -> Reject.iter.module_expr Reject.iter me
      | _ -> default_iterator.expr iter expr
    let module_expr iter me =
      match me.mod_desc with
      | Tmod_functor (_, me) -> Reject.iter.module_expr Reject.iter me
      | Tmod_apply (me1, me2, _) ->
          Reject.iter.module_expr Reject.iter me1 ;
          Reject.iter.module_expr Reject.iter me2
      | _ -> default_iterator.module_expr iter me
    let iter = {default_iterator with expr; module_expr}
  end in
  Check.iter.structure Check.iter str

(* module [@SCaml] M = struct .. end <=== NOT YET DONE

   or

   [@@@SCaml] at the top of structure

   Internal modules are NOT automatically used even if the outer module
   has the attribute.
*)
let filter_by_SCaml_attribute str =
  let open Tast_mapper in
  let open Typedtree in
  let structure mapper str =
    match get_scaml_toplevel_attributes str with
    | _ :: _ ->
        (* It is with [@@@SCaml].  We use the code.
           Internal structures must have their own [@@@SCaml] attributes to be used.
        *)
        let str_items = List.map (mapper.structure_item mapper) str.str_items in
        {str with str_items}
    | [] ->
        (* We skip this structure itself, but its substructures may have [@@@SCaml]. *)
        let str_items =
          List.filter_map
            (fun sitem ->
              match sitem.str_desc with
              | Tstr_module _ -> Some sitem
              | Tstr_recmodule _ -> Some sitem
              | _ -> None)
            str.str_items
        in
        let str_items = List.map (mapper.structure_item mapper) str_items in
        {str with str_items}
  in
  let mapper = {default with structure} in
  mapper.structure mapper str
