(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2020  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Untyped OCaml AST *)

(* Our compiler is 4.12.1
   Ppx_lib.Ast_builder of ppxlib.0.23.0 uses 4.12 *)

module Longident = Ppxlib.Longident
module Parsetree = Ppxlib.Parsetree
module Pprintast = Ppxlib.Pprintast
module Ast_builder = Ppxlib.Ast_builder.Make (struct
  let loc = Location.none
end)

(* module Migrate = Migrate_parsetree__.Migrate_parsetree_411_410_migrate *)
module Migrate = struct
  let copy_attributes x = x
  let copy_payload x = x
end
