(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                Copyright 2019,2020,2021  DaiLambda, Inc.               *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** Compilation unit ( *.ml file) *)
module Module : sig
  type t = {
    name : string  (** Name: "Foobar" for foobar.ml *);
    sourcefile : string  (** "dir/foobar.ml" *);
    contracts : Contract.t list  (** Contracts defined in this module *);
    defs : IML.t option
        (** Toplevel definitions with a placeholder.
          If there is no SCaml definitions, [None]. *);
    conf : Conf.opt  (** Compilation conf *);
  }

  val pp : Format.formatter -> t -> unit
end

(** [translate_and_optimize sourcefile outputprefix modulename typed_module] *)
val translate_and_optimize :
  string ->
  string ->
  string ->
  Typedtree.implementation ->
  Module.t

(** Returns the modules translated so far by [translate_and_optimize] *)
val get_translated_modules : unit -> Module.t list

(** Link the given translated modules then compile them to a Michelson file. *)
val link : string (* outputdir *) -> Module.t list -> unit

val link_specified_module : string (* outputdir *) -> Module.t list -> string (* name *) -> unit

(** "compile" a given file.  The behaviour is controlled by the value
    of [Conf.conf]:

    Compile : compile the given source code to Michelson

    ConvertAll file : convert the constant values and types to Michelson,
                      then print them
    ConvertSingleValue file : convert the expression in the file
                              to a Michelson constant then print it
    ConvertSingleType file : convert the type expression in the file
                             to a Michelson type then print it
*)
val compile :
  string ->
  string ->
  string ->
  Typedtree.implementation ->
  unit
