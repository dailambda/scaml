(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* InterMediate Language, or Intermediate ML *)
open Tools
open Untyped

module M = Michelson
module Type = M.Type

type ('desc, 'attrs) with_loc_and_type = {
  desc : 'desc;
  loc : Location.t;
  typ : Type.t;
  attrs : 'attrs;
}

let mk ~loc typ desc = {loc; desc; typ; attrs = []}

module IdTys = Set.Make (struct
  type t = Ident.t * Type.t
  let compare (id1, _) (id2, _) = compare id1 id2
end)

module Attr = struct
  type t = Comment of string | Annot of string | Type of Outcometree.out_type

  type ts = t list

  let add a t = {t with attrs = a :: t.attrs}
  let adds attrs t = {t with attrs = attrs @ t.attrs}
end

module Idents = Set.Make (struct
  type t = (Ident.t, Attr.ts) with_loc_and_type
  let compare {desc = id1} {desc = id2} = compare id1 id2
end)

module PatVar = struct
  type t = (Ident.t, Attr.ts) with_loc_and_type

  let pp ppf var = Format.fprintf ppf "%s" (Ident.unique_name var.desc)
end

type contract_source =
  | Tz_code of string
  | Tz_file of string
  | Module of Path.t

let self_name = "__scaml_self"

type t = (desc, Attr.ts) with_loc_and_type

and desc =
  | Const of M.Constant.t
  | Nil
  | Cons of t * t
  | IML_None
  | IML_Some of t
  | Left of t
  | Right of t
  | Var of Ident.t
  | Pair of t * t
  | Assert of t
  | AssertFalse
  | Fun of PatVar.t * t
  | IfThenElse of t * t * t option
  | App of t * t list
  | Prim of string * M.Type.t * t list
  | Let of PatVar.t * t * t
  | LetRec of PatVar.t * t * t
  | Unpair of PatVar.t * PatVar.t * t * t
  | Switch_or of t * PatVar.t * t * PatVar.t * t
  | Switch_cons of t * PatVar.t * PatVar.t * t * t
  | Switch_none of t * t * PatVar.t * t
  | Contract_create of contract_source * Location.t * t * t * t
  | Seq of t * t
  | Set of t list
  | Map of (t * t) list
  | BigMap of (t * t) list
  | Dealloc of t * Ident.t (* For optimization *)
  | Dealloc_then of Ident.t * t
(* for optimization.  Deallocate the [id] before [t] *)

(* Printer *)
module P = struct
  (* forge OCaml untyped AST from IML.t just for printing *)
  open Ast_builder
  open Parsetree

  let loc = Location.none

  let memo_size n =
    assert (n > 0) ;
    let rf =
      {
        prf_desc =
          Rtag ({txt = Printf.sprintf "n%d" n; loc = Location.none}, true, []);
        prf_loc = Location.none;
        prf_attributes = [];
      }
    in
    {
      ptyp_desc = Ptyp_variant ([rf], Closed, None);
      ptyp_loc = Location.none;
      ptyp_loc_stack = [];
      ptyp_attributes = [];
    }

  let rec type_ (ty : M.Type.t) =
    let open M.Type in
    let t =
      match ty.desc with
      | TyString -> [%type: string]
      | TyNat -> [%type: nat]
      | TyInt -> [%type: int]
      | TyBytes -> [%type: byte]
      | TyBool -> [%type: bool]
      | TyUnit -> [%type: unit]
      | TyList t -> [%type: [%t type_ t] list]
      | TyPair (_, t1, _, t2) -> [%type: [%t type_ t1] * [%t type_ t2]]
      | TyPairN fts ->
          begin match fts with
            | [] | [_] -> assert false
            | [(_f0,t0); (f1,t1); (f2,t2)] ->
                [%type: [%t type_ t0] * [%t type_ { ty with desc= TyPair (f1,t1,f2,t2) }]]
            | (_f0,t0)::fts ->
                [%type: [%t type_ t0] * [%t type_ { ty with desc= TyPairN fts } ]]
          end
      | TyOption (_, t) -> [%type: [%t type_ t] option]
      | TyOr (_, t1, _, t2) -> [%type: ([%t type_ t1], [%t type_ t2]) sum]
      | TySet t -> [%type: [%t type_ t] set]
      | TyMap (k, v) -> [%type: ([%t type_ k], [%t type_ v]) map]
      | TyBigMap (k, v) -> [%type: ([%t type_ k], [%t type_ v]) big_map]
      | TyMutez -> [%type: tz]
      | TyKeyHash -> [%type: key_hash]
      | TyTimestamp -> [%type: timestamp]
      | TyAddress -> [%type: address]
      | TyChainID -> [%type: chain_id]
      | TyKey -> [%type: key]
      | TySignature -> [%type: signature]
      | TyOperation -> [%type: operation]
      | TyContract t -> [%type: [%t type_ t] contract]
      | TyLambda (t1, t2) -> [%type: [%t type_ t1] -> [%t type_ t2]]
      | TyNever -> [%type: never]
      | TyBLS12_381_Fr -> [%type: bls12_381_fr]
      | TyBLS12_381_G1 -> [%type: bls12_381_g1]
      | TyBLS12_381_G2 -> [%type: bls12_381_g2]
      | TySapling_state n -> [%type: [%t memo_size n] sapling_state]
      | TySapling_transaction n -> [%type: [%t memo_size n] sapling_transction]
      | TyTicket t -> [%type: [%t type_ t] ticket]
    in
    if ty.linear then [%type: [%t t] linear] else t

  let _type = type_
  (* well we do not use it for now, the types are too lousy to read *)

  let rec constant =
    let open M.Constant in
    function
    | Unit -> [%expr ()]
    | Bool true -> [%expr true]
    | Bool false -> [%expr false]
    | Int z ->
        {
          pexp_desc = Pexp_constant (Pconst_integer (Z.to_string z, None));
          pexp_loc = Location.none;
          pexp_loc_stack = [];
          pexp_attributes = [];
        }
    | String s -> estring s
    | Bytes b ->
        let (`Hex h) = Hex.of_string b in
        [%expr Bytes [%e estring h]]
    | Option None -> [%expr None]
    | Option (Some e) -> [%expr Some [%e constant e]]
    | List ts -> elist & List.map constant ts
    | Set ts -> [%expr Set [%e elist & List.map constant ts]]
    | Map kvs ->
        [%expr
          Map
            [%e
              elist
              & List.map
                  (fun (k, v) -> pexp_tuple [constant k; constant v])
                  kvs]]
    | Pair (t1, t2) -> pexp_tuple [constant t1; constant t2]
    | PairN _ -> assert false (* SCaml does not use PairN until the last optimization *)
    | Left t -> [%expr Left [%e constant t]]
    | Right t -> [%expr Right [%e constant t]]
    | Timestamp z -> begin
        match Ptime.of_float_s (Z.to_float z) with
        | Some t -> [%expr Timestamp [%e estring (Ptime.to_rfc3339 t)]]
        | None -> [%expr Timestamp [%e eint (Z.to_int z)]]
      end
    | Code ops ->
        let s =
          Format.sprintf "@[<2>{ %a }@]" (Format.list ";@ " M.Opcode.pp) ops
        in
        {
          pexp_desc =
            Pexp_constant (Pconst_string (s, Location.none, Some "michelson"));
          pexp_loc = Location.none;
          pexp_loc_stack = [];
          pexp_attributes = [];
        }

  let attr a e =
    let open Parsetree in
    match a with
    | Attr.Comment _ -> e
    | Annot a ->
        {
          e with
          pexp_attributes =
            {
              attr_name = {txt = a; loc = Location.none};
              attr_payload = PStr [];
              attr_loc = Location.none;
            }
            :: e.pexp_attributes;
        }
    | Type _ -> e

  let pattr a p =
    let open Parsetree in
    match a with
    | Attr.Comment _ -> p
    | Annot a ->
        {
          p with
          ppat_attributes =
            {
              attr_name = {txt = a; loc = Location.none};
              attr_payload = PStr [];
              attr_loc = Location.none;
            }
            :: p.ppat_attributes;
        }
    | Type _ -> p

  let patvar pv =
    let p = pvar (Ident.unique_name pv.desc) in
    List.fold_right pattr pv.attrs p

  let var id =
    let printable s =
      (* ocamlformat does not support strange ids *)
      let lowercase_latin1 c =
        match c with
        | 'a' .. 'z' | '\223' .. '\246' | '\248' .. '\255' | '_' -> true
        | _ -> false
      in
      let identchar_latin1 c =
        match c with
        | 'A' .. 'Z'
        | 'a' .. 'z'
        | '_'
        | '\192' .. '\214'
        | '\216' .. '\246'
        | '\248' .. '\255'
        | '\''
        | '0' .. '9' ->
            true
        | _ -> false
      in
      let len = String.length s in
      assert (len > 0) ;
      lowercase_latin1 s.[0]
      &&
      let rec f i =
        if i = len then true
        else if identchar_latin1 s.[i] then f (i + 1)
        else false
      in
      f 1
    in
    let s =
      let s = Ident.unique_name id in
      if printable s then s
      else
        (* OCaml does not expose stamp of Ident.t *)
        let pos = String.rindex s '_' in
        let stamp = String.(sub s pos (length s - pos)) in
        "op" ^ stamp
    in
    evar s

  let rec iml ?(limit = max_int) {desc; typ = _; attrs} =
    let e =
      if limit = 0 then
        let iml = iml ~limit:0 in
        let dots =
          {
            pexp_desc =
              Pexp_ident {txt = Longident.Lident ".."; loc = Location.none};
            pexp_loc = Location.none;
            pexp_loc_stack = [];
            pexp_attributes = [];
          }
        in
        match desc with
        | Const c -> constant c
        | Nil -> [%expr []] (* type? *)
        | IML_None -> [%expr None]
        | Var id -> var id
        | AssertFalse -> [%expr assert false]
        | App (t, []) -> attr (Annot "empty_app") & iml t
        | Seq (t1, _t2) ->
            [%expr
              [%e iml t1] ;
              [%e dots]]
        | Dealloc (t, id) ->
            [%expr
              [%e iml t] ;
              dealloc [%e evar (Ident.unique_name id)]]
        | Dealloc_then (id, t) ->
            [%expr
              dealloc [%e evar (Ident.unique_name id)] ;
              [%e iml t]]
        | _ -> dots
      else
        let iml = iml ~limit:(limit - 1) in
        match desc with
        | Const c -> constant c
        | Nil -> [%expr []] (* type? *)
        | Cons (t1, t2) ->
            let t1 = iml t1 in
            let t2 = iml t2 in
            [%expr [%e t1] :: [%e t2]]
        | IML_None -> [%expr None]
        | IML_Some t -> [%expr Some [%e iml t]]
        | Left t -> [%expr Left [%e iml t]]
        | Right t -> [%expr Right [%e iml t]]
        | Var id -> var id
        | Pair (t1, t2) -> pexp_tuple [iml t1; iml t2]
        | Assert t -> [%expr assert [%e iml t]]
        | AssertFalse -> [%expr assert false]
        | Fun (pv, t) ->
            let pv = patvar pv in
            [%expr fun [%p pv] -> [%e iml t]]
        | IfThenElse (t1, t2, None) -> [%expr if [%e iml t1] then [%e iml t2]]
        | IfThenElse (t1, t2, Some t3) ->
            [%expr if [%e iml t1] then [%e iml t2] else [%e iml t3]]
        | App (t, []) -> attr (Annot "empty_app") & iml t
        | App (t, ts) -> eapply (iml t) (List.map iml ts)
        | Prim (s, _, ts) -> eapply (evar s) (List.map iml ts)
        | Let (pv, t1, t2) ->
            let ty = type_ pv.typ in
            let pv = patvar pv in
            [%expr
              let ([%p pv] : [%t ty]) = [%e iml t1] in
              [%e iml t2]]
        | LetRec (pv, t1, t2) ->
            let ty = type_ pv.typ in
            let pv = patvar pv in
            [%expr
              let rec ([%p pv] : [%t ty]) = [%e iml t1] in
              [%e iml t2]]
        | Unpair (pv1, pv2, t1, t2) ->
            let ty1 = type_ pv1.typ in
            let pv1 = patvar pv1 in
            let ty2 = type_ pv2.typ in
            let pv2 = patvar pv2 in
            [%expr
              let ([%p pv1] : [%t ty1]), ([%p pv2] : [%t ty2]) = [%e iml t1] in
              [%e iml t2]]
        | Switch_or (t, pv1, t1, pv2, t2) ->
            let pv1 = patvar pv1 in
            let pv2 = patvar pv2 in
            [%expr
              match [%e iml t] with
              | Left [%p pv1] -> [%e iml t1]
              | Right [%p pv2] -> [%e iml t2]]
        | Switch_cons (t, pv1, pv2, t1, t2) ->
            let pv1 = patvar pv1 in
            let pv2 = patvar pv2 in
            [%expr
              match [%e iml t] with
              | [%p pv1] :: [%p pv2] -> [%e iml t1]
              | [] -> [%e iml t2]]
        | Switch_none (t, t1, pv2, t2) ->
            let pv2 = patvar pv2 in
            [%expr
              match [%e iml t] with
              | None -> [%e iml t1]
              | Some [%p pv2] -> [%e iml t2]]
        | Contract_create (Module p, _, t1, t2, t3) ->
            let code =
              {
                pexp_desc =
                  Pexp_pack
                    {
                      pmod_desc =
                        Pmod_ident
                          {
                            txt = Untypeast.lident_of_path p;
                            loc = Location.none;
                          };
                      pmod_loc = Location.none;
                      pmod_attributes = [];
                    };
                pexp_loc = Location.none;
                pexp_loc_stack = [];
                pexp_attributes = [];
              }
            in
            [%expr
              Contract.create [%e code] [%e iml t1] [%e iml t2] [%e iml t3]]
        | Contract_create (Tz_code code, _, t1, t2, t3) ->
            let code =
              {
                pexp_desc =
                  Pexp_constant (Pconst_string (code, Location.none, Some ""));
                pexp_loc = Location.none;
                pexp_loc_stack = [];
                pexp_attributes = [];
              }
            in
            [%expr
              Contract.create_from_tz_code
                [%e code]
                [%e iml t1]
                [%e iml t2]
                [%e iml t3]]
        | Contract_create (Tz_file file, _, t1, t2, t3) ->
            [%expr
              Contract.create_from_tz_file
                [%e estring file]
                [%e iml t1]
                [%e iml t2]
                [%e iml t3]]
        | Seq (t1, t2) ->
            [%expr
              [%e iml t1] ;
              [%e iml t2]]
        | Set ts -> [%expr Set [%e elist & List.map iml ts]]
        | Map kvs ->
            [%expr
              Map
                [%e
                  elist
                  & List.map
                      (fun (k, v) -> pexp_tuple [iml k; iml v])
                      kvs]]
        | BigMap kvs ->
            [%expr
              BigMap
                [%e
                  elist
                  & List.map
                      (fun (k, v) -> pexp_tuple [iml k; iml v])
                      kvs]]
        | Dealloc (t, id) ->
            [%expr
              [%e iml t] ;
              dealloc [%e evar (Ident.unique_name id)]]
        | Dealloc_then (id, t) ->
            [%expr
              dealloc [%e evar (Ident.unique_name id)] ;
              [%e iml t]]
    in
    List.fold_left (fun e a -> attr a e) e attrs

  let pp ppf t = Pprintast.expression ppf (iml t)

  let pp_with_limit limit ppf t = Pprintast.expression ppf (iml ~limit t)
end

let pp = P.pp

let pp_with_limit = P.pp_with_limit

let save path ts =
  let (), t =
    with_time & fun () ->
    let default () =
      (* default *)
      Format.eprintf "%s: ocamlformat failed.  Using default printer@." path ;
      let oc = open_out path in
      let ppf = Format.of_out_channel oc in
      List.iter (Format.fprintf ppf "%a@.@." P.pp) ts ;
      close_out oc
    in
    match
      (* wrap-fun-args=false (included in sparse) is required
         otherwise it sometimes takes more than 1 minute to format the code.
      *)
      Unix.open_process_out
        (Printf.sprintf
           "ocamlformat --disable-conf-files --enable-outside-detected-project \
            --impl -p ocamlformat - > %s"
           path)
    with
    | exception _ -> default ()
    | oc -> (
        let ppf = Format.formatter_of_out_channel oc in
        List.iter (Format.fprintf ppf "%a;;@.@." P.pp) ts ;
        match Unix.close_process_out oc with WEXITED 0 -> () | _ -> default ())
  in
  (*
  Format.eprintf "Saved to %s in %.2f secs@." path t;
*)
  if t > 1.0 then
    Format.eprintf "Warning: serious performance problem of ocamlformat@."

let rec freevars t =
  let open Idents in
  let psingleton p = singleton p in
  let ( + ) = union in
  match t.desc with
  | Const _ | Nil | IML_None -> empty
  | Contract_create (_, _, t1, t2, t3) ->
      freevars t1 + freevars t2 + freevars t3
  | Cons (t1, t2) | Pair (t1, t2) | Seq (t1, t2) ->
      union (freevars t1) (freevars t2)
  | Left t | Right t | IML_Some t | Assert t -> freevars t
  | AssertFalse -> empty
  | Var id -> singleton {desc = id; typ = t.typ; loc = t.loc; attrs = t.attrs}
  | IfThenElse (t1, t2, Some t3) -> freevars t1 + freevars t2 + freevars t3
  | IfThenElse (t1, t2, None) -> freevars t1 + freevars t2
  | App (t, ts) -> List.fold_left (fun acc t -> acc + freevars t) empty (t :: ts)
  | Prim (_, _, ts) -> List.fold_left (fun acc t -> acc + freevars t) empty ts
  | Fun (pat, t) -> diff (freevars t) (psingleton pat)
  | Let (pat, t1, t2) | LetRec (pat, t1, t2) ->
      diff (freevars t1 + freevars t2) (psingleton pat)
  | Unpair (pv1, pv2, t1, t2) ->
      diff (freevars t1 + freevars t2) (psingleton pv1 + psingleton pv2)
  | Switch_or (t, p1, t1, p2, t2) ->
      freevars t
      + diff (freevars t1) (psingleton p1)
      + diff (freevars t2) (psingleton p2)
  | Switch_cons (t, p1, p2, t1, t2) ->
      freevars t
      + diff (diff (freevars t1) (psingleton p1)) (psingleton p2)
      + freevars t2
  | Switch_none (t, t1, p2, t2) ->
      freevars t + freevars t1 + diff (freevars t2) (psingleton p2)
  | Set ts -> unions (List.map freevars ts)
  | Map tts ->
      unions (List.map (fun (t1, t2) -> union (freevars t1) (freevars t2)) tts)
  | BigMap tts ->
      unions (List.map (fun (t1, t2) -> union (freevars t1) (freevars t2)) tts)
  | Dealloc (t, _) -> freevars t
  | Dealloc_then (_, t) -> freevars t

(* t2[t_i/id_i]
   XXX very inefficient.  should be removed somehow.
*)
let subst id_t_list t2 =
  let rec f t =
    let mk desc = {t with desc} in
    match t.desc with
    | Dealloc _ -> assert false (* cannot run [subst] against it *)
    | Dealloc_then _ -> assert false (* cannot run [subst] against it *)
    | Var id -> begin
        match List.assoc_opt id id_t_list with
        | None -> t
        | Some t' -> Attr.adds t.attrs t'
      end
    | Const _ | Nil | IML_None | AssertFalse -> t
    | Contract_create (s, l, t1, t2, t3) ->
        mk & Contract_create (s, l, f t1, f t2, f t3)
    | IML_Some t -> mk & IML_Some (f t)
    | Left t -> mk & Left (f t)
    | Right t -> mk & Right (f t)
    | Assert t -> mk & Assert (f t)
    | Fun (pat, t) -> mk & Fun (pat, f t)
    | Let (p, t1, t2) -> mk & Let (p, f t1, f t2)
    | LetRec (p, t1, t2) -> mk & LetRec (p, f t1, f t2)
    | Unpair (p1, p2, t1, t2) -> mk & Unpair (p1, p2, f t1, f t2)
    | Cons (t1, t2) -> mk & Cons (f t1, f t2)
    | Pair (t1, t2) -> mk & Pair (f t1, f t2)
    | IfThenElse (t1, t2, Some t3) -> mk & IfThenElse (f t1, f t2, Some (f t3))
    | IfThenElse (t1, t2, None) -> mk & IfThenElse (f t1, f t2, None)
    | Switch_or (t1, p1, t2, p2, t3) -> mk & Switch_or (f t1, p1, f t2, p2, f t3)
    | Switch_cons (t1, p1, p2, t2, t3) ->
        mk & Switch_cons (f t1, p1, p2, f t2, f t3)
    | Switch_none (t1, t2, p, t3) -> mk & Switch_none (f t1, f t2, p, f t3)
    | App (t, ts) -> mk & App (f t, List.map f ts)
    | Prim (a, b, ts) -> mk & Prim (a, b, List.map f ts)
    | Seq (t1, t2) -> mk & Seq (f t1, f t2)
    | Set ts -> mk & Set (List.map f ts)
    | Map tts -> mk & Map (List.map (fun (k, v) -> f k, f v) tts)
    | BigMap tts -> mk & BigMap (List.map (fun (k, v) -> f k, f v) tts)
  in
  f t2

(* t2[id'_i/id_i]
   Same as subst, but variables are renamed to variables.
   It keeps the original locations.
   XXX very inefficient.  should be removed somehow.
*)
let alpha_conv id_t_list t2 =
  let rec f t =
    let mk desc = {t with desc} in
    match t.desc with
    | Dealloc _ -> assert false (* cannot run [alpha_conv] against it *)
    | Dealloc_then _ -> assert false (* cannot run [alpha_conv] against it *)
    | Var id -> begin
        match List.assoc_opt id id_t_list with
        | None -> t
        | Some id' -> {t with desc = Var id'}
      end
    | Const _ | Nil | IML_None | AssertFalse -> t
    | Contract_create (cs, l, t1, t2, t3) ->
        mk & Contract_create (cs, l, f t1, f t2, f t3)
    | IML_Some t -> mk & IML_Some (f t)
    | Left t -> mk & Left (f t)
    | Right t -> mk & Right (f t)
    | Assert t -> mk & Assert (f t)
    | Fun (pat, t) -> mk & Fun (pat, f t)
    | Let (p, t1, t2) -> mk & Let (p, f t1, f t2)
    | LetRec (p, t1, t2) -> mk & LetRec (p, f t1, f t2)
    | Unpair (p1, p2, t1, t2) -> mk & Unpair (p1, p2, f t1, f t2)
    | Cons (t1, t2) -> mk & Cons (f t1, f t2)
    | Pair (t1, t2) -> mk & Pair (f t1, f t2)
    | IfThenElse (t1, t2, Some t3) -> mk & IfThenElse (f t1, f t2, Some (f t3))
    | IfThenElse (t1, t2, None) -> mk & IfThenElse (f t1, f t2, None)
    | Switch_or (t1, p1, t2, p2, t3) -> mk & Switch_or (f t1, p1, f t2, p2, f t3)
    | Switch_cons (t1, p1, p2, t2, t3) ->
        mk & Switch_cons (f t1, p1, p2, f t2, f t3)
    | Switch_none (t1, t2, p, t3) -> mk & Switch_none (f t1, f t2, p, f t3)
    | App (t, ts) -> mk & App (f t, List.map f ts)
    | Prim (a, b, ts) -> mk & Prim (a, b, List.map f ts)
    | Seq (t1, t2) -> mk & Seq (f t1, f t2)
    | Set ts -> mk & Set (List.map f ts)
    | Map tts -> mk & Map (List.map (fun (k, v) -> f k, f v) tts)
    | BigMap tts -> mk & BigMap (List.map (fun (k, v) -> f k, f v) tts)
  in
  f t2

open Michelson.Type

let mke ~loc typ desc = {typ; desc; loc; attrs = []}
let mkvar ~loc (id, typ) = mke ~loc typ & Var id
let mklet ~loc p t1 t2 = mke ~loc t2.typ & Let (p, t1, t2)
let mkletrec ~loc p t1 t2 = mke ~loc t2.typ & LetRec (p, t1, t2)
let mkunpair ~loc p1 p2 t1 t2 = mke ~loc t2.typ & Unpair (p1, p2, t1, t2)
let mkunit ~loc () = mke ~loc tyUnit (Const Unit)
let mkfun ~loc pvar e = mke ~loc (tyLambda (pvar.typ, e.typ)) & Fun (pvar, e)
let mkfun_tmp ~loc pvar e =
  Attr.add (Attr.Annot "fun_tmp")
  & mke ~loc (tyLambda (pvar.typ, e.typ))
  & Fun (pvar, e)
let mkpair ~loc e1 e2 =
  mke ~loc (tyPair (None, e1.typ, None, e2.typ)) (Pair (e1, e2))

let mkprim ~loc ty name ty' (* type of expr *) args =
  match List.assoc_opt name Primitives.primitives with
  | None -> Error.raisef Primitive ~loc "Unknown primitive SCaml.%s" name
  | Some _ -> mke ~loc ty (Prim (name, ty', args))

let mkfst ~loc e =
  let ty =
    match e.typ.desc with TyPair (_, ty, _, _) -> ty | _ -> assert false
  in
  mkprim ~loc ty "fst" (tyLambda (e.typ, ty)) [e]

let mksnd ~loc e =
  let ty =
    match e.typ.desc with TyPair (_, _, _, ty) -> ty | _ -> assert false
  in
  mkprim ~loc ty "snd" (tyLambda (e.typ, ty)) [e]

let mkleft ~loc ty e = mke ~loc (tyOr (None, e.typ, None, ty)) (Left e)
let mkright ~loc ty e = mke ~loc (tyOr (None, ty, None, e.typ)) (Right e)

let mkint ~loc n = mke ~loc tyInt (Const (M.Constant.Int (Z.of_int n)))
let mkcons ~loc h t = mke ~loc t.typ (Cons (h, t))
let mksome ~loc t = mke ~loc (tyOption (None, t.typ)) (IML_Some t)
let mkassert ~loc t = mke ~loc tyUnit & Assert t
let mkassertfalse ~loc ty = mke ~loc ty & AssertFalse

let mkeq ~loc e1 e2 =
  mkprim
    ~loc
    tyBool
    "(=)"
    (tyLambda (e1.typ, tyLambda (e2.typ, tyBool)))
    [e1; e2]
