(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* OCaml Typedtree to IML  *)

val primitive :
  loc:Location.t -> Michelson.Type.t -> string -> IML.t list -> IML.desc

val contract_self_id : Ident.t

module Lenv : sig
  type t

  val empty : int (* fun_level *) -> t

  val add_locals : Ident.t list -> t -> t

  val add_values : (Path.t * Ident.t) list -> t -> t

  val into_fun : loc:Location.t -> t -> t

  val pp : Format.formatter -> t -> unit
end

module TopLet : sig
  (** [() [@scaml_placeholder]], to be replaced later by [replace_top_let_placeholder] *)
  val top_let_placeholder : IML.t

  (** [replace_top_let_placeholder e ~by] replaces [() [@scaml_placeholder]]
      in [e] by [by]. *)
  val replace_top_let_placeholder : IML.t -> by:IML.t -> IML.t
end

val expression : Lenv.t -> Typedtree.expression -> IML.t

type res = {
  globals : IML.PatVar.t list;
  (* ids in modules and how they are exported to the global name space *)
  locals : (Path.t * Ident.t) list;
  (* Declarations in the form `let p = body in unit [@scaml_placeholder]` *)
  code : IML.t list;
}

val pp_res : Format.formatter -> res -> unit

val structure :
  Lenv.t ->
  Path.t (* name of the structure, including the compilation unit name *) ->
  Typedtree.structure ->
  Lenv.t * res
