(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019-2021  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Asttypes
open Untyped
open Typedtree
open Tools

open Translate

module M = Michelson
open M.Type

open IML

let noloc = Location.none

let create_ident s = Ident.create_local s

(* M.N.x  =>  gM__N__x *)
let flatten_global_path p =
  let rec f = function
    | Path.Pdot (p, n) -> f p ^ "__" ^ n
    | Pident id -> Ident.name id
    | _ -> assert false
  in
  (* We need it persistent with stamp 0,
     since the same id must created at multiple places *)
  Ident.create_persistent & ("g" ^ f p)

(* [@entry] or [@view] *)
let attr_has_entry_point_or_view =
  let open Parsetree in
  List.find_map_opt (function
      | {
          attr_name = {txt = ("entry" | "view") as txt; loc};
          attr_payload = payload;
          _;
        } -> (
          let is_view = txt = "view" in
          match Attribute.parse_options_in_payload txt ~loc payload with
          | _ :: _ :: _ ->
              Error.raisef
                Entry
                ~loc
                "@@%s cannot specify more than one options"
                txt
          | [] -> Some (loc, None, is_view)
          | [
           Attribute.
             {
               ident = Longident.Lident "name";
               value = `Constant (Pconst_string (s, _, _));
             };
          ] ->
              Some (loc, Some s, is_view)
          | [{ident = Longident.Lident "name"}] ->
              Error.raisef Entry ~loc "@@%s can take only a string literal" txt
          | [_] ->
              Error.raisef
                Entry
                ~loc
                "@@%s can take at most one name=<name> binding"
                txt)
      | _ -> None)

module Raw_entry = struct
  (* Non translated entry point *)
  type t = {
    path : Path.t;
    (* Used to query value's type in Env.t.  Ex. Module.p *)
    ident : Ident.t;
    (* Flattened with the file name: File__Module__p *)
    local_ident : Ident.t;
    (* p *)
    name : string;
    (* [@entry name="XXX"] otherwise Ident.name ident *)
    pat : Typedtree.pattern;
  }
end

module Raw_view = struct
  (* Non translated view *)
  type t = {
    path : Path.t;
    (* Used to query value's type in Env.t.  Ex.  File.Module.p *)
    ident : Ident.t;
    (* Flatttened: File__Module__p *)
    local_ident : Ident.t;
    (* p *)
    name : string;
    (* [@view name="XXX"] otherwise Ident.name ident *)
    pat : Typedtree.pattern;
  }
end

module Raw_contract = struct
  (* Non translated entry points and views in a module *)
  type t = {
    name : string;
    global_path : Path.t;
    module_final_env : Env.t;
    entries : Raw_entry.t list;
    views : Raw_view.t list;
    loc : Location.t; (* Location of the structure *)
  }

  (* Extraction of entry point modules of a compilation unit
     The ordering is important for (module M)
  *)
  let get ~loc fname mpath str =
    let rec structure_item default_contract_name mpath_global mpath_ocaml
        (rev_vals, rev_entms) {str_desc; _} =
      match str_desc with
      | Tstr_value (Nonrecursive, vbs) ->
          let ps =
            List.filter_map
              (fun vb ->
                match
                  attr_has_entry_point_or_view
                  & Untyped.Migrate.copy_attributes vb.vb_attributes
                with
                | None -> None
                | Some (_, name, false (* entry *)) -> begin
                    match vb.vb_pat.pat_desc with
                    | Tpat_any ->
                        Error.raisef
                          Entry
                          ~loc:vb.vb_pat.pat_loc
                          "Entrypoint must have a name"
                    | Tpat_var (id, _) ->
                        let p =
                          match mpath_ocaml with
                          | None -> Path.Pident id
                          | Some p -> Path.Pdot (p, Ident.name id)
                        in
                        let imlid =
                          flatten_global_path
                          & Path.Pdot (mpath_global, Ident.name id)
                        in
                        let name =
                          Option.default name (fun () ->
                              (* imlid is module name prefixed.  not good for the entry name. *)
                              Ident.name id)
                        in
                        Some
                          (`Entry
                            {
                              Raw_entry.path = p;
                              ident = imlid;
                              local_ident = id;
                              name;
                              pat = vb.vb_pat;
                            })
                    | _ ->
                        Error.raisef
                          Entry
                          ~loc:vb.vb_pat.pat_loc
                          "Entrypoint must have a simple variable"
                  end
                | Some (_, name, true (* view *)) -> begin
                    match vb.vb_pat.pat_desc with
                    | Tpat_any ->
                        Error.raisef
                          Entry
                          ~loc:vb.vb_pat.pat_loc
                          "View must have a name"
                    | Tpat_var (id, _) ->
                        let p =
                          match mpath_ocaml with
                          | None -> Path.Pident id
                          | Some p -> Path.Pdot (p, Ident.name id)
                        in
                        let imlid =
                          flatten_global_path
                          & Path.Pdot (mpath_global, Ident.name id)
                        in
                        let name =
                          Option.default name (fun () ->
                              (* imlid is module name prefixed.  not good for the entry name. *)
                              Ident.name id)
                        in
                        Some
                          (`View
                            {
                              Raw_view.path = p;
                              ident = imlid;
                              local_ident = id;
                              name;
                              pat = vb.vb_pat;
                            })
                    | _ ->
                        Error.raisef
                          Entry
                          ~loc:vb.vb_pat.pat_loc
                          "View must have a simple variable"
                  end)
              vbs
          in
          List.rev_append ps rev_vals, rev_entms
      | Tstr_module {mb_id = None; mb_loc; _} ->
          Error.warnf Unsupported ~loc:mb_loc "unnamed module is not supported" ;
          rev_vals, rev_entms
      | Tstr_module ({mb_id = Some mb_id} as mb) ->
          let mname = Ident.name mb_id in
          let default_contract_name = default_contract_name ^ "." ^ mname in
          let mpath_global = Path.Pdot (mpath_global, mname) in
          let mpath_ocaml =
            Some
              (match mpath_ocaml with
              | None -> Path.Pident mb_id
              | Some p -> Path.Pdot (p, mname))
          in
          let rec module_expr me =
            match me.mod_desc with
            | Tmod_structure str ->
                structure
                  ~loc:me.mod_loc
                  default_contract_name
                  mpath_global
                  mpath_ocaml
                  str
            | Tmod_constraint (me, _, _, _) -> module_expr me
            | _ ->
                Error.unsupported
                  ~loc:mb.mb_loc
                  "module declaration other than simple structure"
          in
          let xs = module_expr mb.mb_expr in
          rev_vals, List.rev_append xs rev_entms
      | _ -> rev_vals, rev_entms
    and structure ~loc default_contract_name mpath_global mpath_ocaml
        ({str_items = sitems} as str) =
      (* mpath_global: ex. File.M
         default_contract_name: ex. "file.M" *)
      let attrs =
        List.concat & List.map snd & Attribute.get_scaml_toplevel_attributes str
      in
      let name =
        match
          List.filter_map
            (fun t ->
              match t.Attribute.ident, t.value with
              | Longident.Lident "contract", `Constant (Pconst_string (s, _, _))
                ->
                  Some (t.loc, s)
              | Longident.Lident "contract", _ ->
                  Error.raisef
                    Attribute
                    ~loc:t.loc
                    "key contract must have a string constant value"
              | _ -> None)
            attrs
        with
        | [] -> default_contract_name
        | [(_, n)] -> n
        | (loc, _) :: _ ->
            Error.raisef
              Attribute
              ~loc
              "only 1 contract name is allowed per module"
      in
      let rev_vals, rev_entms =
        List.fold_left
          (fun st sitem ->
            structure_item
              default_contract_name
              mpath_global
              mpath_ocaml
              st
              sitem)
          ([], [])
          sitems
      in
      match rev_vals with
      | [] -> rev_entms
      | _ ->
          let entry_view_list = List.rev rev_vals in
          let rec check_names ns = function
            | [] -> ()
            | `Entry e :: _ when List.mem e.Raw_entry.name ns ->
                Error.raisef
                  Entry
                  "Entry/view name `%s` is already defined"
                  e.name
                  ~loc:e.pat.pat_loc
                (* XXX names can be defined in the entry attribute *)
            | `Entry e :: es -> check_names (e.name :: ns) es
            | `View e :: _ when List.mem e.Raw_view.name ns ->
                Error.raisef
                  Entry
                  "Entry/view name `%s` is already defined"
                  e.name
                  ~loc:e.pat.pat_loc
                (* XXX names can be defined in the view attribute *)
            | `View e :: es -> check_names (e.name :: ns) es
          in
          check_names [] entry_view_list ;
          let entries, views =
            List.partition_map
              (function `Entry e -> `Left e | `View v -> `Right v)
              entry_view_list
          in
          {
            name;
            global_path = mpath_global;
            module_final_env = str.str_final_env;
            entries;
            views;
            loc;
          }
          :: rev_entms
    in
    List.rev & structure ~loc fname mpath None str

  (* OCaml type checking an entry point *)
  let type_check_entry env templ (vdesc : Types.value_description) =
    let unify ty ty' =
      let open Ctype in
      let loc = vdesc.val_loc in
      try unify env ty ty' with
      | Unify trace ->
          Error.wrap_ocaml_exn
            (Typecore.Error (loc, env, Pattern_type_clash (trace, None)))
            Entry_typing
            ~loc
            "Entry point typing error"
      | Tags (l1, l2) ->
          Error.wrap_ocaml_exn
            (Typetexp.Error (loc, env, Typetexp.Variant_tags (l1, l2)))
            Entry_typing
            ~loc
            "Entry point typing error"
      | e ->
          Error.raisef_internal
            ~loc
            "unify raised something unfamiliar: %s"
            (Printexc.to_string e)
    in
    unify vdesc.val_type (* the actual type *) templ
  (* must have this type *)

  (* OCaml type checking a view *)
  let type_check_view env templ (vdesc : Types.value_description) =
    let unify ty ty' =
      let open Ctype in
      let loc = vdesc.val_loc in
      try unify env ty ty' with
      | Unify trace ->
          Error.wrap_ocaml_exn
            (Typecore.Error (loc, env, Pattern_type_clash (trace, None)))
            Entry_typing
            ~loc
            "View typing error"
      | Tags (l1, l2) ->
          Error.wrap_ocaml_exn
            (Typetexp.Error (loc, env, Typetexp.Variant_tags (l1, l2)))
            Entry_typing
            ~loc
            "View typing error"
      | e ->
          Error.raisef_internal
            ~loc
            "unify raised something unfamiliar: %s"
            (Printexc.to_string e)
    in
    unify vdesc.val_type (* the actual type *) templ
  (* must have this type *)

  let type_check em =
    let ty_storage =
      match
        Env.find_type_by_name (Longident.Lident "storage") em.module_final_env
      with
      | exception Not_found -> Ctype.newvar ()
      | path, td -> (
          match td.type_params with
          | _ :: _ -> assert false (* XXX fancy error *)
          | [] -> Ctype.newty (Tconstr (path, [], ref Types.Mnil)))
    in
    let ty_entry () =
      let ty_parameter = Ctype.newvar () in
      let path, _tydecl =
        try
          Env.lookup_type
            ~loc:Location.none
            Longident.(Ldot (Lident "SCaml", "entry"))
            em.module_final_env
        with Not_found ->
          Error.raisef_internal
            ~loc:noloc
            "Type SCaml.entry is not defined.  Something wrong in your SCaml \
             installation."
      in
      ty_parameter, Ctype.newconstr path [ty_parameter; ty_storage]
    in
    let ty_view () =
      let ty_parameter = Ctype.newvar () in
      let ty_return = Ctype.newvar () in
      ( ty_parameter,
        ty_return,
        Ctype.newty
          (Tarrow
             ( Nolabel,
               ty_parameter,
               Ctype.newty (Tarrow (Nolabel, ty_storage, ty_return, Types.commu_var ())),
               Types.commu_var () )) )
    in
    ( List.map
        (fun ({Raw_entry.path = p; local_ident} as entry) ->
          match Env.find_value (Pident local_ident) em.module_final_env with
          | exception Not_found ->
              Error.raisef_internal
                ~loc:noloc
                "type_check_entry_module: %a not found"
                Path.xpp
                p
          | vdesc ->
              let ty_parameter, templ = ty_entry () in
              type_check_entry em.module_final_env templ vdesc ;
              ty_parameter, entry)
        em.entries,
      List.map
        (fun ({Raw_view.path = p; local_ident} as view) ->
          match Env.find_value (Pident local_ident) em.module_final_env with
          | exception Not_found ->
              Error.raisef_internal
                ~loc:noloc
                "type_check_entry_module: %a not found"
                Path.xpp
                p
          | vdesc ->
              let ty_parameter, ty_return, templ = ty_view () in
              type_check_view em.module_final_env templ vdesc ;
              ty_parameter, ty_return, view)
        em.views,
      ty_storage )
end

(* Compute the contract parameter type by combining the types of entry points *)
let global_parameter_ocaml_type tyenv node =
  let path, _tydecl =
    Env.lookup_type
      ~loc:Location.none
      Longident.(Ldot (Lident "SCaml", "sum"))
      tyenv
  in
  let rec f = function
    | Binplace.Leaf (param_ty, _) -> param_ty
    | Branch (n1, n2) ->
        let ty1 = f n1 in
        let ty2 = f n2 in
        Ctype.newconstr path [ty1; ty2]
  in
  f node

(* let __scaml_self [@no_expand] = SELF in t *)
let add_self self_typ t =
  mklet
    ~loc:noloc
    {
      desc = contract_self_id;
      typ = self_typ;
      loc = noloc;
      attrs = [Attr.Annot "no_expand"];
    }
    (mkprim ~loc:noloc self_typ "Contract.self" self_typ [])
    t

let translate_structure modulename str =
  let lenv = Lenv.empty 0 in
  let mpath = Path.Pident (Ident.create_persistent modulename) in
  snd & Translate.structure lenv mpath str

module Entry_tree = struct
  type t = {
    id_param : Ident.t;
    ty_param : M.Type.t;
    id_storage : Ident.t;
    ty_storage : M.Type.t;
    branch_code : IML.t;
  }

  let pp ppf et =
    Format.fprintf
      ppf
      "@[id_param= %a;@ ty_param= %a;@ id_storage= %a;@ ty_storage= %a;@ branch_code= \
       @[%a@]@]"
      Ident.pp
      et.id_param
      M.Type.pp
      et.ty_param
      Ident.pp
      et.id_storage
      M.Type.pp
      et.ty_storage
      IML.pp
      et.branch_code

  (* Translate an entry point tree into one IML code *)
  let translate ty_param ty_storage ty_return node =
    let id_storage = create_ident "storage" in
    let e_storage = mkvar ~loc:noloc (id_storage, ty_storage) in
    let rec f param_id node =
      match node with
      | Binplace.Leaf (_, {Raw_entry.ident = id; pat}) ->
          (* XXX top entry has poor pattern expressivity *)
          (* id, var: name of the entrypoint *)
          let gloc = Location.ghost pat.pat_loc in
          let typ =
            Result.at_Error (fun e ->
                Error.raisef
                  Type_expr
                  ~loc:pat.pat_loc
                  "This pattern has type %a.  %a"
                  Printtyp.type_expr
                  pat.pat_type
                  Transtype.pp_error
                  e)
            & Transtype.type_expr pat.pat_env pat.pat_type
          in
          let var = mkvar ~loc:pat.pat_loc (id, typ) in
          let param_type =
            match var.typ with
            | {desc = TyLambda (t1, _); _} -> t1
            | _ -> assert false
          in
          if not (M.Type.is_parameterable param_type) then
            Error.raisef
              Entry_typing
              ~loc:var.loc
              "The entry point has a contract parameter of %a which is not \
               parameterable.  It cannot contain operation.@."
              M.Type.pp
              param_type ;
          let param_var = mkvar ~loc:noloc (param_id, param_type) in
          (* <var> <param_var> <e_storage> *)
          ( Attr.add (Attr.Comment ("entry " ^ Ident.unique_name id))
            & mke ~loc:gloc ty_return (App (var, [param_var; e_storage])),
            param_type )
      | Branch (n1, n2) ->
          let id_l = create_ident "l" in
          let id_r = create_ident "r" in
          let e_l, param_typ_l = f id_l n1 in
          let e_r, param_typ_r = f id_r n2 in
          let pat_l = mk ~loc:noloc param_typ_l id_l in
          let pat_r = mk ~loc:noloc param_typ_r id_r in
          let param_typ = tyOr (None, param_typ_l, None, param_typ_r) in
          let param_var = mkvar ~loc:noloc (param_id, param_typ) in
          ( mke
              ~loc:noloc
              ty_return
              (Switch_or (param_var, pat_l, e_l, pat_r, e_r)),
            param_typ )
    in
    let id_param = create_ident "global_param" in
    let e, _ty_param = f id_param node in

    (* _ty_param is ty_param without labels.  Should we check the equality? *)

    (* This is the last defence.  We should check it earlier where the locations
       are known. *)
    if not (M.Type.is_parameterable ty_param) then
      Error.raisef
        Entry_typing
        ~loc:noloc
        "Contract's parameter type %a is not parameterable.  It cannot contain \
         operation.@."
        M.Type.pp
        ty_param ;

    {id_param; ty_param; id_storage; ty_storage; branch_code = e}
end

module View = struct
  type t = {
    name : string;
    ident : Ident.t;
    ty_param : M.Type.t;
    ty_return : M.Type.t;
  }

  let pp ppf view =
    Format.fprintf
      ppf
      "@[name= %s;@ ty_param= %a;@ ty_return= %a@]"
      view.name
      M.Type.pp
      view.ty_param
      M.Type.pp
      view.ty_return
end

(* Translated contract *)
type t = {
  (* [@@@SCaml contract="xxx"] *)
  name : string;
  global_path : Path.t;
  loc : Location.t;
  (* ty_param contract *)
  oty_contract : Types.type_expr;
  ty_storage : M.Type.t;
  entry_tree : Entry_tree.t;
  views : View.t list;
}

type contract = t

let pp ppf {name; oty_contract; entry_tree; views} =
  Format.fprintf
    ppf
    "@[name= %a;@ oty_contract= %a;@ entry_tree= @[%a@]@ views= @[%a@]@]"
    Format.string
    name
    Printtyp.type_expr
    oty_contract
    Entry_tree.pp
    entry_tree
    (Format.list ";@ " View.pp)
    views

let put_global_abstraction' ~param:(id_param, ty_param)
    ~storage:(id_storage, ty_storage) e =
  let pat_storage = mk ~loc:noloc ty_storage id_storage in
  (* inserting the definition of self *)
  let e = add_self (tyContract ty_param) e in
  let pat_param = mk ~loc:noloc ty_param id_param in
  let ty_param_storage =
    tyPair (Some "parameter", ty_param, Some "storage", ty_storage)
  in
  let id_param_storage = create_ident "param_storage" in
  let pat_param_storage = mk ~loc:noloc ty_param_storage id_param_storage in
  let e_param_storage = mkvar ~loc:noloc (id_param_storage, ty_param_storage) in
  (*
     fun pat_param_storage ->
       let (param,storage) = pat_param_storage in
       ...
  *)
  mkfun ~loc:noloc pat_param_storage
  & mkunpair ~loc:noloc pat_param pat_storage e_param_storage e

let put_global_abstraction entry_tree e =
  put_global_abstraction'
    ~param:(entry_tree.Entry_tree.id_param, entry_tree.ty_param)
    ~storage:(entry_tree.id_storage, entry_tree.ty_storage)
    e

(* Translate an source entry module *)
let translate_raw_contract em =
  let final_tyenv = em.Raw_contract.module_final_env in
  let ents, views, oty_storage = Raw_contract.type_check em in
  assert (ents <> []) ;
  let tree = Binplace.place ents in
  let oty_param = global_parameter_ocaml_type final_tyenv tree in
  let oty_contract =
    let contract_path, _tydecl =
      try
        Env.lookup_type
          ~loc:Location.none
          Longident.(Ldot (Lident "SCaml", "contract"))
          final_tyenv
      with Not_found ->
        Error.raisef_internal
          ~loc:noloc
          "Type SCaml.contract is not defined.  Something wrong in your SCaml \
           installation."
    in
    Ctype.newty & Types.(Tconstr (contract_path, [oty_param], ref Mnil))
  in
  (* convert to Michelson types *)
  let ty_operations =
    let oty_operations =
      let path, _tydecl =
        Env.lookup_type
          ~loc:Location.none
          Longident.(Ldot (Lident "SCaml", "operations"))
          final_tyenv
      in
      Ctype.newconstr path []
    in
    Result.at_Error (fun e ->
        Error.raisef_internal
          ~loc:em.loc
          "SCaml.operations failed to be converted to Michelson type: %a"
          Transtype.pp_error
          e)
    & Transtype.type_expr final_tyenv oty_operations
  in
  let transtype oty =
    Result.at_Error (fun e ->
        Error.raisef
          Type_expr
          ~loc:em.loc
          "Contract has parameter type %a.  %a"
          Printtyp.type_expr
          oty
          Transtype.pp_error
          e)
    & Transtype.type_expr final_tyenv oty
  in
  let ty_storage =
    let res = transtype oty_storage in
    if not & M.Type.is_storable res then
      Error.raisef
        Entry_typing
        ~loc:em.loc
        "Contract has non storable type %a for the storage.@."
        (* XXX show the original type if the expansion is different from it *)
        Printtyp.type_expr
        (Ctype.full_expand ~may_forget_scope:false final_tyenv oty_storage) ;
    res
  in
  let ty_return =
    tyPair (Some "operations", ty_operations, Some "storage", ty_storage)
  in
  let ty_param =
    let ty_param = transtype oty_param in
    (* decorate ty_param with entry names *)
    let get_field = function
      | Binplace.Leaf (_, {Raw_entry.name}) -> Some name
      | _ -> None
    in
    let rec add_fields (ty, b) =
      match ty.M.Type.desc, b with
      | TyOr (_, ty1, _, ty2), Binplace.Branch (n1, n2) ->
          tyOr
            ( get_field n1,
              add_fields (ty1, n1),
              get_field n2,
              add_fields (ty2, n2) )
      | _, _ -> ty
    in
    add_fields (ty_param, tree)
  in
  let entry_tree = Entry_tree.translate ty_param ty_storage ty_return tree in
  let views =
    List.map
      (fun (opty, orty, t) ->
        let ty_param = transtype opty in
        let ty_return = transtype orty in
        {
          View.ty_param;
          ty_return;
          ident = t.Raw_view.ident;
          name = t.Raw_view.name;
        })
      views
  in
  {
    name = em.name;
    global_path = em.global_path;
    oty_contract;
    ty_storage;
    entry_tree;
    views;
    loc = em.loc;
  }

(* In PPX + dune:

   sourcefile   : "dir/dir/foobar.ml"
   outputprefix : "dir/dir/dune__exe__Foobar"
   modulename   : "Dune__exe__Foobar"
*)
let translate sourcefile _outputprefix modulename str =
  let mpath = Path.Pident (Ident.create_persistent modulename) in
  let ems =
    Raw_contract.get
      ~loc:(Location.in_file sourcefile)
      Filename.(remove_extension (basename sourcefile))
      mpath
      str
  in
  List.map translate_raw_contract ems

(* In PPX + dune:

   sourcefile   : "dir/dir/foobar.ml"
   outputprefix : "dir/dir/dune__exe__Foobar"
   modulename   : "Dune__exe__Foobar"
*)
let implementation sourcefile outputprefix modulename str =
  (* entries are translated twice, into translated_entry_modules and vbs *)
  let translated_entry_modules =
    (* entry points and branch code *)
    translate sourcefile outputprefix modulename str
  in
  let vbs =
    (* values defined in str *)
    translate_structure modulename str
  in
  translated_entry_modules, vbs

(* expand [vbs] in [global_entry]

   let f = ef;;
   let g = eg;;
   global_entry;;

   =>

   global_entry[ef/f, eg/g];;  but no alpha conv
*)
let link global_entry vbs =
  List.fold_right
    (fun toplet x -> Translate.TopLet.replace_top_let_placeholder toplet ~by:x)
    vbs
    global_entry

(* Contract.self's type must be equal to the contract type *)
let check_self {entry_tree = {ty_param}; oty_contract} iml =
  let rec f t =
    match t.desc with
    | Var id
      when (* fragile: contract_self_id is alpha converted here *)
           Ident.name id = Ident.name contract_self_id -> begin
        match t.typ.desc with
        | TyContract ty_param' -> (
            if M.Type.drop_annots ty_param <> M.Type.drop_annots ty_param' then
              match
                List.find_all
                  (function Attr.Type _ -> true | _ -> false)
                  t.attrs
              with
              | [Attr.Type ty] ->
                  Error.raisef
                    Self
                    ~loc:t.loc
                    "This Contract.self has type@ @[%a@]@ but the actual \
                     contract type is@ @[%a@]"
                    !Oprint.out_type
                    ty
                    Printtyp.type_expr
                    oty_contract
              | _ -> assert false (* it must be attached with the type *))
        | _ -> assert false
      end
    | Const _ | Nil | IML_None | Var _ | AssertFalse -> ()
    | IML_Some t
    | Left t
    | Right t
    | Assert t
    | Dealloc (t, _)
    | Dealloc_then (_, t)
    | Fun (_, t) ->
        f t
    | Cons (t1, t2)
    | Pair (t1, t2)
    | IfThenElse (t1, t2, None)
    | Let (_, t1, t2)
    | LetRec (_, t1, t2)
    | Unpair (_, _, t1, t2)
    | Seq (t1, t2) ->
        f t1 ;
        f t2
    | IfThenElse (t1, t2, Some t3)
    | Contract_create (_, _, t1, t2, t3)
    | Switch_or (t1, _, t2, _, t3)
    | Switch_cons (t1, _, _, t2, t3)
    | Switch_none (t1, t2, _, t3) ->
        f t1 ;
        f t2 ;
        f t3
    | Prim (_, _, ts) | Set ts -> List.iter f ts
    | App (t, ts) -> List.iter f (t :: ts)
    | Map tts | BigMap tts ->
        List.iter
          (fun (t1, t2) ->
            f t1 ;
            f t2)
          tts
  in
  f iml

(* Make one Binplaced tree of defined values *)
let connect vbs =
  let es =
    List.map (fun ({desc = id; typ}, _) -> mkvar ~loc:noloc (id, typ)) vbs
  in
  if es = [] then mkunit ~loc:noloc ()
  else
    let t =
      Binplace.fold
        ~leaf:(fun c -> c)
        ~branch:(fun c1 c2 -> mkpair ~loc:noloc c1 c2)
      & Binplace.place es
    in
    let rec f = function
      | [] -> t
      | (p, t) :: vbs -> mklet ~loc:noloc p t (f vbs)
    in
    f vbs

(* convert mode *)
let convert str =
  let attrs =
    List.concat & List.map snd & Attribute.get_scaml_toplevel_attributes str
  in
  Conf.with_scaml_attrs attrs & fun () ->
  let structure_item lenv str_final_env {str_desc; str_loc = loc} =
    match str_desc with
    | Tstr_value (Recursive, _) ->
        Error.unsupported ~loc "recursive definitions"
    | Tstr_primitive _ -> Error.unsupported ~loc "primitive declaration"
    | Tstr_typext _ -> Error.unsupported ~loc "type extension"
    | Tstr_module _ | Tstr_recmodule _ ->
        Error.unsupported ~loc "module declaration"
    | Tstr_class _ -> Error.unsupported ~loc "class declaration"
    | Tstr_class_type _ -> Error.unsupported ~loc "class type declaration"
    | Tstr_include _ -> Error.unsupported ~loc "include"
    | Tstr_modtype _ -> Error.unsupported ~loc "module type declaration"
    | Tstr_eval (e, _) -> [`Value (None, expression lenv e)]
    | Tstr_value (Nonrecursive, vbs) ->
        List.map
          (fun {vb_pat; vb_expr; vb_attributes = _; vb_loc = _loc} ->
            let ido, e =
              match vb_pat.pat_desc with
              | Tpat_var (id, _) -> Some id, vb_expr
              | Tpat_alias ({pat_desc = Tpat_any; pat_loc = _}, id, _) ->
                  Some id, vb_expr
              | Tpat_any -> None, vb_expr
              | _ ->
                  Error.raisef
                    Pattern_match
                    ~loc:vb_pat.pat_loc
                    "Conversion mode does not support complex patterns"
            in
            `Value (ido, expression lenv e))
          vbs
    | Tstr_open _open_description -> []
    | Tstr_exception _ -> [] (* XXX *)
    | Tstr_type (_, tds) ->
        List.map
          (fun td ->
            match td.typ_params with
            | _ :: _ ->
                Error.raisef
                  Type_expr
                  ~loc:td.typ_loc
                  "Conversion mode does not support parameterized type \
                   declarations"
            | [] -> (
                let id = td.typ_id in
                let ty =
                  Btype.newgenty (Tconstr (Path.Pident id, [], ref Types.Mnil))
                in
                match Transtype.type_expr str_final_env ty with
                | Ok x -> `Type (id, x)
                | Error e ->
                    Error.raisef
                      Type_expr
                      ~loc:td.typ_loc
                      "Type %a.  %a"
                      Printtyp.type_expr
                      ty
                      Transtype.pp_error
                      e))
          tds
    | Tstr_attribute _ -> []
  in
  let structure {str_items = sitems; str_final_env} =
    List.concat_map (structure_item (Lenv.empty (-1)) str_final_env) sitems
  in
  structure str
