(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2021  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Spotlib.Spot

type code =
  | Unsupported
  | Type_expr
  | Stdlib
  | Constant
  | Cfold
  | Entry
  | Entry_typing
  | Link
  | Freevar
  | Self
  | Contract
  | Pattern_match
  | Primitive
  | Flags
  | Attribute
  | Convert_ident
  | Installation
  | Internal

val raisef :
  code ->
  loc:Location.t ->
  ('a, Format.formatter, unit, unit, unit, 'b) format6 ->
  'a

val unsupported :
  loc:Location.t -> ('a, Format.formatter, unit, unit, unit, 'b) format6 -> 'a

val raisef_internal :
  loc:Location.t -> ('a, Spotlib.Spot.Format.formatter, unit, 'b) format4 -> 'a

exception Wrapped_OCaml_error of Location.t * string * exn

val wrap_ocaml_exn :
  exn ->
  code ->
  loc:Location.t ->
  ('a, Format.formatter, unit, unit, unit, 'b) format6 ->
  'a

val warnf :
  code -> loc:Location.t -> ('a, Format.formatter, unit, unit) format4 -> 'a
