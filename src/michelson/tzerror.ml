type t = Tezos_error_monad.Error_monad.error list

let pp ppf es =
  Tezos_error_monad.Error_monad.TzTrace.pp_print
    Tezos_error_monad.Error_monad.pp
    ppf es
