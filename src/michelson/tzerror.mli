type t = Tezos_error_monad.Error_monad.error list

val pp : Format.formatter -> t -> unit
