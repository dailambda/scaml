(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2022  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open SCaml_tools

let parse s =
  match Z.of_string s with
  | z -> Ok z
  | exception _ ->
      match Ptime.of_rfc3339 s with
      | Ok (t, _, _) ->
          let t' = Ptime.truncate ~frac_s:0 t in
          if not (Ptime.equal t t') then
            Error "Subsecond is not allowed in timestamps"
          else
            let posix = Ptime.to_float_s t in
            if posix < 0. then Error "Timestamp before Epoch is not allowed"
            else Ok (Z.of_float posix)
      | Error (`RFC3339 (_, e)) ->
          Error (Format.sprintf "%a" Ptime.pp_rfc3339_error e)
