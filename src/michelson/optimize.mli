(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2022 DaiLambda, Inc.                     *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

module Make(_ : sig
    val logger : string -> unit

    (** perform Dijkstra search in order to verify A*'s correctness *)
    val do_dijkstra : bool

    (** default: 1.0 *)
    val search_space_multiplier : float

    (** default: false *)
    val use_lcs_for_astar_score : bool
  end) : sig
  val module_ : Michelson.Module.t -> Michelson.Module.t

  val test : unit -> unit
end
