(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2022  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Michelson

let rec type_ f t =
  let open Type in
  f t;
  match t.desc with
  | TyList t
  | TyOption (_,t)
  | TySet t
  | TyContract t
  | TyTicket t
    -> type_ f t
  | TyPair (_, t1, _, t2)
  | TyOr (_, t1, _, t2)
  | TyMap (t1, t2)
  | TyBigMap (t1, t2)
  | TyLambda (t1, t2)
    -> type_ f t1; type_ f t2
  | TyPairN fts
    -> List.iter (fun (_, t) -> type_ f t) fts
  | TyString
  | TyNat
  | TyInt
  | TyBytes
  | TyBool
  | TyUnit
  | TyMutez
  | TyKeyHash
  | TyTimestamp
  | TyAddress
  | TyChainID
  | TyKey
  | TySignature
  | TyOperation
  | TyNever
  | TyBLS12_381_Fr
  | TyBLS12_381_G1
  | TyBLS12_381_G2
  | TySapling_state _
  | TySapling_transaction _
      -> ()

let rec constant (fty, fc, fo) t =
  let open Constant in
  let opcode = opcode (fty, fc, fo) in
  let rec gs ts = List.iter g ts
  and g t =
    fc t;
    match t with
    | Left t
    | Right t
    | Option (Some t)
      -> g t
    | List ts
    | Set ts
    | PairN ts
      -> gs ts
    | Map tts
      -> List.iter (fun (t1,t2) -> g t1; g t2) tts
    | Pair (t1,t2)
        -> g t1; g t2
    | Unit
    | Bool _
    | Int _
    | String _
    | Bytes _
    | Timestamp _
    | Option None
        -> ()
    | Code ops -> List.iter opcode ops
  in
  g t

and opcode (fty, fc, fo) t =
  let open Opcode in
  let type_ = type_ fty in
  let constant = constant (fty, fc, fo) in
  let module_ = module_ (fty, fc, fo) in
  let rec fs ts = List.iter f ts
  and f t =
    fo t;
    match t.desc with
    | BLOCK ts
    | DIP ts
    | DIPn (_, ts)
    | ITER ts
    | MAP ts
    | LOOP ts
    | LOOP_LEFT ts
      -> fs ts
    | IF (ts1, ts2)
    | IF_NONE (ts1, ts2)
    | IF_LEFT (ts1, ts2)
    | IF_CONS (ts1, ts2)
        -> fs ts1; fs ts2
    | UNPACK ty
    | CAST ty
    | EMPTY_SET ty
    | CONTRACT ty
    | CONTRACT' (ty, _)
    | LEFT ty
    | RIGHT ty
    | NIL ty
    | NONE ty
    | VIEW (_, ty)
        -> type_ ty
    | LAMBDA (ty1, ty2, ts) -> type_ ty1; type_ ty2; fs ts
    | PUSH (ty, c) -> type_ ty; constant c
    | EMPTY_MAP (ty1, ty2)
    | EMPTY_BIG_MAP (ty1, ty2)
      -> type_ ty1; type_ ty2;
    | CREATE_CONTRACT m -> module_ m
    | DUP
    | DIG _
    | DUG _
    | DROP
    | DROPn _
    | SWAP
    | PAIR _
    | CAR
    | CDR
    | CONS
    | SOME
    | COMPARE
    | EQ
    | LT
    | LE
    | GT
    | GE
    | NEQ
    | ADD
    | SUB
    | SUB_MUTEZ
    | MUL
    | EDIV
    | ABS
    | ISNAT
    | NEG
    | LSL
    | LSR
    | AND
    | OR
    | XOR
    | NOT
    | EXEC
    | FAILWITH
    | UNIT
    | APPLY
    | SIZE
    | MEM
    | UPDATE
    | CONCAT
    | SELF _
    | GET
    | RENAME _
    | PACK
    | SLICE
    | TRANSFER_TOKENS
    | SET_DELEGATE
    | CREATE_ACCOUNT
    | IMPLICIT_ACCOUNT
    | NOW
    | AMOUNT
    | BALANCE
    | CHECK_SIGNATURE
    | BLAKE2B
    | SHA256
    | SHA512
    | HASH_KEY
    | STEPS_TO_QUOTA
    | SOURCE
    | SENDER
    | ADDRESS
    | CHAIN_ID
    | INT
    | LEVEL
    | SELF_ADDRESS
    | UNPAIR
    | PAIRING_CHECK
    | NEVER
    | KECCAK
    | SHA3
    | TICKET
    | READ_TICKET
    | SPLIT_TICKET
    | JOIN_TICKETS
    | SAPLING_EMPTY_STATE _
    | SAPLING_VERIFY_UPDATE
    | VOTING_POWER
    | TOTAL_VOTING_POWER
    | GET_AND_UPDATE
    | GETn _
    | UPDATEn _
    | PAIRn _
    | UNPAIRn _
    | DUPn _
      -> ()
  in
  f t

and module_ (fty, fc, fo) m =
  let open Module in
  let opcode = opcode (fty, fc, fo) in
  let view v =
    type_ fty v.View.ty_param;
    type_ fty v.View.ty_return;
    List.iter opcode v.code
  in
  type_ fty m.parameter;
  type_ fty m.storage;
  List.iter opcode m.code;
  List.iter view m.views
