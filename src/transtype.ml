(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019-2021  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Asttypes
open Tools
open Result.Infix

module M = Michelson
open M.Type

type error =
  | Type_variable of Types.type_expr
  | Unsupported_type of Types.type_expr
  | Unsupported_data_type of Path.t
  | Invalid_michelson of M.Type.t * string
  | Exception_out_of_raise
  | GADT of Path.t
  | Not_memo_size of Types.type_expr

let pp_error ppf = function
  | Type_variable ty ->
      Format.fprintf
        ppf
        "Type variable %a is not supported in SCaml.@ Instantiate it with type \
         constraints."
        Printtyp.type_expr
        ty
  | Unsupported_type ty ->
      Format.fprintf
        ppf
        "Type %a is not supported in SCaml."
        Printtyp.type_expr
        ty
  | Unsupported_data_type p ->
      Format.fprintf ppf "Data type %s is not supported in SCaml." (Path.name p)
  | Invalid_michelson (mty, s) ->
      Format.fprintf ppf "Michelson type %a is invalid: %s" M.Type.pp mty s
  | Exception_out_of_raise ->
      Format.fprintf ppf "Values of type exn are only allowed to raise."
  | GADT p ->
      Format.fprintf
        ppf
        "Data type %s is GADT, not supported in SCaml."
        (Path.name p)
  | Not_memo_size ty ->
      Format.fprintf ppf "Type %a is not for memo size." Printtyp.type_expr ty

let encode_by branch xs =
  Binplace.fold ~leaf:(fun x -> x) ~branch & Binplace.place xs

let unify tyenv ty1 ty2 =
  let tr = Printexc.get_backtrace () in
  try Ctype.unify tyenv ty1 ty2
  with e ->
    prerr_endline tr ;
    raise e

let memo_size ty =
  match Types.get_desc ty with
  | Tvariant row_desc ->
      begin match Types.row_fields row_desc with
      | [(l, row_field)] ->
          begin match Types.row_field_repr row_field with
          | Rpresent None ->
              begin match l with
              | "" -> Error (Not_memo_size ty)
              | _ when String.unsafe_get l 0 <> 'n' -> Error (Not_memo_size ty)
              | _ -> (
                  let ns = String.sub l 1 (String.length l - 1) in
                  try
                    let n = int_of_string ns in
                    if string_of_int n <> ns then Error (Not_memo_size ty) else Ok n
                  with _ -> Error (Not_memo_size ty))
              end
          | _ -> Error (Not_memo_size ty)
        end
      | _ -> Error (Not_memo_size ty)
      end
  | _ -> Error (Not_memo_size ty)

let rec type_expr tyenv ty =
  let rec fn tyenv ty =
    let ty = Ctype.expand_head tyenv ty in
    match Types.get_desc ty with
    | Tvar _ when Types.get_level ty = Btype.generic_level -> Error (Type_variable ty)
    | Tvar _ ->
        (* Non generalized type variable.  We are brave enough to unify it with Unit *)
        unify tyenv ty Predef.type_unit ;
        (* must succeed *)
        fn tyenv ty
    | Tarrow (Nolabel, f, t, _) ->
        fn tyenv f >>= fun f ->
        fn tyenv t >>= fun t -> Ok (tyLambda (f, t))
    | Ttuple [t1; t2] ->
        fn tyenv t1 >>= fun t1 ->
        fn tyenv t2 >>= fun t2 -> Ok (tyPair (None, t1, None, t2))
    | Ttuple tys ->
        Result.mapM (fn tyenv) tys
        >>| encode_by (fun ty1 ty2 -> tyPair (None, ty1, None, ty2))
    | Tconstr (p, [], _) when p = Predef.path_bool -> Ok tyBool
    | Tconstr (p, [t], _) when p = Predef.path_list ->
        fn tyenv t >>= fun t -> Ok (tyList t)
    | Tconstr (p, [t], _) when p = Predef.path_option ->
        fn tyenv t >>= fun t -> Ok (tyOption (None, t))
    | Tconstr (p, [], _) when p = Predef.path_unit -> Ok tyUnit
    | Tconstr (p, [], _) when p = Predef.path_string -> Ok tyString
    | Tconstr (p, tys, _) -> (
        let rec f res = function
          | [] -> Ok (List.rev res)
          | ty :: tys -> fn tyenv ty >>= fun ty -> f (ty :: res) tys
        in
        let memo_size tys =
          match tys with [ty] -> memo_size ty | _ -> assert false
        in
        match Path.is_scaml p with
        | Some "sapling_state" ->
            memo_size tys >>= fun ms -> Ok (tySapling_state ms)
        | Some "sapling_transaction" ->
            memo_size tys >>= fun ms -> Ok (tySapling_transaction ms)
        | res -> (
            f [] tys >>= fun tys ->
            match res, tys with
            | Some "sum", [t1; t2] ->
                Ok
                  (Michelson.Type.annotate (fun _ -> Some "sum")
                  & tyOr (None, t1, None, t2))
            | Some "int", [] -> Ok tyInt
            | Some "nat", [] -> Ok tyNat
            | Some "tz", [] -> Ok tyMutez
            | Some "set", [ty] -> Ok (tySet ty)
            | Some "map", [ty1; ty2] -> Ok (tyMap (ty1, ty2))
            | Some "big_map", [ty1; ty2] -> Ok (tyBigMap (ty1, ty2))
            | Some "operation", [] -> Ok tyOperation
            | Some "contract", [ty] -> Ok (tyContract ty)
            | Some "timestamp", [] -> Ok tyTimestamp
            | Some "address", [] -> Ok tyAddress
            | Some "key", [] -> Ok tyKey
            | Some "signature", [] -> Ok tySignature
            | Some "key_hash", [] -> Ok tyKeyHash
            | Some "bytes", [] -> Ok tyBytes
            | Some "chain_id", [] -> Ok tyChainID
            | Some "bls12_381_g1", [] -> Ok tyBLS12_381_G1
            | Some "bls12_381_g2", [] -> Ok tyBLS12_381_G2
            | Some "bls12_381_fr", [] -> Ok tyBLS12_381_Fr
            | Some "ticket", [ty] -> Ok (tyTicket ty)
            | Some "never", [] -> Ok tyNever
            | Some _, _ -> Error (Unsupported_data_type p)
            | None, _ -> (
                match Env.find_type_descrs p tyenv with
                | Type_abstract when p = Predef.path_exn ->
                    Error Exception_out_of_raise
                | Type_abstract -> Error (Unsupported_data_type p) (* abstract XXX *)
                | Type_record (labels, _) -> record_type tyenv ty p labels
                | Type_variant (constrs, _) ->
                    variant_type tyenv ty p constrs >>| fun (_, _, ty) -> ty
                | Type_open -> assert false (* impossible *)
                | exception e ->
                    Printexc.print_backtrace Stdlib.stderr ;
                    prerr_endline (Printexc.to_string e) ;
                    Error (Unsupported_data_type p)) (* abstract XXX *)))
    | Tpoly (ty, []) -> fn tyenv ty
    | _ -> Error (Unsupported_type ty)
  in
  fn tyenv ty >>= fun mty ->
  (* double check type invariants in Michelson *)
  match M.Type.validate (Conf.get_protocol ()) mty with
  | Ok () -> Ok mty
  | Error (mty', mes) -> Error (Invalid_michelson (mty', mes))

and record_type tyenv ty _p labels =
  (* record *)
  let ltys =
    List.map
      (fun label ->
        let _, ty_arg, ty_res =
          Ctype.instance_label false (* XXX I do not know what it is *) label
        in
        unify tyenv ty ty_res ;
        (* XXX should succeed *)
        label.lbl_name, ty_arg)
      labels
  in
  Result.mapM (fun (l, ty) -> type_expr tyenv ty >>| fun ty -> Some l, ty) ltys
  >>| encode_by (fun (f1, ty1) (f2, ty2) -> None, tyPair (f1, ty1, f2, ty2))
  >>| fun (_, ty) ->
  (* M.Type.type_annotate (fun _ -> Some (Path.name p)) (* XXX p=P(Q) fails *) ty *)
  ty

and variant_type tyenv ty p constrs =
  if List.exists (fun c -> c.Types.cstr_generalized) constrs then Error (GADT p)
  else
    let consts, non_consts = Variant.variant_type tyenv ty constrs in
    let non_consts =
      Result.mapM
        (fun (n, tys) ->
           Result.mapM (type_expr tyenv) tys >>| fun tys -> n, tys)
        non_consts
      >>| fun ctys_list ->
      List.map
        (fun (n, tys) ->
           n, encode_by (fun ty1 ty2 -> tyPair (None, ty1, None, ty2)) tys)
        ctys_list
    in
    non_consts >>| fun non_consts ->
    ( consts,
      non_consts,
      (* M.Type.type_annotate (fun _ -> Some (Path.name p)) (* XXX p=P(Q) fails *)
         & *) snd
      & encode_by (fun (f1, ty1) (f2, ty2) -> None, tyOr (f1, ty1, f2, ty2))
      & List.map
          (fun (f, ty) -> Some f, ty)
          ((match consts with
           | None -> []
           | Some names -> [String.concat "_" (List.map String.uncapitalize_ascii names), tyInt])
           @ List.map (fun (n,ty) -> String.uncapitalize_ascii n, ty) non_consts) )
