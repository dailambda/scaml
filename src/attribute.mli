(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** Handles OCaml atttributes *)

type t = {
  ident : Longident.t;
  loc : Location.t;
  value : [`Bool of bool | `Constant of Ppxlib.Parsetree.constant];
}

(** Parse attributes.  Possible form is:

    ident = const , ident = const ...

    const can be an OCaml constant or true or false.
*)
val parse_options_in_payload :
  loc:Tools.Location.t -> string -> Ppxlib.Parsetree.payload -> t list

val get_scaml_toplevel_attributes :
  Typedtree.structure -> (Location.t * t list) list

(** Reject [@@@SCaml] in unsupported module constructs *)
val reject_SCaml_attribute_in_complex_structure : Typedtree.structure -> unit

(** Extract module constructs with [@@@SCaml] *)
val filter_by_SCaml_attribute : Typedtree.structure -> Typedtree.structure
