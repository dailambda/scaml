(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2020  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open SCaml_tools

(* Hack for PPX *)

let rev_modules = ref []

let register s =
  (* The string is created by [preprocess] of ppx/main.ml *)
  let module_ : SCamlComp.Module.t = Marshal.from_string s 0 in
  rev_modules := module_ :: !rev_modules

let emit name =
  try
    let modules = List.rev !rev_modules in
    SCamlComp.link_specified_module "." modules name
  with
  | Location.Error error ->
      let report_printer = Location.best_toplevel_printer () in
      Format.eprintf "%a@." (report_printer.pp report_printer) error; exit 1
  | e -> Format.eprintf "%a@." Location.report_exception e; exit 1
