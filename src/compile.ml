(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Tools

module M = Michelson
open M.Type
open M.Opcode

module type Config = sig
  val allow_big_map : bool
end

module Env : sig
  type t = (Ident.t * M.Type.t) list
  val find : Ident.t -> t -> (int * M.Type.t) option
  val find_and_remove : Ident.t -> t -> (int * M.Type.t * t) option
  val pp : Format.formatter -> t -> unit [@@ocaml.warning "-32"]
end = struct
  type t = (Ident.t * M.Type.t) list

  let find id env =
    let rec aux n = function
      | [] -> None
      | (id', ty) :: _ when id = id' -> Some (n, ty)
      | _ :: env -> aux (n + 1) env
    in
    aux 0 env

  let find_and_remove id env =
    let rec aux rev_env n = function
      | [] -> None
      | (id', ty) :: env when id = id' ->
          Some (n, ty, List.rev_append rev_env env)
      | ent :: env -> aux (ent :: rev_env) (n + 1) env
    in
    aux [] 0 env

  let pp ppf t =
    Format.fprintf
      ppf
      "@[<2>[ %a ]@]"
      (Format.list ";@ " (fun ppf (id, ty) ->
           Format.fprintf ppf "(%s : %a)" (Ident.unique_name id) M.Type.pp ty))
      t
end

let iname = Ident.unique_name

let ( |// ) x y = append_comments x [y]

(* Copy a value of the identifier from the deep of the stack to its top. *)
let var ~loc env id =
  match Env.find id env with
  | None -> Error.raisef_internal ~loc "Variable not found: %s" (iname id)
  | Some (_, typ) when not & M.Type.is_dupable typ -> assert false
  | Some (0, typ) ->
      let id' = Ident.dummy (Ident.name id) in
      ( [_DUP |// Printf.sprintf "let %s = %s" (iname id') (iname id)],
        (id', typ) :: env )
  | Some (n, typ) ->
      (* XXX only for >= 008 *)
      (* old [ COMMENT( "var " ^ iname id, [ DIG n; DUP; DUG (n+1) ]) ] *)
      let id' = Ident.dummy (Ident.name id) in
      ( [_DUPn (n + 1) |// Printf.sprintf "let %s = %s" (iname id') (iname id)],
        (id', typ) :: env )

let dealloc ~loc:_ env id =
  match Env.find_and_remove id env with
  | None ->
      Format.eprintf "%a not found@." Ident.pp id ;
      assert false (* XXX internal error *)
  | Some (0, _, env) -> [_DROP |// "free " ^ iname id], env
  | Some (1, _, env) -> [_DIP [_DROP] |// "free " ^ iname id], env
  | Some (n, _, env) -> [_DIPn (n, [_DROP]) |// "free " ^ iname id], env

let var_with_dealloc ~loc env id =
  match Env.find_and_remove id env with
  | None ->
      Error.raisef_internal
        ~loc
        "(var_with_dealloc) Variable not found: %s"
        (iname id)
  | Some (0, ty, env) -> [], (id, ty) :: env
  | Some (n, ty, env) ->
      (* we can safely reuse the id *)
      let id' = id in
      [_DIG n |// "pop " ^ iname id'], (id', ty) :: env

let compiled_modules = ref ([] : (Path.t * Michelson.Module.t) list)

module Make (Config : Config) = struct
  let takeids n env = List.map fst & List.take n env

  let take2ids env =
    match env with (id1, _) :: (id2, _) :: _ -> id1, id2 | _ -> assert false

  let takeid env = match env with (id1, _) :: _ -> id1 | _ -> assert false

  let rec compile env t =
    let os, env = desc env t in
    let comments =
      List.filter_map
        (function
          | IML.Attr.Comment s -> Some s
          | _ -> None)
        t.IML.attrs
    in
    (* environment is hard to read and lousy *)
    (* let env_doc = [Format.asprintf "env: %a" Env.pp env] in *)
    let env_doc = [] in
    match comments with
    | [] -> append_comments_at_last os env_doc, env
    | _ -> append_comments_at_last os (comments @ env_doc), env

  and desc env t =
    let loc = t.IML.loc in
    let dummy name ty = Ident.dummy & Varname.create name ty in
    let prim_default n ty ts =
      (* XXX if the args are pure and the prim is commutative,
         we may try to swap the arguments to have the shorter code. *)
      match List.assoc_opt n Primitives.primitives with
      | None -> assert false
      | Some (_pure, arity, f) ->
          assert (arity = List.length ts) ;
          let os, env = args env ts in
          let ids = takeids arity env in
          let id' = dummy "result" t.typ in
          let env = (id', t.typ) :: List.drop arity env in
          ( os
            @ [
                (let xs = f ~loc:t.IML.loc ty in
                 let x = match xs with [x] -> x | _ -> _BLOCK xs in
                 x
                 |// Printf.sprintf
                       "let %s = %s %s"
                       (iname id')
                       n
                       (String.concat " " & List.map iname ids));
              ],
            env )
    in
    match t.IML.desc with
    | IML.Set _ -> Error.raisef Constant ~loc "Set elements must be constants"
    | Map _ -> Error.raisef Constant ~loc "Map bindings must be constants"
    | BigMap _ ->
        Error.raisef
          Constant
          ~loc
          "BigMap constructor is not allowed in smart contracts"
    | Const Unit ->
        let id' = dummy "unit" t.typ in
        ( [_UNIT |// Printf.sprintf "let %s = ()" (iname id')],
          (id', t.typ) :: env )
    | Nil | Const (List []) ->
        let ty = match t.typ.desc with TyList ty -> ty | _ -> assert false in
        let id' = dummy "nil" t.typ in
        ( [_NIL ty |// Printf.sprintf "let %s = []" (iname id')],
          (id', t.typ) :: env )
    | Cons (t1, t2) ->
        (* evaluate tl first *)
        let os2, env = compile env t2 in
        let id2, _ = List.hd env in
        let os1, env = compile env t1 in
        let id1 = takeid env in
        let id' = dummy "cons" t.typ in
        ( os2 @ os1
          @ [
              _CONS
              |// Printf.sprintf
                    "let %s = %s :: %s"
                    (iname id')
                    (iname id1)
                    (iname id2);
            ],
          (id', t.typ) :: List.drop 2 env )
    | IML_None | Const (Option None) ->
        let ty =
          match t.IML.typ.desc with TyOption (_, ty) -> ty | _ -> assert false
        in
        let id' = dummy "none" t.typ in
        ( [_NONE ty |// Printf.sprintf "let %s = None" (iname id')],
          (id', t.typ) :: env )
    | IML_Some t1 ->
        let os1, env = compile env t1 in
        let id1 = takeid env in
        let id' = dummy "some" t.typ in
        ( os1
          @ [
              _SOME |// Printf.sprintf "let %s = Some %s" (iname id') (iname id1);
            ],
          (id', t.typ) :: List.drop 1 env )
    | Const c ->
        if is_pushable t.typ then
          let id' = dummy "const" t.typ in
          ( [
              _PUSH (t.typ, c)
              |// Format.asprintf
                    "let %s = %a"
                    (iname id')
                    M.Constant.pp
                    c;
            ],
            (id', t.typ) :: env )
        else
          Error.raisef_internal ~loc
            "@[<2>Constant of non pushable type is now allowed:@ %a@]" M.Type.pp t.typ
    | Left t' ->
        let ty =
          match t.typ.desc with TyOr (_, _, _, ty) -> ty | _ -> assert false
        in
        let os, env = compile env t' in
        let id1 = takeid env in
        let id' = dummy "left" t.typ in
        ( os
          @ [
              _LEFT ty
              |// Printf.sprintf "let %s = Left %s" (iname id') (iname id1);
            ],
          (id', t.typ) :: List.drop 1 env )
    | Right t' ->
        let ty =
          match t.typ.desc with TyOr (_, ty, _, _) -> ty | _ -> assert false
        in
        let os, env = compile env t' in
        let id1 = takeid env in
        let id' = dummy "right" t.typ in
        ( os
          @ [
              _RIGHT ty
              |// Printf.sprintf "let %s = Right %s" (iname id') (iname id1);
            ],
          (id', t.typ) :: List.drop 1 env )
    | Dealloc (({desc = Var id} as t), id') when id = id' ->
        var_with_dealloc ~loc:t.loc env id
    | Dealloc (t, id) ->
        let os, env = compile env t in
        let os2, env = dealloc ~loc:t.IML.loc env id in
        os @ os2, env
    | Dealloc_then (id, t) ->
        let os1, env = dealloc ~loc:t.IML.loc env id in
        let os2, env = compile env t in
        os1 @ os2, env
    | Var id -> var ~loc env id
    | Pair (t1, t2) ->
        let os2, env = compile env t2 in
        let id2 = fst & List.hd env in
        let os1, env = compile env t1 in
        let id1 = takeid env in
        let id' = dummy "pair" t.typ in
        let env = (id', t.typ) :: List.drop 2 env in
        ( os2 @ os1
          @ [
              _PAIR []
              |// Printf.sprintf
                    "let %s = (%s, %s)"
                    (iname id')
                    (iname id1)
                    (iname id2);
            ],
          env )
    | Seq (t1, t2) ->
        let os1, env = compile env t1 in
        let env = List.drop 1 env in
        let os2, env = compile env t2 in
        os1 @ [_DROP |// ";"] @ os2, env
    | Assert t ->
        let os, env = compile env t in
        let id1 = fst & List.hd env in
        ( os @ [_ASSERT |// Printf.sprintf "assert %s" (iname id1); _UNIT],
          (Ident.dummy "assert", tyUnit) :: List.drop 1 env )
    | AssertFalse ->
        ( [_UNIT; _FAILWITH |// "assert false"],
          (Ident.dummy "assert", t.IML.typ) :: env )
    | IfThenElse (t1, t2, Some t3) ->
        let oif, env = compile env t1 in
        let id1 = fst & List.hd env in
        let env = List.drop 1 env in
        let othen, env' = compile env t2 in
        let oelse, _env'' = compile env t3 in
        (* XXX env' and _env'' must be equal *)
        oif @ [_IF (othen, oelse) |// Printf.sprintf "if %s" (iname id1)], env'
    | IfThenElse (t1, t2, None) ->
        let oif, env = compile env t1 in
        let id1 = fst & List.hd env in
        let env = List.drop 1 env in
        let othen, env = compile env t2 in
        if t2.typ.desc = TyUnit then
          ( oif @ [_IF (othen, [_UNIT]) |// Printf.sprintf "if %s" (iname id1)],
            env )
        else
          (* I think this is never called *)
          let othen, env = compile env t2 in
          let env = (dummy "then" tyUnit, tyUnit) :: List.drop 1 env in
          ( oif
            @ [
                _IF (othen @ [_DROP; _UNIT], [_UNIT])
                |// Printf.sprintf "if %s" (iname id1);
              ],
            env )
    | Prim
        ( "Contract.contract'",
          ty,
          [address; {desc = IML.Const (M.Constant.String entry)}] ) ->
        let os, env = compile env address in
        let id1 = fst & List.hd env in
        let id' = dummy "contract" t.typ in
        ( os
          @ [
              Primitives.contract' entry ~loc ty
              |// Printf.sprintf
                    "let %s = Contract.contract' %s"
                    (iname id')
                    (iname id1);
            ],
          (id', t.typ) :: List.drop 1 env )
    | Prim
        ( "view",
          _ty,
          [address; {desc = IML.Const (M.Constant.String name)}; param] ) ->
        let ty =
          match t.typ.desc with TyOption (_, ty) -> ty | _ -> assert false
        in
        let os1, env = compile env address in
        let os2, env = compile env param in
        let id1, id2 = take2ids env in
        let id' = dummy "view" t.typ in
        ( os1 @ os2
          @ [
              _VIEW (name, ty)
              |// Printf.sprintf
                    "let %s = view %s %S %s"
                    (iname id')
                    (iname id2)
                    name
                    (iname id1);
            ],
          (id', t.typ) :: List.drop 2 env )
    | Prim ("List.map", _ty, [{desc = Fun (pv, body)}; t]) ->
        (*
           # xs : _
           MAP [  #  x : _
              body  # y : _
           ]
        *)
        let os1, env1 = args env [t] in
        let env = (pv.desc, pv.typ) :: List.drop 1 env1 in
        let os2, _env2 = compile env body in
        let id' = dummy "map_res" t.typ in
        let env_final = (id', t.typ) :: List.drop 1 env1 in
        ( os1
          @ [
              _MAP os2
              |// Printf.sprintf
                    "let %s = List.map (..) %s"
                    (iname id')
                    (iname pv.desc);
            ],
          env_final )
    | Prim ("List.fold_left'", _ty, [{desc = Fun (pv, body)}; init; xs]) ->
        let os1, env1 = args env [init; xs] in
        let id1, id2 = take2ids env1 in
        let env = (pv.desc, pv.typ) :: List.drop 2 env1 in
        let os2, _env2 = compile env body in
        let id' = dummy "fold_res" t.typ in
        let env_final = (id', t.typ) :: List.drop 2 env1 in
        (*
           # init : xs : _
           SWAP  # xs : init : _
           ITER [  #  x : acc(=init) : _
              SWAP  #  acc, x : _
              PAIR  #  (acc, x) : _
              body  # acc : _
           ]
        *)
        ( os1
          @ [
              _BLOCK [_SWAP; _ITER (_SWAP :: _PAIR [] :: os2)]
              |// Printf.sprintf
                    "let %s = List.fold_left' (..) %s %s"
                    (iname id')
                    (iname id1)
                    (iname id2);
            ],
          env_final )
    | Prim
        ( "List.fold_left",
          _ty,
          [{desc = Fun (pv, {desc = Fun (pv', body)})}; init; xs] ) ->
        let os1, env1 = args env [init; xs] in
        let id1, id2 = take2ids env1 in
        let env =
          (pv.desc, pv.typ) :: (pv'.desc, pv'.typ) :: List.drop 2 env1
        in
        let os2, _env2 = compile env body in
        let id' = dummy "fold_res" t.typ in
        let env_final = (id', t.typ) :: List.drop 2 env1 in
        (*
           # init : xs : _
           SWAP; # xs : init : _
           ITER [  #  x : acc(=init) : _
              SWAP  # acc, x : _
              body  # acc : _
           ]
        *)
        ( os1
          @ [
              _BLOCK [_SWAP; _ITER (_SWAP :: os2)]
              |// Printf.sprintf
                    "let %s = List.fold_left (..) %s %s"
                    (iname id')
                    (iname id1)
                    (iname id2);
            ],
          env_final )
    | Prim ("Set.fold'", _ty, [{desc = Fun (pv, body)}; xs; init]) ->
        let os1, env1 = args env [xs; init] in
        let id1, id2 = take2ids env1 in
        let env = (pv.desc, pv.typ) :: List.drop 2 env1 in
        let os2, _env2 = compile env body in
        let id' = dummy "fold_res" t.typ in
        let env_final = (id', t.typ) :: List.drop 2 env1 in
        (*
           # xs : init : _
           ITER [  #  x : acc(=init) : _
              PAIR  #  (x, acc) : _
              body  # acc : _
           ]
        *)
        ( os1
          @ [
              _ITER (_PAIR [] :: os2)
              |// Printf.sprintf
                    "let %s = Set.fold' (..) %s %s"
                    (iname id')
                    (iname id1)
                    (iname id2);
            ],
          env_final )
    | Prim
        ( "Set.fold",
          _ty,
          [{desc = Fun (pv, {desc = Fun (pv', body)})}; xs; init] ) ->
        let os1, env1 = args env [xs; init] in
        let id1, id2 = take2ids env1 in
        let env =
          (pv.desc, pv.typ) :: (pv'.desc, pv'.typ) :: List.drop 2 env1
        in
        let os2, _env2 = compile env body in
        let id' = dummy "fold_res" t.typ in
        let env_final = (id', t.typ) :: List.drop 2 env1 in
        (*
           # xs : init : _
           ITER [  #  x : acc(=init) : _
              body  # acc : _
           ]
        *)
        ( os1
          @ [
              _ITER os2
              |// Printf.sprintf
                    "let %s = Set.fold (..) %s %s"
                    (iname id')
                    (iname id1)
                    (iname id2);
            ],
          env_final )
    | Prim ("Map.fold'", _ty, [{desc = Fun (pv, body)}; xs; init]) ->
        let os1, env1 = args env [xs; init] in
        let id1, id2 = take2ids env1 in
        let env = (pv.desc, pv.typ) :: List.drop 2 env1 in
        let os2, _env2 = compile env body in
        let id' = dummy "fold_res" t.typ in
        let env_final = (id', t.typ) :: List.drop 2 env1 in
        (*
           # xs : init : _
           ITER [  #  (k, v) : acc(=init) : _
              UNPAIR  k, v, acc : _
              DIP { PAIR }  #  k : (v, acc) : _
              PAIR  #  (k, (v, acc)) : _
              body  # acc : _
           ]
        *)
        ( os1
          @ [
              _ITER (_UNPAIR :: _DIP [_PAIR []] :: _PAIR [] :: os2)
              |// Printf.sprintf
                    "let %s = Map.fold' (..) %s %s"
                    (iname id')
                    (iname id1)
                    (iname id2);
            ],
          env_final )
    | Prim
        ( "Map.fold",
          _ty,
          [
            {desc = Fun (kv, {desc = Fun (vv, {desc = Fun (av, body)})})};
            xs;
            init;
          ] ) ->
        let os1, env1 = args env [xs; init] in
        let id1, id2 = take2ids env1 in
        let env =
          (kv.desc, kv.typ) :: (vv.desc, vv.typ) :: (av.desc, av.typ)
          :: List.drop 2 env1
        in
        let os2, _env2 = compile env body in
        let id' = dummy "fold_res" t.typ in
        let env_final = (id', t.typ) :: List.drop 2 env1 in
        (*
           # map : init : _
           ITER [  #  (k, v) : acc(=init) : _
              UNPAIR # k : v : acc
              body   # acc : _
           ]
        *)
        ( os1
          @ [
              _ITER (_UNPAIR :: os2)
              |// Printf.sprintf
                    "let %s = Map.fold (..) %s %s"
                    (iname id')
                    (iname id1)
                    (iname id2);
            ],
          env_final )
    | Prim ("Map.map'", _ty, [{desc = Fun (pv, body)}; xs]) ->
        let os1, env1 = args env [xs] in
        let id1 = takeid env1 in
        let env = (pv.desc, pv.typ) :: List.drop 1 env1 in
        let os2, _env2 = compile env body in
        let id' = dummy "map_res" t.typ in
        let env_final = (id', t.typ) :: List.drop 1 env1 in
        (*
           # xs : _
           MAP [  #  (k, v) : acc(=init) : _
              body  # acc : _
           ]
        *)
        ( os1
          @ [
              _MAP os2
              |// Printf.sprintf
                    "let %s = Map.map' (..) %s"
                    (iname id')
                    (iname id1);
            ],
          env_final )
    | Prim ("Map.map", _ty, [{desc = Fun (kv, {desc = Fun (vv, body)})}; xs]) ->
        let os1, env1 = args env [xs] in
        let id1 = takeid env1 in
        let env = (kv.desc, kv.typ) :: (vv.desc, vv.typ) :: List.drop 1 env1 in
        let os2, _env2 = compile env body in
        let id' = dummy "map_res" t.typ in
        let env_final = (id', t.typ) :: List.drop 1 env1 in
        (*
           # map : _
           MAP [  #  (k, v) : _
              UNPAIR # k : v : acc
              body   # acc : _
           ]
        *)
        ( os1
          @ [
              _MAP (_UNPAIR :: os2)
              |// Printf.sprintf
                    "let %s = Map.map (..) %s"
                    (iname id')
                    (iname id1);
            ],
          env_final )
    | Prim ("Option.map", _ty, [{desc = Fun (v, body)}; x]) ->
        let os1, env1 = args env [x] in
        let id1 = takeid env1 in
        let env = (v.desc, v.typ) :: List.drop 1 env1 in
        let os2, _env2 = compile env body in
        let id' = dummy "map_res" t.typ in
        let env_final = (id', t.typ) :: List.drop 1 env1 in
        (*
           # map : _
           MAP [  #  v : _
              body   # acc : _
           ]
        *)
        ( os1
          @ [
              _MAP os2
              |// Printf.sprintf
                    "let %s = Option.map (..) %s"
                    (iname id')
                    (iname id1);
            ],
          env_final )
    | Prim ("Loop.left", _ty, [{desc = Fun (kv, body)}; x]) ->
        let os1, env1 = args env [x] in
        let id1 = takeid env1 in
        let env = (kv.desc, kv.typ) :: List.drop 1 env1 in
        let os2, _env2 = compile env body in
        let id' = dummy "loop_res" t.typ in
        let env_final = (id', t.typ) :: List.drop 1 env1 in
        ( os1
          @ [
              _BLOCK [_LEFT t.typ; _LOOP_LEFT os2]
              |// Printf.sprintf
                    "let %s = Loop.left (..) %s"
                    (iname id')
                    (iname id1);
            ],
          env_final )
    | Prim (n, ty, ts) -> prim_default n ty ts
    | Let (pat, t1, t2) ->
        let os1, env = compile env t1 in
        let id = takeid env in
        let env = (pat.desc, pat.typ) :: List.drop 1 env in
        let os2, env = compile env t2 in
        ( append_comments_at_last
            os1
            [Printf.sprintf "let %s = %s" (iname pat.desc) (iname id)]
          @ os2,
          env )
    | LetRec _ -> Error.unsupported ~loc "non tail rec function"
    | Unpair (pat1, pat2, t1, t2) ->
        (* XXX This is only valid from 008 *)
        let os1, env = compile env t1 in
        let id = takeid env in
        let env =
          (pat1.desc, pat1.typ) :: (pat2.desc, pat2.typ) :: List.drop 1 env
        in
        let os2, env = compile env t2 in
        ( os1
          @ (_UNPAIR
            |// Printf.sprintf
                  "let (%s, %s) = %s"
                  (iname pat1.desc)
                  (iname pat2.desc)
                  (iname id))
            :: os2,
          env )
    | Switch_or (t, p1, t1, p2, t2) ->
        let os, env = compile env t in
        let id1 = takeid env in
        let env = List.drop 1 env in
        let os1, env' = compile ((p1.desc, p1.typ) :: env) t1 in
        let os2, _env'' = compile ((p2.desc, p2.typ) :: env) t2 in
        (* XXX env' and env'' must be equal *)
        ( os
          @ [
              _IF_LEFT (os1, os2)
              |// Printf.sprintf
                    "match %s with Left %s -> .. | Right %s -> .."
                    (iname id1)
                    (iname p1.desc)
                    (iname p2.desc);
            ],
          env' )
    | Switch_cons (t, p1, p2, t1, t2) ->
        let os, env = compile env t in
        let id1 = takeid env in
        let env = List.drop 1 env in
        let os1, env' =
          compile ((p1.desc, p1.typ) :: (p2.desc, p2.typ) :: env) t1
        in
        let os2, _env'' = compile env t2 in
        (* XXX env' and env'' must be equal *)
        ( os
          @ [
              _IF_CONS (os1, os2)
              |// Printf.sprintf
                    "match %s with %s::%s -> .. | [] -> .."
                    (iname id1)
                    (iname p1.desc)
                    (iname p2.desc);
            ],
          env' )
    | Switch_none (t, t1, p2, t2) ->
        let os, env = compile env t in
        let id1 = takeid env in
        let env = List.drop 1 env in
        let os1, env' = compile env t1 in
        let os2, _env'' = compile ((p2.desc, p2.typ) :: env) t2 in
        (* XXX env' and env'' must be equal *)
        ( os
          @ [
              _IF_NONE (os1, os2)
              |// Printf.sprintf
                    "match %s with None -> .. | Some %s -> .."
                    (iname id1)
                    (iname p2.desc);
            ],
          env' )
    | Fun (p, body) -> (
        let fvars = IML.Idents.elements & IML.freevars t in
        (*
        Format.eprintf "fvars: @[%a@] env: @[%a@]@."
          (Format.list ";@ " (fun ppf (id,ty) ->
               Format.fprintf ppf "%s:%a" (iname id) M.Type.pp ty)) fvars
          (Format.list ";@ " (fun ppf (id,ty) ->
               Format.fprintf ppf "%s:%a" (iname id) M.Type.pp ty)) env;
        *)
        match t.typ.desc with
        | TyLambda (ty1, ty2) -> begin
            match fvars with
            | [] ->
                let env' = [p.desc, p.typ] in
                let o, _env' = compile env' body in
                [_LAMBDA (ty1, ty2, o)], (dummy "lambda" t.typ, t.typ) :: env
            | _ ->
                (* https://tezos.gitlab.io/michelson-reference/#instr-APPLY

                   Values that are not both pushable and storable
                   (values of type operation, contract _ and big_map _ _)
                   cannot be captured by APPLY (cannot appear in ty1).
                *)
                List.iter
                  (fun id ->
                    if is_pushable id.IML.typ && is_storable id.typ then ()
                    else if id.loc <> Location.none then
                      Error.raisef
                        Freevar
                        ~loc:id.loc
                        "@[This variable of non pushable or non storable type \
                         is captured in the following closure as `%a`:@]@ \
                         @[%a@]"
                        Ident.pp
                        id.desc
                        IML.pp
                        t
                    else
                      Error.raisef
                        Freevar
                        ~loc:id.loc
                        "@[Variable `%a` of non pushable or non storable type \
                         is captured in the following closure:@]@ @[%a@]"
                        Ident.pp
                        id.desc
                        IML.pp
                        t)
                  fvars ;
                (* fvars: x1:ty1 :: x2:ty2 :: .. :: xn:tyn

                   inside lambda:  p :: x1:ty1 :: x2:ty2 :: .. :: xn:tyn

                   lambda's parameter:  tyn * (ty(n-1) * ( .. (ty1 * p.typ) .. ))
                *)
                let lambda, _env' =
                  let env' =
                    (p.desc, p.typ)
                    :: List.map (fun {IML.desc; typ} -> desc, typ) fvars
                  in
                  let ity =
                    (* (tyn * (ty(n-1) * .. * (ty1 * p.typ) .. )) *)
                    List.fold_left
                      (fun st {IML.typ} ->
                        if is_pushable typ && is_storable typ then
                          tyPair (None, typ, None, st)
                        else assert false (* must be rejected earlier *))
                      p.typ
                      fvars
                  in

                  (*
                         -   (xn, (xn-1, .., (x1, p1) ..))  :: 0

                         UNPAIR  xn :: (xn-1, .., (x1, p1) ..))  :: 0
                         SWAP    (xn-1, .., (x1, p1) ..)) :: xn :: 0
                         UNPAIR  xn-1 :: (.., (x1, p1) ..)) :: xn :: 0
                         SWAP    (.., (x1, p1) ..)) :: xn-1 :: xn :: 0
                         ...
                                 (x1, p1) :: x2 :: ... :: xn :: 0
                         UNPAIR + SWAP

                         ..  p1 :: x1 :: .. :: xn :: 0
                      *)
                  let extractor =
                    List.rev_map
                      (fun {IML.desc = v} ->
                        _BLOCK [_UNPAIR; _SWAP] |// "fvar " ^ iname v)
                      fvars
                  in
                  let body, env' = compile env' body in
                  _LAMBDA (ity, ty2, extractor @ body), env'
                in
                let partial_apply =
                  (* Apply fvars from xn to x1 *)
                  List.fold_left
                    (fun st {IML.desc = x} ->
                      let o, _ =
                        var
                          ~loc
                          ((Ident.dummy "lambda", tyUnit (* for lambda *))
                          :: env)
                          x
                      in
                      (_BLOCK (o @ [_APPLY]) |// "partial app " ^ iname x) :: st)
                    []
                    fvars
                in
                lambda :: partial_apply, (dummy "lambda" t.typ, t.typ) :: env
          end
        | _ -> assert false)
    | App (t, []) -> compile env t
    | App (f, ts) ->
        let ofun, env = compile env f in
        let id1 = takeid env in
        let os, _typ, env, rev_ids =
          List.fold_left
            (fun (ofun, ftyp, env, rev_ids) arg ->
              let restyp =
                match ftyp.M.Type.desc with
                | TyLambda (_, ty) -> ty
                | _ -> assert false
              in
              let oarg, env = compile env arg in
              let idarg = takeid env in
              ( ofun @ oarg @ [_EXEC],
                restyp,
                (dummy "app_res" restyp, restyp) :: List.drop 2 env,
                idarg :: rev_ids ))
            (ofun, f.typ, env, [])
            ts
        in
        let id' = takeid env in
        ( [
            _BLOCK os
            |// Printf.sprintf
                  "let %s = %s %s"
                  (iname id')
                  (iname id1)
                  (String.concat " " (List.rev_map iname rev_ids));
          ],
          env )
    | Contract_create (s, sloc, e1, e2, e3) ->
        let m =
          match s with
          | Module path ->
              begin match List.assoc_opt path !compiled_modules with
              | None -> assert false (* XXX fancy error *)
              | Some m -> m
              end
          | Tz_file path ->
              begin match Michelson.Module.parse_file ~check_indentation:true path with
              | Error es ->
                  Error.raisef
                    Contract
                    ~loc:sloc
                    "Michelson parse error:@ %a"
                    Tezos_error_monad.Error_monad.pp_print_trace
                    es
              | Ok m -> m
              end
          | Tz_code s ->
              begin match Michelson.Module.parse_string ~check_indentation:true s with
              | Error es ->
                  Error.raisef
                    Contract
                    ~loc:sloc
                    "Michelson parse error:@ %a"
                    Tezos_error_monad.Error_monad.pp_print_trace
                    es
              | Ok m -> m
              end
        in
        let es, env = args env [e1; e2; e3] in
        let ids = List.map fst & List.take 3 env in
        let id' = dummy "create_contract" t.typ in
        es
        @ [
            _BLOCK [_CREATE_CONTRACT m; _PAIR []]
            |// Printf.sprintf
                  "let %s = Contract.create .. %s"
                  (iname id')
                  (String.concat " " & List.map iname ids);
          ],
        (id', t.typ) :: List.drop 3 env

and args env ts =
  List.fold_right
    (fun t (os, env) ->
      let os', env = compile env t in
      os @ os', env)
    ts
    ([], env)

let structure t =
  let t = Constantize.f ~do_big_map:Config.allow_big_map ~compile:None t in
  match t.IML.desc with
  | IML.Fun (pv, t) ->
      let os, env = compile [pv.desc, pv.typ] t in
      let id' = match env with [(id, _)] -> id | _ -> assert false in
      (_BLOCK []
      |// Printf.sprintf "let %s = (input, storage)" (iname pv.desc))
      :: os
      @ [_BLOCK [] |// Printf.sprintf "return %s" (iname id')]
      |> clean_failwith |> fst
  | _ ->
      Format.eprintf "Unsupported %a@." IML.pp t ;
      assert false
          end
