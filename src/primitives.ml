(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* We can omit the types here, since they are coded in SCaml.ml *)
(* XXX We should consider to merge SCaml.ml and primitives.ml into one *)

module M = Michelson
open M.Opcode
open M.Type
open SCaml_tools

let simple ~loc:(_ : Location.t) (os : Michelson.Opcode.t list)
    (_ty : Michelson.Type.t) =
  os

let protocol pred f ~loc ty =
  let proto = Conf.get_protocol () in
  if pred proto then f ~loc ty
  else
    Error.raisef
      Primitive
      ~loc
      "This primitive is not supported in the current protocol version %s"
      (Protocol.to_string proto)

let since v = protocol (fun x -> x >= v)

let since_008 = since (8, 0)
let since_012 = since (12, 0)

let rec args ty = function
  | 0 -> []
  | n -> (
      match ty.desc with
      | TyLambda (ty1, ty2) -> ty1 :: args ty2 (n - 1)
      | _ -> assert false)

let comparison ~loc os ty =
  match args ty 2 with
  | [ty1; _ty2] ->
      (* ty1 == ty2 *)
      if not & M.Type.is_comparable (Conf.get_protocol ()) ty1 then
        Error.raisef
          Primitive
          ~loc
          "Comparison operator takes a non comparable type %a"
          M.Type.pp
          ty1 ;
      os
  | _ -> assert false

let op desc = {Michelson.Opcode.desc; comments = []}

let primitives =
  [
    (* name   , (pure,  arity, code) *)

    (* tuple *)
    "fst", (true, 1, simple [op CAR]);
    "snd", (true, 1, simple [op CDR]);
    (* comparison *)
    "compare", (true, 2, comparison [op COMPARE]);
    "(=)", (true, 2, comparison [op COMPARE; op EQ]);
    "(<>)", (true, 2, comparison [op COMPARE; op NEQ]);
    "(<)", (true, 2, comparison [op COMPARE; op LT]);
    "(>)", (true, 2, comparison [op COMPARE; op GT]);
    "(<=)", (true, 2, comparison [op COMPARE; op LE]);
    "(>=)", (true, 2, comparison [op COMPARE; op GE]);
    (* arith *)
    "(+)", (true, 2, simple [op ADD]);
    "(+^)", (true, 2, simple [op ADD]);
    "(+$)", (false, 2, simple [op ADD]);
    "(-)", (true, 2, simple [op SUB]);
    "(-^)", (true, 2, simple [op SUB]);
    "(-$)", (false, 2,
             let proto = Conf.get_protocol () in
             (* SUB is deprecated since 012_Itaca *)
             if proto >= (12, 0) then
               simple [op SUB_MUTEZ; op & IF_NONE ([op & PUSH (tyInt, Int (Z.of_int (-1))); op FAILWITH], [])]
             else
               simple [op SUB]);
    "(-$?)", (false, 2,
              since_012 @@ simple [op SUB_MUTEZ]);
    "( * )", (true, 2, simple [op MUL]);
    "( *^ )", (true, 2, simple [op MUL]);
    "( *$ )", (false, 2, simple [op MUL]);
    "(lsl)", (true, 2, simple [op LSL]);
    "(lsr)", (true, 2, simple [op LSR]);
    "(~-)", (true, 1, simple [op NEG]);
    "(~-^)", (true, 1, simple [op NEG]);
    "abs", (true, 1, simple [op ABS]);
    "isnat", (true, 1, simple [op ISNAT]);
    "int_of_nat", (true, 1, simple [op INT]);
    ( "nat_of_int",
      ( false,
        1,
        simple
          [
            op ISNAT;
            op
            & IF_NONE ([op & PUSH (tyInt, Int (Z.of_int (-1))); op FAILWITH], []);
          ] ) );
    "mutez_of_nat", (false, 1, simple [op & PUSH (tyMutez, Int Z.one); op MUL]);
    ( "mutez_of_int",
      ( false,
        1,
        simple
          [
            op ISNAT;
            op
            & IF_NONE ([op & PUSH (tyInt, Int (Z.of_int (-1))); op FAILWITH], []);
            op & PUSH (tyMutez, Int Z.one);
            op MUL;
          ] ) );
    ( "nat_of_mutez",
      ( false,
        1,
        simple
          [
            op & PUSH (tyMutez, Int Z.one);
            op SWAP;
            op EDIV;
            op & IF_NONE ([op & UNIT; op FAILWITH], []);
            op CAR;
          ] ) );
    ( "int_of_mutez",
      ( false,
        1,
        simple
          [
            op & PUSH (tyMutez, Int Z.one);
            op SWAP;
            op EDIV;
            op & IF_NONE ([op & UNIT; op FAILWITH], []);
            op CAR;
            op INT;
          ] ) );
    "ediv_int_int", (true, 2, simple [op EDIV]);
    "ediv_int_nat", (true, 2, simple [op EDIV]);
    "ediv_nat_int", (true, 2, simple [op EDIV]);
    "ediv_nat_nat", (true, 2, simple [op EDIV]);
    "ediv_tz_tz", (true, 2, simple [op EDIV]);
    "ediv_tz_nat", (true, 2, simple [op EDIV]);

    "ediv_rem_int", (true, 2, simple [op EDIV]);
    "ediv_rem_nat", (true, 2, simple [op EDIV]);
    "ediv_rem_tz", (true, 2, simple [op EDIV]);
    "ediv_rem_tz_nat", (true, 2, simple [op EDIV]);

    ( "(/)", (* XXX deprecated *)
      ( false,
        2,
        simple
          [
            op EDIV;
            op & IF_NONE ([op & PUSH (tyInt, Int Z.zero); op FAILWITH], [op CAR]);
          ] ) );
    ( "(//)",
      ( false,
        2,
        simple
          [
            op EDIV;
            op & IF_NONE ([op & PUSH (tyInt, Int Z.zero); op FAILWITH], [op CAR]);
          ] ) );
    ( "(/^)",
      ( false,
        2,
        simple
          [
            op EDIV;
            op & IF_NONE ([op & PUSH (tyNat, Int Z.zero); op FAILWITH], [op CAR]);
          ] ) );
    ( "(/$)",
      ( false,
        2,
        simple
          [
            op EDIV;
            op
            & IF_NONE ([op & PUSH (tyMutez, Int Z.zero); op FAILWITH], [op CAR]);
          ] ) );
    ( "(/$^)",
      ( false,
        2,
        simple
          [
            op EDIV;
            op & IF_NONE ([op & PUSH (tyNat, Int Z.zero); op FAILWITH], [op CAR]);
          ] ) );

    ( "(%%)",
      ( false,
        2,
        simple
          [
            op EDIV;
            op & IF_NONE ([op & PUSH (tyInt, Int Z.zero); op FAILWITH], [op CDR]);
          ] ) );
    ( "(%^)",
      ( false,
        2,
        simple
          [
            op EDIV;
            op & IF_NONE ([op & PUSH (tyNat, Int Z.zero); op FAILWITH], [op CDR]);
          ] ) );
    ( "(%$)",
      ( false,
        2,
        simple
          [
            op EDIV;
            op
            & IF_NONE ([op & PUSH (tyMutez, Int Z.zero); op FAILWITH], [op CDR]);
          ] ) );
    ( "(%$^)",
      ( false,
        2,
        simple
          [
            op EDIV;
            op & IF_NONE ([op & PUSH (tyNat, Int Z.zero); op FAILWITH], [op CDR]);
          ] ) );

    (* bool *)
    "band", (true, 2, simple [op AND]);
    "bor", (true, 2, simple [op OR]);
    "xor", (true, 2, simple [op XOR]);
    "not", (true, 1, simple [op NOT]);
    "(lor)", (true, 2, simple [op OR]);
    "(land)", (true, 2, simple [op AND]);
    "land_int_nat", (true, 2, simple [op AND]);
    "(lxor)", (true, 2, simple [op XOR]);
    "lnot_nat", (true, 1, simple [op NOT]);
    "lnot", (true, 1, simple [op NOT]);
    (* list *)
    "List.length", (true, 1, simple [op SIZE]);
    ( "List.map",
      ( false,
        2,
        (* lambda : map : S              SWAP ;
           { hd; <tl> } : lambda : S     MAP {
             hd : lambda : S              DIP DUP
             hd : lambda : lambda : S     EXECx
             hd' : lambda : S

           {} : lambda : S               MAP {..}

           list' : lambda : S             DIP DROP
           list' : S
        *)
        simple
          [
            op SWAP;
            op & MAP ([op & DIP [op DUP]] @ [op EXEC]);
            op & DIP [op & DROP];
          ] ) );
    ( "List.fold_left",
      ( false,
        3,
        (*
                             lam : acc : list : s                  SWAP; DIP { SWAP } SWAP
                             list : acc : lam : s                  ITER {
                             hd : acc : lam : s                       DIP 2 { DUP } ; DUG 2
                             acc : lam : hd : lam : s                 EXEC ;
                             lam' : hd : lam : s                      SWAP ; EXEC ;
                             acc' : lam : s                        }

                             [] : acc : lam : s                    ITER {..}
                             acc : lam : s                         DIP { DROP }
                             acc : s
                           *)
        simple
          [
            op SWAP;
            op & DIP [op SWAP];
            op SWAP;
            op
            & ITER
                [op & DIPn (2, [op DUP]); op & DUG 2; op EXEC; op SWAP; op EXEC];
            op & DIP [op & DROP];
          ] ) );
    ( "List.fold_left'",
      ( false,
        3,
        (*
                              lam : acc : list : s                  SWAP; DIP { SWAP } SWAP
                              list : acc : lam : s                  ITER {
                              hd : acc : lam : s                       SWAP PAIR
                              (acc, hd) : lam : s                      DIP { DUP }
                              (acc, hd) : lam : lam : s                EXEC
                              acc : lam : s                         }

                              [] : acc : lam : s                    ITER {..}
                              acc : lam : s                         DIP { DROP }
                              acc : s
                            *)
        simple
          [
            op SWAP;
            op & DIP [op SWAP];
            op SWAP;
            op & ITER [op SWAP; op & PAIR []; op & DIP [op DUP]; op EXEC];
            op & DIP [op & DROP];
          ] ) );
    ( "List.rev",
      ( true,
        1,
        fun ~loc:_ ty ->
          match ty.desc with
          | TyLambda ({desc = TyList ty}, {desc = TyList _ty'}) ->
              (* ty = _ty' *)
              [op & DIP [op & NIL ty]; op & ITER [op CONS]]
          | _ -> assert false ) );
    "List.rev_append", (true, 2, simple [op & ITER [op CONS]]);
    (* set *)
    ( "Set.empty",
      ( true,
        0,
        fun ~loc:_ typ ->
          match typ.desc with
          | TySet ty -> [op & EMPTY_SET ty]
          | _ -> assert false ) );
    ( "Set.is_empty",
      ( true,
        1,
        simple [op SIZE; op & PUSH (tyNat, Int Z.zero); op COMPARE; op EQ] ) );
    "Set.length", (true, 1, simple [op SIZE]);
    "Set.mem", (true, 2, simple [op MEM]);
    "Set.update", (true, 3, simple [op UPDATE]);
    ( "Set.add",
      (true, 2, simple [op & PUSH (tyBool, Bool true); op SWAP; op UPDATE]) );
    ( "Set.remove",
      (true, 2, simple [op & PUSH (tyBool, Bool false); op SWAP; op UPDATE]) );
    ( "Set.fold",
      ( false,
        3,
        (*
                       lam : set : acc : s                   SWAP DIP { SWAP }
                       set : acc : lam : s                   ITER {
                       elt : acc : lam : s                   DIP { DIP { DUP } SWAP }
                       elt : lam : acc : lam : s             EXECx
                       lam2 : acc : lam : s                  SWAP
                       acc : lam2 : lam : s                  EXECx
                       acc : lam : s                         }

                       set : acc : lam : s                   ITER {

                       empty : acc : lam : s                 ITER {
                       acc : lam : s                         DIP { DROP }
                       acc : s
                     *)
        simple
          [
            op SWAP;
            op & DIP [op SWAP];
            op
            & ITER
                ([op & DIP [op & DIP [op DUP]; op SWAP]]
                @ [op EXEC] @ [op SWAP] @ [op EXEC]);
            op & DIP [op & DROP];
          ] ) );
    ( "Set.fold'",
      ( false,
        3,
        (*
                        lam : set : acc : s                   SWAP DIP { SWAP }
                        set : acc : lam : s                   ITER {
                        elt : acc : lam : s                     PAIR
                        (elt, acc) : lam : s                    DIP DUP
                        (elt, acc) : lam : lam : s              EXEC
                        acc : lam : s                         }

                        empty : acc : lam : s                 ITER {..}
                        acc : lam : s                         DIP { DROP }
                        acc : s
                      *)
        simple
          [
            op SWAP;
            op & DIP [op SWAP];
            op & ITER [op & PAIR []; op & DIP [op & DUP]; op & EXEC];
            op & DIP [op & DROP];
          ] ) );
    ( "Set.map",
      ( false,
        2,
        (* lambda : map : S              SWAP ;
           { hd; <tl> } : lambda : S     MAP {
             hd : lambda : S              DIP DUP
             hd : lambda : lambda : S     EXECx
             hd' : lambda : S

           {} : lambda : S               MAP {..}

           list' : lambda : S             DIP DROP
           list' : S
        *)
        simple
          [
            op & SWAP;
            op & MAP ([op & DIP [op & DUP]] @ [op & EXEC]);
            op & DIP [op & DROP];
          ] ) );
    (* map *)
    ( "Map.empty",
      ( true,
        0,
        fun ~loc:_ typ ->
          match typ.desc with
          | TyMap (ty1, ty2) -> [op & EMPTY_MAP (ty1, ty2)]
          | _ -> assert false ) );
    ( "Map.is_empty",
      ( true,
        1,
        simple [op & SIZE; op & PUSH (tyNat, Int Z.zero); op & COMPARE; op & EQ]
      ) );
    "Map.length", (true, 1, simple [op & SIZE]);
    "Map.get", (true, 2, simple [op & GET]);
    "Map.find", (true, 2, simple [op & GET]);
    "Map.mem", (true, 2, simple [op & MEM]);
    "Map.update", (true, 3, simple [op & UPDATE]);
    "Map.add", (true, 3, simple [op & DIP [op & SOME]; op & UPDATE]);
    ( "Map.remove",
      ( true,
        2,
        fun ~loc:_ typ ->
          let ty =
            match typ.desc with
            | TyLambda (_, {desc = TyLambda (_, {desc = TyMap (_, ty)})}) -> ty
            | _ -> assert false
          in
          [op & NONE ty; op & SWAP; op & UPDATE] ) );
    ( "Map.get_and_update",
      (true, 3, since_008 @@ simple [op & GET_AND_UPDATE; op & PAIR []]) );
    ( "Map.map",
      ( false,
        2,
        (* lambda : map : S                 SWAP ;
           { (k,v); <tl> } : lambda : S     MAP {
             (k, v) : lambda : S              DIP DUP
             (k, v) : lambda : lambda : S     DUP CAR DIP { CDR ; SWAP }
             k : lambda : v : lambda : S      EXECx
             lambda' : v : lambda : S         SWAP EXECx
             w : : lambda : S

           { <tl> } : lambda : S            MAP { ..

           {} : lambda : S                  Map {

           map' : lambda : S                  DIP DROP
           map' : S
        *)
        simple
          [
            op & SWAP;
            op
            & MAP
                ([
                   op & DIP [op & DUP];
                   op & DUP;
                   op & CAR;
                   op & DIP [op & CDR; op & SWAP];
                 ]
                @ [op & EXEC] @ [op & SWAP] @ [op & EXEC]);
            op & DIP [op & DROP];
          ] ) );
    ( "Map.map'",
      ( false,
        2,
        (* lambda : map : S                 SWAP ;
           { (k,v); <tl> } : lambda : S     MAP {
             (k, v) : lambda : S              DIP DUP
             (k, v) : lambda : lambda : S     EXEC
             w : : lambda : S

           {} : lambda : S                  Map {..}

           map' : lambda : S                  DIP DROP
           map' : S
        *)
        simple
          [
            op & SWAP;
            op & MAP [op & DIP [op & DUP]; op & EXEC];
            op & DIP [op & DROP];
          ] ) );
    ( "Map.fold",
      ( false,
        3,
        (*
                       lam : map : acc : s                 SWAP DIP { SWAP }
                       set : acc : lam : s                 ITER {
                       (k,v) : acc : lam : s               DUP CAR DIP { CDR }
                       k : v : acc : lam : s               DIP { DIP { DIP { DUP } SWAP } SWAP
                       k : lam : v : acc : lam : s         EXECx
                       lam2 : v : acc : lam : s            SWAP
                       v : lam2 : acc : lam : s            EXECx
                       lam3 : acc : lam : s                SWAP
                       acc : lam3 : lam : s                EXECx
                       acc' : lam : s

                       empty : acc : lam : s               ITER {..}
                       acc : lam : s                       DIP { DROP }
                       acc : s
                     *)
        simple
          [
            op & SWAP;
            op & DIP [op & SWAP];
            op
            & ITER
                ([
                   op & DUP;
                   op & CAR;
                   op & DIP [op & CDR];
                   op
                   & DIP [
                           op & DIP [op & DIP [op & DUP]; op & SWAP];
                           op & SWAP;
                         ];
                 ]
                @ [op & EXEC] @ [op & SWAP] @ [op & EXEC] @ [op & SWAP]
                @ [op & EXEC]);
            op & DIP [op & DROP];
          ] ) );
    ( "Map.fold'",
      ( false,
        3,
        (*
                       lam : map : acc : s                   SWAP DIP { SWAP }
                       set : acc : lam : s                   ITER {
                       (k,v) : acc : lam : s                   DUP CAR DIP { CDR }
                       k : v : acc : lam : s                   DIP { PAIR } PAIR
                       (k, (v, acc)) : lam : s                 DIP DUP
                       (k, (v, acc)) : lam : lam : s           EXEC
                       acc : lam : s                         }

                       empty : acc : lam : s                 ITER {..}
                       acc : lam : s                         DIP { DROP }
                       acc : s
                     *)
        simple
          [
            op & SWAP;
            op & DIP [op & SWAP];
            op
            & ITER
                [
                  op & DUP;
                  op & CAR;
                  op & DIP [op & CDR];
                  op & DIP [op & PAIR []];
                  op & PAIR [];
                  op & DIP [op & DUP];
                  op & EXEC;
                ];
            op & DIP [op & DROP];
          ] ) );
    ( "Map.get_with_default",
      ( true,
        3,
        simple [op & GET; op & IF_NONE ([], [op & DIP [op & DROP]])] ) );
    ( "Map.find_with_default",
      ( true,
        3,
        simple [op & GET; op & IF_NONE ([], [op & DIP [op & DROP]])] ) );
    (* big map *)
    ( "BigMap.empty",
      ( true,
        0,
        fun ~loc:_ typ ->
          match typ.desc with
          | TyBigMap (ty1, ty2) -> [op & EMPTY_BIG_MAP (ty1, ty2)]
          | _ -> assert false ) );
    "BigMap.get", (true, 2, simple [op & GET]);
    "BigMap.find", (true, 2, simple [op & GET]);
    "BigMap.mem", (true, 2, simple [op & MEM]);
    "BigMap.update", (true, 3, simple [op & UPDATE]);
    "BigMap.add", (true, 3, simple [op & DIP [op & SOME]; op & UPDATE]);
    ( "BigMap.remove",
      ( true,
        2,
        fun ~loc:_ typ ->
          let ty =
            match typ.desc with
            | TyLambda (_, {desc = TyLambda (_, {desc = TyBigMap (_, ty)})}) ->
                ty
            | _ -> assert false
          in
          [op & NONE ty; op & SWAP; op & UPDATE] ) );
    ( "BigMap.get_and_update",
      (true, 3, since_008 @@ simple [op & GET_AND_UPDATE; op & PAIR []]) );
    ( "BigMap.get_with_default",
      ( true,
        3,
        simple [op & GET; op & IF_NONE ([], [op & DIP [op & DROP]])] ) );
    ( "BigMap.find_with_default",
      ( true,
        3,
        simple [op & GET; op & IF_NONE ([], [op & DIP [op & DROP]])] ) );
    (* control *)
    ( "Loop.left",
      ( false,
        2,
        fun ~loc:_ typ ->
          let rty =
            match typ.desc with
            | TyLambda (_, {desc = TyLambda (_, rty)}) -> rty
            | _ ->
                Format.eprintf "Loop.left %a@." M.Type.pp typ ;
                assert false
          in
          (* lambda : acc : S                 SWAP ; LEFT ;
             Left acc : lambda : S            LOOP_LEFT {
             acc : lambda : S                   DUP DIP
             acc : lambda : lambda : S          EXECx
             LorR : lambda : S

             Left acc : lambda : S            LOOP_LEFT { ..

             Right res : lambda : S           LOOP_LEFT { .. }
             res : lambda : S                 DIP DROP
          *)
          [
            op & SWAP;
            op & LEFT rty;
            op & LOOP_LEFT [op (DIP [op & DUP]); op & EXEC];
            op & DIP [op & DROP];
          ] ) );
    (* string and bytes *)
    "String.concat", (true, 2, simple [op & CONCAT]);
    "String.cat", (true, 2, simple [op & CONCAT]);
    "(^)", (true, 2, simple [op & CONCAT]);
    "String.length", (true, 1, simple [op & SIZE]);
    "Bytes.concat", (true, 2, simple [op & CONCAT]);
    "Bytes.cat", (true, 2, simple [op & CONCAT]);
    "Bytes.length", (true, 1, simple [op & SIZE]);
    "String.slice", (true, 3, simple [op & SLICE]);
    "Bytes.slice", (true, 3, simple [op & SLICE]);
    (* XXX not tested *)
    (* obj *)
    ( "Obj.pack",
      ( true,
        1,
        fun ~loc ty ->
          match args ty 1 with
          | [aty] ->
              if not & M.Type.is_packable aty then
                Error.raisef
                  Primitive
                  ~loc
                  "Obj.pack cannot take a non packable type %a"
                  M.Type.pp
                  aty ;
              [op & PACK]
          | _ -> assert false ) );
    ( "Obj.unpack",
      ( true,
        1,
        fun ~loc ty ->
          match ty.desc with
          | TyLambda (_, {desc = TyOption (None, ty)}) ->
              if not & M.Type.is_packable ty then
                Error.raisef
                  Primitive
                  ~loc
                  "Obj.unpack cannot unpack to a non packable type %a"
                  M.Type.pp
                  ty ;
              [op & UNPACK ty]
          | _ -> assert false ) );
    (* contracts *)
    ( "Contract.contract",
      ( false,
        1,
        fun ~loc:_ ty ->
          match ty.desc with
          | TyLambda (_, {desc = TyOption (None, {desc = TyContract ty})}) ->
              [op & CONTRACT ty]
          | _ -> assert false ) );
    ( "Contract.contract'",
      ( false,
        2,
        (* compilation is given by [Primitives.contract'] *)
        fun ~loc:_ _ty -> assert false ) );
    ( "view",
      ( false,
        3,
        (* compilation is given by compile.ml *)
        fun ~loc:_ _ty -> assert false ) );
    "Contract.implicit_account", (false, 1, simple [op & IMPLICIT_ACCOUNT]);
    "Contract.address", (true, 1, simple [op & ADDRESS]);
    "Contract.self", (true, 0, simple [op & SELF None]);
    "Contract.self_address", (true, 0, simple [op & SELF_ADDRESS]);
    (* operation *)
    "Operation.transfer_tokens", (true, 3, simple [op & TRANSFER_TOKENS]);
    "Operation.set_delegate", (true, 1, simple [op & SET_DELEGATE]) (* global *);
    "Global.get_now", (true, 1, simple [op & DROP; op & NOW]);
    "Global.get_amount", (true, 1, simple [op & DROP; op & AMOUNT]);
    "Global.get_balance", (true, 1, simple [op & DROP; op & BALANCE]);
    "Global.get_source", (true, 1, simple [op & DROP; op & SOURCE]);
    "Global.get_sender", (true, 1, simple [op & DROP; op & SENDER]);
    ( "Global.get_steps_to_quota",
      (true, 1, simple [op & DROP; op & STEPS_TO_QUOTA]) );
    "Global.get_chain_id", (true, 1, simple [op & DROP; op & CHAIN_ID]);
    "Global.get_level", (true, 1, since_008 @@ simple [op & DROP; op & LEVEL]);
    ( "Global.get_voting_power",
      (true, 1, since_008 @@ simple [op & DROP; op & VOTING_POWER]) );
    ( "Global.get_total_voting_power",
      (true, 1, since_008 @@ simple [op & DROP; op & TOTAL_VOTING_POWER]) );
    (* crypto *)
    "Crypto.check_signature", (true, 3, simple [op & CHECK_SIGNATURE]);
    "Crypto.blake2b", (true, 1, simple [op & BLAKE2B]);
    "Crypto.sha256", (true, 1, simple [op & SHA256]);
    "Crypto.sha512", (true, 1, simple [op & SHA512]);
    "Crypto.sha3", (true, 1, simple [op & SHA3]);
    "Crypto.keccak", (true, 1, simple [op & KECCAK]);
    "Crypto.hash_key", (true, 1, simple [op & HASH_KEY]) (* errors *);
    "Error.failwith", (false, 1, simple [op & FAILWITH]) (* deprecated *);
    "failwith", (false, 1, simple [op & FAILWITH]);
    "raise", (false, 1, simple [op & FAILWITH]);
    "never", (false, 1, simple [op NEVER]) (* timestamp *);
    "Timestamp.add", (true, 2, simple [op & ADD]);
    "Timestamp.sub", (true, 2, simple [op & SUB]);
    "Timestamp.diff", (true, 2, simple [op & SUB]) (* option *);
    ( "Option.value",
      (true, 2, simple [op & IF_NONE ([], [op & DIP [op & DROP]])]) );
    ( "Option.get",
      ( false,
        1,
        simple
          [
            op
            & IF_NONE
                ([op & PUSH (tyString, String "Option.get"); op & FAILWITH], []);
          ] ) );
    ( "Option.map",
      ( false,
        2,
        (* lambda : opta : S                SWAP ;
           opta : lambda : S                MAP {
             a : lambda : S                  DIP { DUP }
             a : lambda : lambda : S         EXEC
             b : lambda : S                 }
           optb : lambda : S                DIP { DROP }
           optb : S
        *)
        simple
          [
            op SWAP;
            op & MAP [op & DIP [op & DUP]; op EXEC];
            op & DIP [op DROP]
          ] ) );

    (* sum *)
    ( "Sum.get_left",
      ( false,
        1,
        simple
          [
            op
            & IF_LEFT
                ( [],
                  [op & PUSH (tyString, String "Sum.get-left"); op & FAILWITH]
                );
          ] ) );
    ( "Sum.get_right",
      ( false,
        1,
        simple
          [
            op
            & IF_LEFT
                ( [op & PUSH (tyString, String "Sum.get-left"); op & FAILWITH],
                  [] );
          ] ) );
    (* bls12_381 *)
    "BLS12_381.G1.(+)", (true, 2, since_008 @@ simple [op & ADD]);
    "BLS12_381.G2.(+)", (true, 2, since_008 @@ simple [op & ADD]);
    "BLS12_381.Fr.(+)", (true, 2, since_008 @@ simple [op & ADD]);
    "BLS12_381.Fr.to_int", (true, 1, since_008 @@ simple [op & INT]);
    "BLS12_381.G1.( * )", (true, 2, since_008 @@ simple [op & MUL]);
    "BLS12_381.G2.( * )", (true, 2, since_008 @@ simple [op & MUL]);
    "BLS12_381.Fr.( * )", (true, 2, since_008 @@ simple [op & MUL]);
    "BLS12_381.mul_nat", (true, 2, since_008 @@ simple [op & MUL]);
    "BLS12_381.mul_int", (true, 2, since_008 @@ simple [op & MUL]);
    "BLS12_381.G1.(~-)", (true, 1, since_008 @@ simple [op & NEG]);
    "BLS12_381.G2.(~-)", (true, 1, since_008 @@ simple [op & NEG]);
    "BLS12_381.Fr.(~-)", (true, 1, since_008 @@ simple [op & NEG]);
    ( "BLS12_381.pairing_check",
      (true, 1, since_008 @@ simple [op & PAIRING_CHECK]) );
    (* tickets *)
    "Ticket.create", (true, 2, since_008 @@ simple [op & TICKET]);
    ( "Ticket.read",
      ( true,
        1,
        (* it returns 2 stack values.
           XXX We need PAIR UNPAIR elimination.
        *)
        since_008 @@ simple [op & READ_TICKET; op & PAIR []] ) );
    "Ticket.split", (true, 2, since_008 @@ simple [op & SPLIT_TICKET]);
    "Ticket.join", (true, 2, since_008 @@ simple [op & PAIR []; op & JOIN_TICKETS]);
    (* sapling *)
    ( "Sapling.empty_state",
      ( true,
        0,
        since_008 @@ fun ~loc:_ ty ->
        match ty.desc with
        | TySapling_state n -> [op & SAPLING_EMPTY_STATE n]
        | _ -> assert false ) );
    ( "Sapling.verify_update",
      (true, 2, since_008 @@ simple [op & SAPLING_VERIFY_UPDATE]) );
  ]

let contract' entry ~loc:_ ty =
  match ty.desc with
  | TyLambda
      ( _,
        {desc = TyLambda (_, {desc = TyOption (None, {desc = TyContract ty})})}
      ) ->
      op & CONTRACT' (ty, entry)
  | _ -> assert false
