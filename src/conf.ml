(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open SCaml_tools
open Ocaml_conv.Default
open Untyped

type mode =
  | Compile
  | ConvertAll
  | ConvertSingleValue of string
  | ConvertSingleType of string
  | Revert of string

and t = {
  iml_optimization : bool;
  michelson_optimization : bool;
  michelson_optimization_do_dijkstra : bool;
  michelson_optimization_search_space_multiplier : float;
  mode : mode;
  noscamlib : bool;
  dump_iml : bool;
  protocol : Protocol.t;
  (* Support protocol specific features *)
  contract : string option;
}
[@@deriving conv {ocaml}]

and opt = {
  op_iml_optimization : bool option;
  op_michelson_optimization : bool option;
  op_michelson_optimization_do_dijkstra : bool option;
  op_michelson_optimization_search_space_multiplier : float option;
  op_mode : mode option;
  op_noscamlib : bool option;
  op_dump_iml : bool option;
  op_protocol : Protocol.t option;
  op_contract : string option;
}
[@@deriving conv {ocaml}]

let pp = Camlon.Ocaml.format_with ocaml_of_t
let pp_opt = Camlon.Ocaml.format_with ocaml_of_opt

let default =
  {
    iml_optimization = true;
    michelson_optimization = true;
    michelson_optimization_do_dijkstra = false;
    michelson_optimization_search_space_multiplier = 1.0;
    mode = Compile;
    noscamlib = false;
    dump_iml = false;
    protocol = Protocol.default;
    contract = None;
  }

let conf = ref default
let get_conf () = !conf

let get_protocol () = !conf.protocol

let default_dump_iml =
  try
    ignore (Sys.getenv "SCAML_DUMP_IML") ;
    true
  with _ -> false

let unopt o =
  let f def = function None -> def | Some x -> x in
  {
    iml_optimization = f true o.op_iml_optimization;
    michelson_optimization = f true o.op_michelson_optimization;
    michelson_optimization_do_dijkstra = f false o.op_michelson_optimization_do_dijkstra;
    michelson_optimization_search_space_multiplier = f 1.0 o.op_michelson_optimization_search_space_multiplier;
    mode = f Compile o.op_mode;
    noscamlib = f false o.op_noscamlib;
    dump_iml = f default_dump_iml o.op_dump_iml;
    protocol = f Protocol.default o.op_protocol;
    contract = o.op_contract;
  }

let none =
  {
    op_iml_optimization = None;
    op_michelson_optimization = None;
    op_michelson_optimization_do_dijkstra = None;
    op_michelson_optimization_search_space_multiplier = None;
    op_mode = None;
    op_noscamlib = None;
    op_dump_iml = None;
    op_protocol = None;
    op_contract = None;
  }

let merge o1 o2 =
  let f n opt1 opt2 =
    match opt1, opt2 with
    | Some v1, None -> Some v1
    | None, Some v2 -> Some v2
    | None, None -> None
    | Some v1, Some v2 when v1 = v2 -> Some v1
    | Some _, Some _ -> failwith n
  in
  try
    {
      op_iml_optimization =
        f "iml_optimization" o1.op_iml_optimization o2.op_iml_optimization;
      op_michelson_optimization =
        f
          "michelson_optimization"
          o1.op_michelson_optimization
          o2.op_michelson_optimization;
      op_michelson_optimization_do_dijkstra =
        f
          "michelson_optimization_do_dijkstra"
          o1.op_michelson_optimization_do_dijkstra
          o2.op_michelson_optimization_do_dijkstra;
      op_michelson_optimization_search_space_multiplier =
        f
          "michelson_optimization_search_space_multiplier"
          o1.op_michelson_optimization_search_space_multiplier
          o2.op_michelson_optimization_search_space_multiplier;
      op_mode = f "mode" o1.op_mode o2.op_mode;
      op_noscamlib = f "noscamlib" o1.op_noscamlib o2.op_noscamlib;
      op_dump_iml = f "dump_iml" o1.op_dump_iml o2.op_dump_iml;
      op_protocol = f "protocol" o1.op_protocol o2.op_protocol;
      op_contract = f "contract" o1.op_contract o2.op_contract;
    }
  with Failure e ->
    Error.raisef
      Flags
      ~loc:Location.none
      "Cannot merge conflicting option of %s"
      e

let eval (k, v) =
  let open Result.Infix in
  let must_be_a_bool = Error "attribute type error: must be a bool" in
  match String.concat "." & Longident.flatten_exn k, v with
  | "iml_optimization", `Bool b -> Ok {none with op_iml_optimization = Some b}
  | "iml_optimization", _ -> must_be_a_bool
  | "noscamlib", `Bool b -> Ok {none with op_noscamlib = Some b}
  | "noscamlib", _ -> must_be_a_bool
  | "dump_iml", `Bool b -> Ok {none with op_dump_iml = Some b}
  | "dump_iml", _ -> must_be_a_bool
  | "protocol", `Constant (Parsetree.Pconst_float (s, None)) ->
      Protocol.parse s >>| fun v -> {none with op_protocol = Some v}
  | "protocol", _ -> begin
      match Protocol.parse "dummy" with
      | Error e -> Error e
      | Ok _ -> assert false
    end
  | "contract", `Constant (Parsetree.Pconst_string (s, _, None)) ->
      Ok {none with op_contract = Some s}
  | "contract", _ -> Error "illegal contract name"
  | n, _ -> Error (Printf.sprintf "Unknown attribute %s" n)

let of_attrs attrs =
  List.fold_left
    (fun op {Attribute.ident; loc; value} ->
      merge op
      & Result.at_Error (fun e -> Error.raisef Attribute ~loc "%s" e)
      & eval (ident, value))
    none
    attrs

let set_to_conf op = conf := unopt op

let opt = ref none

let with_opt xopt f =
  let org = !opt in
  opt := merge org xopt ;
  set_to_conf !opt ;
  let res = f () in
  opt := org ;
  res

let with_scaml_attrs attrs f = with_opt (of_attrs attrs) f

let get_opt () = !opt
