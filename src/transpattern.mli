(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019-2021  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

module Cnstr : sig
  type t =
    | Unit
    | Left
    | Right
    | Some
    | None
    | Cons
    | Nil
    | Bool of bool
    | Pair
    | Constant of Michelson.Constant.t

  val to_string : t -> string
end

module Pattern : sig
  type desc =
    | Var of Tools.Ident.t
    | Constr of Cnstr.t * t list
    | Wild
    | Alias of t * Tools.Ident.t * Tools.Location.t
    | Or of t * t

  and t = (desc, IML.Attr.ts) IML.with_loc_and_type

  val pp : Format.formatter -> t -> unit

  val vars : t -> IML.Idents.t
end

val mkppair : loc:Location.t -> Pattern.t -> Pattern.t -> Pattern.t
val mkpint : loc:Location.t -> int -> Pattern.t
val mkpleft : loc:Location.t -> Michelson.Type.t -> Pattern.t -> Pattern.t
val mkpright : loc:Location.t -> Michelson.Type.t -> Pattern.t -> Pattern.t

val pattern_top :
  Typedtree.pattern -> (Ident.t, IML.Attr.ts) IML.with_loc_and_type list

val pattern : Typedtree.pattern -> Pattern.t
