(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019-2021  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Asttypes
open Typedtree
open Tools
open Error

module M = Michelson
open M.Type
module C = M.Constant

open IML

module Cnstr = struct
  type t =
    | Unit
    | Left
    | Right
    | Some
    | None
    | Cons
    | Nil
    | Bool of bool
    | Pair
    | Constant of C.t
  (* XXX [Constant of C.t] cannot contain values of Unit, Left, Right...
     listed in the first line.  This is confusing. *)

  let to_string = function
    | Pair -> "(,)"
    | Left -> "Left"
    | Right -> "Right"
    | Some -> "Some"
    | None -> "None"
    | Cons -> "(::)"
    | Nil -> "[]"
    | Unit -> "()"
    | Bool true -> "true"
    | Bool false -> "false"
    | Constant c -> Format.sprintf "%a" C.pp c
end

(* pattern *)
module Pattern = struct
  type desc =
    | Var of Ident.t
    | Constr of Cnstr.t * t list
    | Wild
    | Alias of t * Ident.t * Location.t (* location of ident *)
    | Or of t * t

  and t = (desc, Attr.ts) with_loc_and_type

  let rec pp ppf pat =
    let open Format in
    match pat.desc with
    | Var i -> fprintf ppf "%s" (Ident.unique_name i)
    | Constr (c, []) -> fprintf ppf "%s" (Cnstr.to_string c)
    | Constr (c, ps) ->
        fprintf ppf "@[%s (%a)@]" (Cnstr.to_string c) (Format.list ",@ " pp) ps
    | Wild -> string ppf "_"
    | Alias (p, id, _) ->
        fprintf ppf "(@[%a as %s@])" pp p (Ident.unique_name id)
    | Or (p1, p2) -> fprintf ppf "(@[%a@ | %a@])" pp p1 pp p2

  let rec vars p =
    let open Idents in
    match p.desc with
    | Var id -> singleton {desc = id; typ = p.typ; loc = p.loc; attrs = p.attrs}
    | Constr (_, []) -> empty
    | Constr (_, p :: ps) -> List.fold_left union (vars p) (List.map vars ps)
    | Wild -> empty
    | Alias (p, id, _) ->
        add {desc = id; typ = p.typ; loc = p.loc; attrs = p.attrs} & vars p
    | Or (p1, p2) -> union (vars p1) (vars p2)
end

open Pattern

let mkppair ~loc p1 p2 =
  IML.mk
    ~loc
    (tyPair (None, p1.typ, None, p2.typ))
    (Constr (Cnstr.Pair, [p1; p2]))
let mkpint ~loc n =
  IML.mk
    ~loc
    tyInt
    (Constr (Cnstr.Constant (Michelson.Constant.Int (Z.of_int n)), []))
let mkpleft ~loc ty p =
  IML.mk ~loc (tyOr (None, p.typ, None, ty)) & Constr (Cnstr.Left, [p])
let mkpright ~loc ty p =
  IML.mk ~loc (tyOr (None, ty, None, p.typ)) & Constr (Cnstr.Right, [p])

let pattern_top {pat_desc; pat_loc = loc; pat_type = mltyp; pat_env = tyenv} =
  let typ =
    Result.at_Error (fun e ->
        Error.raisef
          Type_expr
          ~loc
          "@[This pattern has type %a.@ %a@]"
          Printtyp.type_expr
          mltyp
          Transtype.pp_error
          e)
    & Transtype.type_expr tyenv mltyp
  in
  let mk loc id typ = {loc; desc = id; typ; attrs = []} in
  let mk_dummy loc typ = mk loc (Ident.dummy "") typ in
  match pat_desc with
  | Tpat_var (id, {loc}) -> [mk loc id typ]
  | Tpat_alias ({pat_desc = Tpat_any; pat_loc = _}, id, _) ->
      (* We transform (_ as x) to x if _ and x have the same location, since
         OCaml compiler transforms let (x:t) = .. into let (_ as x : t) = .. .
         This avoids transforming a warning 27 into a 26.
      *)
      [mk loc id typ]
  | Tpat_any -> [mk_dummy loc typ]
  | Tpat_construct ({loc}, _, [], _) when typ.desc = TyUnit -> [mk_dummy loc typ]
  | Tpat_construct _ -> unsupported ~loc "variant pattern at toplevel let"
  | Tpat_alias _ -> unsupported ~loc "alias pattern at toplevel let"
  | Tpat_constant _ -> unsupported ~loc "constant pattern at toplevel let"
  | Tpat_tuple _ -> unsupported ~loc "tuple pattern at toplevel let"
  | Tpat_record _ -> unsupported ~loc "record pattern at toplevel let"
  | Tpat_variant _ -> unsupported ~loc "polymorphic variant pattern"
  | Tpat_array _ -> unsupported ~loc "array pattern"
  | Tpat_or _ -> unsupported ~loc "or pattern"
  | Tpat_lazy _ -> unsupported ~loc "lazy pattern"
(*
  | Tpat_exception _ -> unsupported ~loc "exception pattern"
*)

let rec pattern {pat_desc; pat_loc = loc; pat_type = mltyp; pat_env = tyenv} =
  let gloc = Location.ghost loc in
  let typ =
    Result.at_Error (fun e ->
        Error.raisef
          Type_expr
          ~loc
          "@[This pattern has type %a.@ %a@]"
          Printtyp.type_expr
          mltyp
          Transtype.pp_error
          e)
    & Transtype.type_expr tyenv mltyp
  in
  let mk desc = {loc; desc; typ; attrs = []} in
  match pat_desc with
  | Tpat_array _ -> unsupported ~loc "array pattern"
  | Tpat_lazy _ -> unsupported ~loc "lazy pattern"
  | Tpat_variant _ ->
      unsupported ~loc "polymorphic variant pattern"
      (*
  | Tpat_exception _ -> unsupported ~loc "exception pattern"
*)
  | Tpat_var (id, _) -> mk (Var id)
  | Tpat_alias ({pat_desc = Tpat_any; pat_loc = _}, id, _) ->
      (* We transform (_ as x) in x if _ and x have the same location.
         The compiler transforms (x:t) into (_ as x : t).
         This avoids transforming a warning 27 into a 26.
      *)
      mk (Var id)
  | Tpat_any -> mk Wild
  | Tpat_alias (p, id, {loc}) -> mk (Alias (pattern p, id, loc))
  | Tpat_tuple [p1; p2] -> mk (Constr (Cnstr.Pair, [pattern p1; pattern p2]))
  | Tpat_tuple ps ->
      Transtype.encode_by (mkppair ~loc:gloc) & List.map pattern ps
  | Tpat_constant (Const_string (s, _loc, None)) ->
      mk (Constr (Cnstr.Constant (C.String s), []))
  | Tpat_constant (Const_string (s, _loc, Some _)) ->
      (* quoted string *)
      mk (Constr (Cnstr.Constant (C.String s), []))
  | Tpat_constant _ ->
      (* Since the numbers are variant wrapped, i.e. Int 32 *)
      Error.unsupported
        ~loc
        "constant pattern of type %s"
        (Format.sprintf "%a" Printtyp.type_scheme mltyp)
  | Tpat_or (p1, p2, None) -> mk & Or (pattern p1, pattern p2)
  | Tpat_or (_, _, _) -> unsupported ~loc "or pattern with row"
  | Tpat_construct (_, cdesc, ps, _) -> begin
      (* XXX should check the module path *)
      match cdesc.cstr_name, typ.desc, ps with
      | "()", TyUnit, [] -> mk (Constr (Cnstr.Unit, []))
      | "Left", TyOr _, [p] -> mk (Constr (Cnstr.Left, [pattern p]))
      | "Right", TyOr _, [p] -> mk (Constr (Cnstr.Right, [pattern p]))
      | "Some", TyOption _, [p] -> mk (Constr (Cnstr.Some, [pattern p]))
      | "None", TyOption _, [] -> mk (Constr (Cnstr.None, []))
      | "::", TyList _, [p1; p2] ->
          mk (Constr (Cnstr.Cons, [pattern p1; pattern p2]))
      | "[]", TyList _, [] -> mk (Constr (Cnstr.Nil, []))
      | "true", TyBool, [] -> mk (Constr (Cnstr.Bool true, []))
      | "false", TyBool, [] -> mk (Constr (Cnstr.Bool false, []))
      | "Int", TyInt, [{pat_desc = Tpat_constant (Const_int n)}] ->
          mk & Constr (Cnstr.Constant (C.Int (Z.of_int n)), [])
      | "Int", TyInt, [_] ->
          Error.raisef Constant ~loc "Int can only take an integer constant"
      | "Nat", TyNat, [{pat_desc = Tpat_constant (Const_int n)}] ->
          if n < 0 then
            Error.raisef
              Constant
              ~loc
              "Nat can only take a positive integer constant" ;
          mk & Constr (Cnstr.Constant (C.Int (Z.of_int n)), [])
      | "Nat", TyNat, [_] ->
          Error.raisef Constant ~loc "Nat can only take an integer constant"
      | _, TyMutez, [_] ->
          Error.raisef Constant ~loc "tz constant cannot be used as a pattern"
      | ( "Key_hash",
          TyKeyHash,
          [{pat_desc = Tpat_constant (Const_string (s, _loc, _))}] ) ->
          mk & Constr (Cnstr.Constant (C.String s), [])
      | "Key_hash", TyKeyHash, [_] ->
          unsupported ~loc "Key_hash can only take a string constant"
      | ( "Address",
          TyAddress,
          [{pat_desc = Tpat_constant (Const_string (s, _loc, _))}] ) ->
          mk & Constr (Cnstr.Constant (C.String s), [])
      | "Address", TyAddress, [_] ->
          Error.raisef Constant ~loc "Address can only take a string constant"
      | ( "Timestamp",
          TyTimestamp,
          [{pat_desc = Tpat_constant (Const_string (s, _loc, _))}] ) -> begin
          match Transliteral.parse_timestamp ~loc s with
          | Error _e ->
              Error.raisef Constant ~loc "strange argument for Timestamp"
          | Ok t -> mk & Constr (Cnstr.Constant t, [])
        end
      | "Timestamp", TyTimestamp, [_] ->
          unsupported ~loc "Timestamp can only take a string constant"
      | ( "Bytes",
          TyBytes,
          [{pat_desc = Tpat_constant (Const_string (s, _loc, _)); pat_loc}] ) ->
        begin
          match Transliteral.parse_bytes ~loc:pat_loc s with
          | Error _e -> Error.raisef Constant ~loc "strange argument for Bytes"
          | Ok t -> mk & Constr (Cnstr.Constant t, [])
        end
      | "Bytes", TyBytes, [_] ->
          Error.raisef Constant ~loc "Bytes can only take a string constant"
      | _, _, _ -> begin
          match Types.get_desc mltyp with
          | Tconstr (p, _, _) -> begin
              match Env.find_type_descrs p tyenv with
              | Type_abstract when p = Predef.path_exn ->
                  Error.raisef
                    Type_expr
                    ~loc
                    "%a"
                    Transtype.pp_error
                    Exception_out_of_raise
              | Type_abstract ->
                  Error.raisef
                    Type_expr
                    ~loc
                    "Abstract data type %s is not supported in SCaml"
                    (Path.name p)
              | Type_record _ -> assert false (* record cannot come here *)
              | Type_variant (constrs, _) -> (
                  let consts_ty, non_consts_ty, entire_ty =
                    from_Ok & Transtype.variant_type tyenv mltyp p constrs
                  in
                  let rec find_constr i = function
                    | [] -> assert false
                    | n :: _ when n = cdesc.cstr_name -> i
                    | _ :: names -> find_constr (i + 1) names
                  in
                  let rec embed ty sides arg =
                    match ty.M.Type.desc, sides with
                    | _, [] -> arg
                    | TyOr (_, ty1, _, ty2), Binplace.Left :: sides ->
                        mkpleft ~loc:gloc ty2 (embed ty1 sides arg)
                    | TyOr (_, ty1, _, ty2), Right :: sides ->
                        mkpright ~loc:gloc ty1 (embed ty2 sides arg)
                    | _ -> assert false
                  in
                  match cdesc.cstr_arity, consts_ty, non_consts_ty with
                  | _, None, [] -> assert false
                  | 0, None, _ -> assert false
                  | 0, Some names, [] -> mkpint ~loc:gloc & find_constr 0 names
                  | 0, Some names, xs ->
                      let sides = Binplace.path 0 (List.length xs + 1) in
                      embed entire_ty sides & mkpint ~loc:gloc
                      & find_constr 0 names
                  | _, _, [] -> assert false
                  | _, _, cs ->
                      let names = List.map fst cs in
                      let i = find_constr 0 names in
                      let sides =
                        let shift = if consts_ty = None then 0 else 1 in
                        Binplace.path (i + shift) (List.length names + shift)
                      in
                      embed entire_ty sides
                      & Transtype.encode_by (mkppair ~loc:gloc)
                      & List.map pattern ps)
              | Type_open -> assert false
            end
          | _ -> unsupported ~loc "pattern %s" cdesc.cstr_name
        end
    end
  | Tpat_record (pfields, _) -> (
      (* fields are sorted, but omitted labels are not in it *)
      match Types.get_desc mltyp with
      | Tconstr (p, _, _) -> begin
          match Env.find_type_descrs p tyenv with
          | Type_record (labels, _) ->
              let labels =
                List.map
                  (fun label ->
                    let _, arg, res =
                      Ctype.instance_label false (* XXX ? *) label
                    in
                    Transtype.unify tyenv res mltyp ;
                    label.lbl_name, arg)
                  labels
              in
              let rec f labels pfields =
                match labels, pfields with
                | (n, _) :: labels, (_, plabel, p) :: pfields
                  when n = plabel.Types.lbl_name ->
                    pattern p :: f labels pfields
                | (n, typ) :: labels, _ ->
                    let typ =
                      Result.at_Error (fun e ->
                          Error.raisef
                            Type_expr
                            ~loc
                            "@[This pattern has a field %s with type %a.@ %a@]"
                            n
                            Printtyp.type_expr
                            typ
                            Transtype.pp_error
                            e)
                      & Transtype.type_expr tyenv typ
                    in
                    {loc; desc = Wild; typ; attrs = []} :: f labels pfields
                | [], [] -> []
                | [], _ -> assert false
              in
              Transtype.encode_by (mkppair ~loc:gloc) & f labels pfields
          | Type_abstract | Type_variant _ | Type_open -> assert false
        end
      | _ -> assert false)
