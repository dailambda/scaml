(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Tools

open IML
module Type = Michelson.Type

type use = Linear of int | Multiple

let find id env =
  let rec f rev_env = function
    | [] -> None, List.rev rev_env
    | (id', use) :: xs when id = id' -> begin
        match use with
        | Multiple -> Some Multiple, List.rev_append rev_env ((id', use) :: xs)
        | Linear n ->
            ( Some (Linear n),
              List.rev_append rev_env ((id', Linear (n - 1)) :: xs) )
      end
    | x :: xs -> f (x :: rev_env) xs
  in
  f [] env

let merge env1 env2 =
  let rec f = function
    | [], [] -> []
    | (id, _) :: _, (id', _) :: _ when id <> id' -> assert false
    | (id, Multiple) :: env1, (_id, Multiple) :: env2 ->
        (id, Multiple) :: f (env1, env2)
    | (id, Linear n1) :: env1, (_id, Linear n2) :: env2 ->
        (id, Linear (min n1 n2)) :: f (env1, env2)
    | _ -> assert false
  in
  f (env1, env2)

let f ?(permit_not_bound = false) env t =
  let rec f env t =
    match t.desc with
    | Dealloc _ -> assert false
    | Dealloc_then _ -> assert false
    | Var id when id = Translate.contract_self_id ->
        (* contract_self_id is not in [env] *)
        env
    | Var id -> begin
        match find id env with
        | None, _ when permit_not_bound -> env
        | None, _ ->
            Error.raisef Link
              ~loc:Location.none
              "Compiled identifier %a is not found in the linked modules. You may want to annotate the module defining it with [@@@SCaml]"
              Ident.pp id
        | Some Multiple, env -> env
        | Some (Linear n), _env when n <= 0 ->
            Error.raisef
              Type_expr (* XXX *)
              ~loc:t.loc
              "This variable is linear but used more than once"
              Ident.pp
              id
            (* id does not keep the original name, therefore
               no point to print it
            *)
        | Some (Linear _), env -> env
      end
    | Fun (pv, t) ->
        let use = if Type.is_linear pv.typ then Linear 1 else Multiple in
        let env = (pv.desc, use) :: env in
        List.tl & f env t
        (* check head is for pv *)
    | Const _ | Nil | IML_None | AssertFalse -> env
    | IML_Some t | Left t | Right t | Assert t -> f env t
    | Cons (t1, t2) | Pair (t1, t2) | IfThenElse (t1, t2, None) | Seq (t1, t2)
      ->
        let env = f env t1 in
        f env t2
    | IfThenElse (t1, t2, Some t3) ->
        let env1 = f env t1 in
        let env2 = f env1 t2 in
        let env3 = f env1 t3 in
        merge env2 env3
    | App (t, ts) ->
        let ts = t :: ts in
        List.fold_left (fun env t -> f env t) env ts
    | Prim (_, _, ts) | Set ts -> List.fold_left (fun env t -> f env t) env ts
    | Let (pv, t1, t2) ->
        let env = f env t1 in
        let use = if Type.is_linear pv.typ then Linear 1 else Multiple in
        let env = (pv.desc, use) :: env in
        List.tl & f env t2
        (* check the head is for pv *)
    | LetRec (pv, t1, t2) ->
        let use = if Type.is_linear pv.typ then Linear 1 else Multiple in
        let env = (pv.desc, use) :: env in
        let env = f env t1 in
        List.tl & f env t2
        (* check the head is for pv *)
    | Unpair (pv1, pv2, t1, t2) ->
        let env = f env t1 in
        let use1 = if Type.is_linear pv1.typ then Linear 1 else Multiple in
        let use2 = if Type.is_linear pv2.typ then Linear 1 else Multiple in
        let env = (pv1.desc, use1) :: (pv2.desc, use2) :: env in
        List.tl & List.tl & f env t2
        (* check the heads are for pv1 and pv2 *)
    | Switch_or (t, pv1, t1, pv2, t2) ->
        let env = f env t in
        let use1 = if Type.is_linear pv1.typ then Linear 1 else Multiple in
        let env1 = (pv1.desc, use1) :: env in
        let env1 = List.tl & f env1 t1 in
        (* check the head is for pv1 *)
        let use2 = if Type.is_linear pv2.typ then Linear 2 else Multiple in
        let env2 = (pv2.desc, use2) :: env in
        let env2 = List.tl & f env2 t2 in
        (* check the head is for pv2 *)
        merge env1 env2
    | Switch_cons (t, pv1, pv2, t1, t2) ->
        let env = f env t in
        let use1 = if Type.is_linear pv1.typ then Linear 1 else Multiple in
        let use2 = if Type.is_linear pv2.typ then Linear 1 else Multiple in
        let env1 = (pv1.desc, use1) :: (pv2.desc, use2) :: env in
        let env1 = List.tl & List.tl & f env1 t1 in
        (* check the head is for pv1 *)
        let env2 = f env t2 in
        merge env1 env2
    | Switch_none (t, t1, pv2, t2) ->
        let env = f env t in
        let env1 = f env t1 in
        let use2 = if Type.is_linear pv2.typ then Linear 1 else Multiple in
        let env2 = (pv2.desc, use2) :: env in
        let env2 = List.tl & f env2 t2 in
        (* check the head is for pv2 *)
        merge env1 env2
    | Contract_create (_, _, t1, t2, t3) ->
        let env = f env t1 in
        let env = f env t2 in
        f env t3
    | Map tts | BigMap tts ->
        List.fold_left
          (fun env (t1, t2) ->
            let env = f env t1 in
            f env t2)
          env
          tts
  in
  f env t
