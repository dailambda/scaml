(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Spotlib.Spot

type mode =
  | Compile
  | ConvertAll
  | ConvertSingleValue of string
  | ConvertSingleType of string
  | Revert of string

and t = {
  iml_optimization : bool;
  michelson_optimization : bool;
  michelson_optimization_do_dijkstra : bool;
  michelson_optimization_search_space_multiplier : float;
  mode : mode;
  noscamlib : bool;  (** do not add -I `opam config var prefix`/scaml/lib *)
  dump_iml : bool;
  protocol : Protocol.t;
  contract : string option;
}
[@@deriving conv {ocaml}]

val pp : Format.t -> t -> unit

val get_conf : unit -> t
val get_protocol : unit -> int * int

type opt = {
  op_iml_optimization : bool option;
  op_michelson_optimization : bool option;
  op_michelson_optimization_do_dijkstra : bool option;
  op_michelson_optimization_search_space_multiplier : float option;
  op_mode : mode option;
  op_noscamlib : bool option;
  op_dump_iml : bool option;
  op_protocol : Protocol.t option;
  op_contract : string option;
}
[@@deriving conv {ocaml}]

val pp_opt : Format.t -> opt -> unit

val none : opt
val merge : opt -> opt -> opt (* may fail *)

val unopt : opt -> t
val eval :
  Untyped.Longident.t
  * [> `Bool of bool | `Constant of Untyped.Parsetree.constant] ->
  (opt, string) result
val with_scaml_attrs : Attribute.t list -> (unit -> 'a) -> 'a
val get_opt : unit -> opt
val with_opt : opt -> (unit -> 'a) -> 'a
