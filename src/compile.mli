(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

val compiled_modules : (Path.t * Michelson.Module.t) list ref

module Make (_ : sig
  (** Big maps are possible to be compiled for the initial storage *)
  val allow_big_map : bool
end) : sig
  val structure : IML.t -> Michelson.Opcode.t list
end
