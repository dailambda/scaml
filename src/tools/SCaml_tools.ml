(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

include Spotlib.Spot

module Command = Spotlib.Command

module List = struct
  include List

  let rec mark_last = function
    | [] -> []
    | [x] -> [x, true]
    | x :: xs -> (x, false) :: mark_last xs

  let rec last = function [] -> None | [x] -> Some x | _ :: xs -> last xs
end

module Result = struct
  include Result
  open Infix

  let rec fold_leftM f acc = function
    | [] -> return acc
    | x :: xs -> f acc x >>= fun acc' -> fold_leftM f acc' xs

  module Syntax = struct
    let (let+) x f = fmap f x
    let (let*) = bind
  end

end

module Longident = struct
  include Longident

  let rec to_string = function
    | Lident s -> s
    | Ldot (lid, s) -> to_string lid ^ "." ^ s
    | Lapply (t1, t2) -> to_string t1 ^ "(" ^ to_string t2 ^ ")"
end

module Ident = struct
  include Ident

  let is_stdlib i = name i = "Stdlib" && persistent i
  let is_scaml i = name i = "SCaml" && persistent i

  let dummy n = Ident.create_local ("_" ^ n)

  let pp = print
end

module Path = struct
  include Path

  let rec xname = function
    | Pident id -> Ident.unique_name id
    | Pdot (t, s) -> Printf.sprintf "%s.%s" (xname t) s
    | Papply (t1, t2) -> xname t1 ^ "(" ^ xname t2 ^ ")"

  let xpp ppf p = Format.pp_print_string ppf (xname p)

  let is_stdlib = function
    | Pdot (Pident id, s) when Ident.is_stdlib id -> Some s
    | _ -> None

  let may_put_parens s =
    let ocaml_kwd_ops =
      ["or"; "lor"; "lxor"; "mod"; "land"; "lsl"; "lsr"; "asr"]
    in
    if List.mem s ocaml_kwd_ops then "(" ^ s ^ ")"
    else begin
      if s = "" then assert false ;
      match s.[0] with
      | 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' -> s
      | '*' -> "( " ^ s ^ " )"
       | _ -> "(" ^ s ^ ")"
    end

  let rec is_scaml = function
    | Pdot (Pident id, s) when Ident.is_scaml id -> Some (may_put_parens s)
    | Pdot (p, s) -> begin
        match is_scaml p with
        | None -> None
        | Some m -> Some (m ^ "." ^ may_put_parens s)
      end
    | _ -> None

  let is_scaml_dot n = function
    | Pdot (Pident id, s) when Ident.is_scaml id -> s = n
    | _ -> false
end

module Location = struct
  include Location
  let ghost t = {t with loc_ghost = true}
end

let with_time f =
  let t1 = Unix.gettimeofday () in
  let res = f () in
  let t2 = Unix.gettimeofday () in
  res, t2 -. t1

module Debug : sig

  (** [register name] creates a debug switch. A switch is a string list
      to express subgroups *)
  val register : string list -> bool ref

  (** [set_by_pattern "p1:p2:...:pn"] turns on the debug switches match with
      one of pi.  pi is a list of debug group names separated by '.', ex.
      "scaml.pmatch".

      Group name can be '*' which matches with any group.
  *)
  val set_by_pattern : string -> unit

  (** Check GGG_NNN_DEBUG env vars and set switches ggg.nnn *)
  val set_by_env : unit -> unit

  val registered : unit -> (string list * bool ref) list
end = struct
  let tbl = Hashtbl.create 101

  let register names =
    let b = ref false in
    let rec check xs = function
      | [] -> ()
      | y::ys ->
          let xs = y::xs in
          let rxs = List.rev xs in
          if Hashtbl.mem tbl rxs then
            failwithf "debug switch %s is a prefix of another switch"
              (String.concat "." rxs);
          check xs ys
    in
    check [] names;
    Hashtbl.add tbl names b;
    b

  let filter ss ns =
    let rec f ss ns =
      match ss, ns with
      | [], _ -> true
      | ["*"], [] -> false
      | ["*"], _::_ -> true
      | "*"::ss, _::ns -> f ss ns
      | s::ss, n::ns when s = n -> f ss ns
      | _ -> false
    in
    f ss ns

  let set_by_pattern s =
    let ss = String.split_on_char ':' s in
    List.iter (fun s ->
        let ss = String.split_on_char '.' s in
        let hit = ref false in
        Hashtbl.iter (fun ns b ->
            if filter ss ns then begin
              b := true;
              hit := true
            end) tbl;
        if not !hit then
          failwithf "debug switch pattern %s matches with none" s) ss

  let set_by_env () =
    Hashtbl.iter (fun ns b ->
        let v =
          Printf.sprintf "%s_DEBUG"
            (String.concat "_"
               (List.map String.uppercase_ascii ns))
        in
        b := !b || Sys.getenv_opt v <> None) tbl

  let registered () =
    List.sort (fun (k1,_) (k2,_) -> compare k1 k2)
      @@ Hashtbl.fold (fun k v acc -> (k,v)::acc) tbl []
end
