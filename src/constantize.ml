(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Tools

module C = Michelson.Constant

open IML

(* do_big_map = false, non pushable exps cannot be constantized.

   [PUSH ty v] is invalid if [ty] is not pushable.
*)
let f ~do_big_map ~compile exp =
  let rec f exp =
    let is_pushable = do_big_map || Michelson.Type.is_pushable exp.typ in
    let const c = Some c, {exp with desc = Const c} in
    let non_const desc = None, {exp with desc} in
    let f' exp = snd & f exp in
    let id = None, exp in
    match exp.desc with
    | Var _ -> id
    | AssertFalse -> id
    | Const c -> const c
    | Nil ->
        if is_pushable then const (List [])
        else non_const Nil
    | IML_None ->
        if is_pushable then const & Option None
        else non_const & IML_None
    | IML_Some e -> begin
        match f e with
        | Some c, _ when is_pushable -> const & C.Option (Some c)
        | _, e -> non_const & IML_Some e
      end
    | Left t -> begin
        match f t with
        | Some c, _ when is_pushable -> const & Left c
        | _, e -> non_const & Left e
      end
    | Right t -> begin
        match f t with
        | Some c, _ when is_pushable -> const & Right c
        | _, e -> non_const & Right e
      end
    | Cons (h, t) -> begin
        match f h, f t with
        | (Some h, _), (Some (C.List t), _) when is_pushable -> const & C.List (h :: t)
        | (_, eh), (_, et) -> non_const & Cons (eh, et)
      end
    | Pair (l, r) -> begin
        match f l, f r with
        | (Some l, _), (Some r, _) when is_pushable -> const & Pair (l, r)
        | (_, l), (_, r) -> non_const & Pair (l, r)
      end
    | Fun (pv, t) -> begin
        match compile with
        | None -> non_const & Fun (pv, f' t)
        | Some compile ->
            if IML.Idents.is_empty (IML.freevars exp) then
              const & Code (compile exp)
            else non_const & Fun (pv, f' t)
      end
    | IfThenElse (t1, t2, None) -> non_const & IfThenElse (f' t1, f' t2, None)
    | IfThenElse (t1, t2, Some t3) ->
        non_const & IfThenElse (f' t1, f' t2, Some (f' t3))
    | App (t, ts) -> non_const & App (f' t, List.map (fun t -> f' t) ts)
    | Prim (s, ty, ts) -> non_const & Prim (s, ty, List.map (fun t -> f' t) ts)
    | Let (pv, t1, t2) -> non_const & Let (pv, f' t1, f' t2)
    | LetRec (pv, t1, t2) -> non_const & LetRec (pv, f' t1, f' t2)
    | Unpair (pv1, pv2, t1, t2) -> non_const & Unpair (pv1, pv2, f' t1, f' t2)
    | Assert t -> non_const & Assert (f' t)
    | Seq (t1, t2) -> non_const & Seq (f' t1, f' t2)
    | Contract_create (cs, l, t1, t2, t3) ->
        non_const & Contract_create (cs, l, f' t1, f' t2, f' t3)
    | Set ts ->
        let ts = List.map f ts in
        let check cs =
          let cs = List.sort compare cs in
          (* XXX OCaml's compare *)
          let rec check_uniq = function
            | [] | [_] -> ()
            | c1 :: c2 :: _ when c1 = c2 ->
                (* XXX OCaml's compare *)
                Error.raisef
                  Constant
                  ~loc:exp.loc
                  "Set literal contains duplicated value %a"
                  C.pp
                  c1
            | _ :: xs -> check_uniq xs
          in
          check_uniq cs ;
          cs
        in
        let rec loop (stc, ste) ts =
          match stc, ts with
          | None, [] -> non_const & Set (List.rev ste)
          | Some cs, [] ->
              if is_pushable then
                const & C.Set (check & List.rev cs)
              else
                non_const & Set (List.rev ste)
          | None, (_, t) :: ts -> loop (None, t :: ste) ts
          | Some _, (None, t) :: ts -> loop (None, t :: ste) ts
          | Some cs, (Some c, t) :: ts -> loop (Some (c :: cs), t :: ste) ts
        in
        loop (Some [], []) ts
    | Map tts ->
        let tts =
          List.map
            (fun (t1, t2) ->
               match f t1, f t2 with
               | (Some c1, t1), (Some c2, t2) -> Some (c1, c2), (t1, t2)
               | (_, t1), (_, t2) -> None, (t1, t2))
            tts
        in
        let check kvs =
          let kvs = List.sort (fun (k1, _) (k2, _) -> compare k1 k2) kvs in
          (* XXX OCaml's compare *)
          let rec check_uniq = function
            | [] | [_] -> ()
            | (c1, _) :: (c2, _) :: _ when c1 = c2 ->
                (* XXX OCaml's compare *)
                Error.raisef
                  Constant
                  ~loc:exp.loc
                  "Map literal contains duplicated key %a"
                  C.pp
                  c1
            | _ :: xs -> check_uniq xs
          in
          check_uniq kvs ;
          kvs
        in
        let rec loop (stc, ste) tts =
          match stc, tts with
          | None, [] -> non_const & Map (List.rev ste)
          | Some ccs, [] ->
              if is_pushable then
                const & C.Map (check & List.rev ccs)
              else
                non_const & Map (List.rev ste)
          | None, (_, tt) :: tts -> loop (None, tt :: ste) tts
          | Some _, (None, tt) :: tts -> loop (None, tt :: ste) tts
          | Some ccs, (Some cc, tt) :: tts ->
              loop (Some (cc :: ccs), tt :: ste) tts
        in
        loop (Some [], []) tts
    | BigMap tts when do_big_map ->
        let tts =
          List.map
            (fun (t1, t2) ->
               match f t1, f t2 with
               | (Some c1, t1), (Some c2, t2) -> Some (c1, c2), (t1, t2)
               | (_, t1), (_, t2) -> None, (t1, t2))
            tts
        in
        let check kvs =
          let kvs = List.sort (fun (k1, _) (k2, _) -> compare k1 k2) kvs in
          (* XXX OCaml's compare *)
          let rec check_uniq = function
            | [] | [_] -> ()
            | (c1, _) :: (c2, _) :: _ when c1 = c2 ->
                (* XXX OCaml's compare *)
                Error.raisef
                  Constant
                  ~loc:exp.loc
                  "BigMap literal contains duplicated key %a"
                  C.pp
                  c1
            | _ :: xs -> check_uniq xs
          in
          check_uniq kvs ;
          kvs
        in
        let rec loop (stc, ste) tts =
          match stc, tts with
          | None, [] -> non_const & Map (List.rev ste)
          | Some ccs, [] -> const & C.Map (check & List.rev ccs)
          | None, (_, tt) :: tts -> loop (None, tt :: ste) tts
          | Some _, (None, tt) :: tts -> loop (None, tt :: ste) tts
          | Some ccs, (Some cc, tt) :: tts ->
              loop (Some (cc :: ccs), tt :: ste) tts
        in
        loop (Some [], []) tts
    | Switch_or (t1, pv1, t2, pv2, t3) ->
        non_const & Switch_or (f' t1, pv1, f' t2, pv2, f' t3)
    | Switch_cons (t1, pv1, pv2, t2, t3) ->
        non_const & Switch_cons (f' t1, pv1, pv2, f' t2, f' t3)
    | Switch_none (t1, t2, pv, t3) ->
        non_const & Switch_none (f' t1, f' t2, pv, f' t3)
    | BigMap tts ->
        (* XXX sort and uniq *)
        non_const & BigMap (List.map (fun (t1, t2) -> f' t1, f' t2) tts)
    | Dealloc (t, id) -> non_const & Dealloc (f' t, id)
    | Dealloc_then (id, t) -> non_const & Dealloc_then (id, f' t)
  in
  match f exp with None, t -> t | Some c, _ -> {exp with desc = Const c}
