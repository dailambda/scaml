(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019-2021  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

module Raw_view : sig
  (* The actual compiled code of SCaml is NOT here but in [Translate.res] *)
  type t = {
    path : Path.t;
    (* Used to query value's type in Env.t *)
    ident : Ident.t;
    local_ident : Ident.t;
    name : string;
    (* [@view name="XXX"] otherwise Ident.name ident *)
    pat : Typedtree.pattern;
  }
end

module View : sig
  (* The actual compiled code of SCaml is NOT here but in [Translate.res] *)
  type t = {
    name : string;
    ident : Ident.t;
    ty_param : Michelson.Type.t;
    ty_return : Michelson.Type.t;
  }

  val pp : Format.formatter -> t -> unit
end

module Entry_tree : sig
  (* The actual compiled code of SCaml is NOT here but in [Translate.res] *)
  type t = {
    id_param : Ident.t;
    ty_param : Michelson.Type.t;
    id_storage : Ident.t;
    ty_storage : Michelson.Type.t;
    branch_code : IML.t (** code to select one of the entry points *);
  }
end

(* The actual compiled code of SCaml is NOT here but in [Translate.res] *)
type t = {
  name : string;  (** [@@@SCaml contract=<name>] *)
  global_path : Path.t;
  loc : Location.t;  (** of the structure *)
  oty_contract : Types.type_expr;  (** contract type in ocaml *)
  ty_storage : Michelson.Type.t;
  entry_tree : Entry_tree.t;  (** pattern match to call the entries *)
  views : View.t list; (** views defined *)
}

type contract = t

val pp : Format.formatter -> contract -> unit

val implementation :
  string (* source file name "dir/foobar.ml" *) ->
  string (* outputprefix "dir/foobar" *) ->
  string (* module name "Foobar" *) ->
  Typedtree.structure ->
  contract list * Translate.res

(** Expand the bindings in the global_entry *)
val link : IML.t (* global entry *) -> IML.t list (* definitions *) -> IML.t

(** [put_global_abstraction' ~param ~storage body] builds:

    fun pat_param_storage ->
      let (param,storage) = pat_param_storage in
      body
*)
val put_global_abstraction' :
  param:Ident.t * Michelson.Type.t ->
  storage:Ident.t * Michelson.Type.t ->
  IML.t ->
  IML.t

(** Same as the above but takes [Entry_tree.t] for the id+types of param and storage *)
val put_global_abstraction : Entry_tree.t -> IML.t -> IML.t

(** Make one Binplaced tree of defined values *)
val connect : (IML.PatVar.t * IML.t) list (* definitions *) -> IML.t

(** Conversion mode *)
val convert :
  Typedtree.structure ->
  [> `Type of Ident.t * Michelson.Type.t | `Value of Ident.t option * IML.t]
  list

(** Contract.self's type must be equal to the contract type *)
val check_self : contract -> IML.t -> unit
