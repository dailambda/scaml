(** Make IML constant expressions to Michelson constants *)
val f :
  do_big_map:bool ->
  compile:(IML.t -> Michelson.Opcode.t list) option ->
  IML.t ->
  IML.t
