(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019-2021  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Tools

type error =
  | Type_variable of Types.type_expr
  | Unsupported_type of Types.type_expr
  | Unsupported_data_type of Path.t
  | Invalid_michelson of Michelson.Type.t * string
  | Exception_out_of_raise
  | GADT of Path.t
  | Not_memo_size of Types.type_expr

val pp_error : Spotlib.Spot.Format.formatter -> error -> unit

val encode_by : ('a -> 'a -> 'a) -> 'a list -> 'a

val unify : Env.t -> Types.type_expr -> Types.type_expr -> unit

(** Sapling memo size *)
val memo_size : Types.type_expr -> (int, error) result

val type_expr : Env.t -> Types.type_expr -> (Michelson.Type.t, error) Result.t

val record_type :
  Env.t ->
  Types.type_expr ->
  Path.t ->
  Types.label_description list ->
  (Michelson.Type.t, error) Result.t

val variant_type :
  Env.t ->
  Types.type_expr ->
  Path.t ->
  Types.constructor_description list ->
  ( string list option * (string * Michelson.Type.t) list * Michelson.Type.t,
    error )
  Result.t
