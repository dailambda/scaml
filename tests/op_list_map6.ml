(*
STORAGE=(BigMap [(Int 1, Nat 2)] : (int, nat) big_map)
INPUT=[Int 1]
*)
open SCaml

let[@entry] balance_of ks storage =
  let balances : nat option list =
    List.map (fun (k : int) -> BigMap.get k storage) ks
  in
  assert (List.length balances <> Nat 0) ;
  [], storage
