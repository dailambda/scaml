(* INPUT=(None : [`n8] sapling_transaction option)
*)
open SCaml

let[@entry] main transaction_opt () =
  match transaction_opt with
  | None -> [], ()
  | Some transaction -> (
      match Sapling.verify_update transaction (Sapling.empty_state `n8) with
      | None -> [], ()
      | Some (_ : (bytes * (int * _ Sapling.state))) -> [], ())
