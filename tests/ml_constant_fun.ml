(*
INPUT=(fun x -> x + Int 1)
STORAGE= Int 1
*)
open SCaml
let[@entry] main p s =
  ( [],
    (assert (p s = Int 2) ;
     p s) )
