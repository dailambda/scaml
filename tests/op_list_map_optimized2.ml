open SCaml
let[@entry] main () () =
  ( [],
    assert (
      List.fold_left
        ( + )
        (Int 0)
        (List.map (fun x -> x + Int 1) [Int 1; Int 2; Int 3])
      = Int 9) )
