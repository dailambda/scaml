(* STORAGE=Bytes "0x00"
*)
open SCaml

module Dummy = struct
  type storage = unit
  let[@entry] main () () = [], ()
end

let[@entry] main () last =
  (* Using Contract.create to get a pseudo random address *)
  let _, address = Contract.create (module Dummy) None (Tz 0.0) () in
  let level = Global.get_level () in
  [], Crypto.blake2b (Obj.pack (last, level, address))
