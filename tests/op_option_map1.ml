[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main x y =
  [],
  assert (Option.map (fun x -> x + Int 1) (Some (Int 1)) = Some (Int 2)
          && Option.map (fun x -> x + Int 1) None = None
         )
