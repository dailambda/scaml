(*
INPUT= Int 2
STORAGE= Int 3
*)
open SCaml
let[@entry] main x y =
  if x > y then failwith x ;
  [], Int 1
