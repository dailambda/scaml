(*
STORAGE= Int 0
*)
open SCaml

let[@entry] f p s =
  [], List.fold_left' (fun (st, t) -> st + t + s) s [Int 1; Int 2; Int 3]
