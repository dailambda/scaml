(*
   STORAGE= Int 1
*)
open SCaml
let[@entry] main param storage =
  ( [],
    let t = Int 1, Int 2 in
    let x, y = t in
    x + y )
