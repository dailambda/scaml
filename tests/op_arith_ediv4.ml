[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main () () =
  [],
  
  begin
    assert (
      match ediv_rem_int (Int 10) (Int 3) with
      | None -> false
      | Some i_n -> fst i_n = Int 3 && snd i_n = Nat 1 );

    assert (
      match ediv_rem_int (Int (-10)) (Int 3) with
      | None -> false
      | Some i_n -> fst i_n = Int (-4) && snd i_n = Nat 2 );

    assert (
      match ediv_rem_nat (Nat 10) (Nat 3) with
      | None -> false
      | Some i_n -> fst i_n = Nat 3 && snd i_n = Nat 1 );

    assert (
      match ediv_rem_tz (Tz 10.) (Tz 3.) with
      | None -> false
      | Some i_n -> fst i_n = Nat 3 && snd i_n = Tz 1. );

    assert (
      match ediv_rem_tz_nat (Tz 10.) (Nat 3) with
      | None -> false
      | Some i_n -> fst i_n = Tz 3.333333 && snd i_n = Tz 0.000001 );

    assert (Int 10 // Int 3 = Int 3);
    assert (Int (-10) // Int 3 = Int (-4));
    assert (Nat 10 /^ Nat 3 = Nat 3);
    assert (Tz 10. /$ Tz 3. = Nat 3);
    assert (Tz 10. /$^ Nat 3 = Tz 3.333333);

    assert (Int 10 %% Int 3 = Nat 1);
    assert (Int (-10) %% Int 3 = Nat 2);
    assert (Nat 10 %^ Nat 3 = Nat 1);
    assert (Tz 10. %$ Tz 3. = Tz 1.);
    assert (Tz 10. %$^ Nat 3 = Tz 0.000001)
  end
