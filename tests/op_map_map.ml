[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main x y =
  let f x y = x + y in
  ( [],
    assert (
      Map.length
        (Map.map (if true then f else f) (Map [Int 1, Int 1; Int 2, Int 2]))
      = Nat 2) )
