open SCaml

let[@entry] main () () =
  let t10 = Ticket.create () (Nat 10) in
  let t3 = Ticket.create () (Nat 3) in
  match Ticket.join t10 t3 with
  | None -> assert false
  | Some t13 -> (
      match Ticket.read t13 with
      | (_, _, Nat 13), _ -> [], ()
      | _ -> assert false)
