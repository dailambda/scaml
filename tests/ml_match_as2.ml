(*
   STORAGE= Int 2
*)

[@@@SCaml iml_optimization = false]

open SCaml

(* Had a bug of (v as v') *)
let[@entry] main param (storage as x) =
  ( [],
    (assert (storage + x = Int 4) ;
     storage - x) )
