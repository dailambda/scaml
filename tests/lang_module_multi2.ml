(*
   TZ=lang_module_multi2.M.tz lang_module_multi2.M.N.tz
*)
open SCaml

module M = struct
  let[@entry] main () () = [], ()

  module N = struct
    let[@entry] main' () () = [], ()
  end
end
