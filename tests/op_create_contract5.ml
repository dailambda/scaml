open SCaml

module M = struct
  (* lousy but we have to explicitly define type storage *)
  type storage = unit
  let[@entry] main () () = [], ()
end

let[@entry] main () () =
  let op, ad =
    Contract.create ((module M) : _ Contract.module_) None (Tz 0.0) ()
  in
  [op], ()
