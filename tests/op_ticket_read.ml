(*
STORAGE= (None : (address * nat * unit ticket) option)
*)

open SCaml

(* XXX need to check

   - ticket value in the initial storage:
     (Pair "KT1BE..." Unit 10)
*)
let[@entry] main () _ =
  let t = Ticket.create () (Nat 10) in
  let (adrs, (), nat), t = Ticket.read t in
  [], Some (adrs, nat, t)
