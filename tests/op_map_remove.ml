[@@@SCaml iml_optimization = false]
open SCaml
open Map
let[@entry] main x y =
  [], assert (mem "d" (remove "b" (Map ["b", Int 1; "d", Int 2])))
