(*
STORAGE= (None : unit ticket option)
*)

open SCaml

let[@entry] main () (s : unit ticket option) = [], s
