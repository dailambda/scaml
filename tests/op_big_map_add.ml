[@@@SCaml iml_optimization = false]
open SCaml
open BigMap
let[@entry] main x y =
  [], assert (mem "a" (add "b" (Int 4) (add "a" (Int 3) BigMap.empty)))
