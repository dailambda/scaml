open SCaml

(*
STORAGE= Int 2
*)
let[@entry] main () x =
  [], List.fold_left (fun acc x -> acc + x) (Int 0) [Int 1]
