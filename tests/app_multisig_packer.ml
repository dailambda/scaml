(* MUST_FAIL *)
open SCaml

type parameter = {payload : payload; sigs : signature option list}

and payload = {counter : nat; action : action}

and action =
  | Transfer of transfer
  | Delegate of key_hash option
  | Change_keys of change_keys

and transfer = {amount : tz; dest : key_hash}

and change_keys = {threshold : nat; keys : key list}

let[@entry] main () () =
  (* pair the payload with the current contract address, to ensure signatures
     can't be replayed accross different contracts if a key is reused. *)
  let payload =
    {
      counter = Nat 42;
      action =
        Transfer
          {
            amount = Tz 1.0;
            dest = Key_hash "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx";
          };
    }
  in
  let signature_target =
    ( payload,
      Address "KT1BEqzn5Wx8uJrZNvuS9DVHmLvG9td3fDLi",
      Chain_id "NetXdQprcVkpaWU" )
  in
  failwith (signature_target, Obj.pack signature_target)
