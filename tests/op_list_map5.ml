(*
STORAGE=BigMap [(), ()]
INPUT=[()]
*)
open SCaml

let[@entry] balance_of ks storage =
  let balances = List.map (fun k -> BigMap.get k storage) ks in
  assert (List.length balances <> Nat 0) ;
  [], storage
