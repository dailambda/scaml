(* We can pass big map literal from the outside of the chain *)
(*
INPUT=(None : (int, int) big_map contract option)
STORAGE=(BigMap [] : (int, int) big_map)
*)
open SCaml

let[@entry] main (c : (int, int) big_map contract option)
    (bm : (int, int) big_map) =
  match c with
  | None -> [], bm
  | Some c ->
      let op = Operation.transfer_tokens bm (Tz 0.) c in
      [op], bm
