(*
  INPUT=Int 3
  STORAGE=(BigMap [] : (int, bool) BigMap.t)
*)
open SCaml

(* We had a bug to constantize non pushable values *)
type world_t = (int, bool) BigMap.t

let rec f (st : world_t * bool * bool) = function
  | Int 0 -> ()
  | n -> f st (n - Int 1)

let[@entry] submit i (world : world_t) =
  f (world, true, true) i;
  [], world
