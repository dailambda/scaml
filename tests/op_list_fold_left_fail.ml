(* REJECT *)
open SCaml

let[@entry] main (ks, b) m =
  let f acc k = acc +^ if BigMap.mem k m then Nat 1 else Nat 0 in
  assert (List.fold_left (if b then f else f) (Nat 0) ks <> Nat 0) ;
  [], m
