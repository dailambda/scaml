open SCaml
let[@entry] main x y =
  ( [],
    assert (Set.length (Set.remove (Int 3) (Set [Int 1; Int 2; Int 3])) = Nat 2)
  )
