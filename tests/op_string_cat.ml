[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main x y =
  ( [],
    assert (
      "hello world" = String.cat "hello " "world"
      && "hello world" = "hello " ^ "world") )
