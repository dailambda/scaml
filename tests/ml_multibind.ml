(* STORAGE= Int 0
*)

open SCaml

let x, y = Int 1, Int 2

let[@entry] main () _ = [], x + y
