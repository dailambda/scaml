[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main () () =
  ( List.fold_left
      (fun opts c -> Operation.transfer_tokens () (Tz 1.0) c :: opts)
      []
      [],
    () )
