(*
INPUT=((fun x -> x + Int 1), Int 1)
*)
open SCaml
let[@entry] main (p : (int -> int) * int) () =
  let f, x = p in
  [], assert (f x = Int 2)
