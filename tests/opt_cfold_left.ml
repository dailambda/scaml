(* STORAGE=(Right (Int 2) : (int, int) sum)
*)
open SCaml

(* done by [inline], not by [cfold] *)
let[@entry] main () _ =
  let x = Int 1 in
  let y : (int, int) sum = Left x in
  [], y
