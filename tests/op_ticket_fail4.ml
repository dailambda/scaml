(* REJECT *)
(*
STORAGE= ([] : unit ticket list)
*)

open SCaml

let[@entry] main () (_ : unit ticket list) =
  let t = Ticket.create () (Nat 10) in
  let x = Int 1, t in
  [], [snd x; snd x]
