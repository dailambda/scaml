open SCaml
let[@entry] main () () =
  ( [],
    let f x = x = Int 1 in
    assert (List.length (List.map f [Int 1; Int 2; Int 3]) = Nat 3) )
