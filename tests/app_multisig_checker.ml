open SCaml

type parameter = {payload : payload; sigs : signature option list}

and payload = {counter : nat; action : action}

and action =
  | Transfer of transfer
  | Delegate of key_hash option
  | Change_keys of change_keys

and transfer = {amount : tz; dest : key_hash}

and change_keys = {threshold : nat; keys : key list}

let[@entry] main () () =
  let payload =
    {
      counter = Nat 42;
      action =
        Transfer
          {
            amount = Tz 1.0;
            dest = Key_hash "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx";
          };
    }
  in
  let signature_target =
    Obj.pack
      ( payload,
        Address "KT1BEqzn5Wx8uJrZNvuS9DVHmLvG9td3fDLi",
        Chain_id "NetXdQprcVkpaWU" )
  in
  assert (
    signature_target
    = Bytes
        "0x0507070707002a050507070080897a0a000000150002298c03ed7d454a101eb7022bc95f7e5f41ac7807070a00000016011d23c1d3d2f8a4ea5e8784b8f7ecf2ad304c0fe6000a000000047a06a770") ;
  let key = Key "edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav" in
  let sig_ =
    Signature
      "edsigtwF7xSvjmubGXrQUNdbvtBYBn1tLHjQ6kxBi6n6TMGHzJ2FoRRitQAAaX4CbrEHQkkvxQLp3sSKWgZSyEHgT2Gz7wqh5yJ"
  in
  assert (Crypto.check_signature key sig_ signature_target) ;
  [], ()
