open SCaml

let[@entry] main () () =
  assert (mutez_of_nat (Nat 1) = Tz 0.000001) ;
  [], ()
