[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main x y =
  [], assert (-1i + 2i = 1i && 1n +^ 2n = 3n)
