(* INPUT=Int 0
*)
[@@@SCaml iml_optimization = false]
open SCaml

let[@entry] main x () = if not (x <> Int 0) then [], () else assert false
