(*
INPUT=true
STORAGE= (None : unit ticket option)
*)
[@@@SCaml iml_optimization = false]
open SCaml

(* XXX need to check

   - ticket value in the initial storage:
     (Pair "KT1BE..." Unit 10)
*)
let[@entry] main p _ =
  let t = Ticket.create () (Nat 10) in
  let t = if p then Ticket.create () (Nat 3) else snd (Ticket.read t) in
  [], Some t
