(* MUST_FAIL *)
open SCaml
let[@entry] main x y = [], assert (nat_of_int (Int (-1)) = Nat 3)
