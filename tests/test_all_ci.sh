#!/bin/sh

set -e

for i in *.ml
do
    errors=""
    logdir=_build/$i
    if [ ! -d $logdir ]; then
	mkdir -p $logdir
    fi
    if ./test.sh $i > $logdir/log 2>&1; then
	:
    else
	echo Error $i
	cat $logdir/log
	errors="$errors $i"
    fi
done

if [ -n "$errors" ]; then
    echo "Error: $errors"
    exit 2
else
    echo "All tests pass"
fi
