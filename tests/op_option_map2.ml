[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main x y =
  [],
  let incr x = x + Int 1 in
  assert (Option.map incr (Some (Int 1)) = Some (Int 2)
          && Option.map incr None = None
         )
