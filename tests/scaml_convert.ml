(* STORAGE= Scaml_convert.(Foo { u= { x= Int 1; y= Nat 2 } }, Foo { u= { x= Int 1; y= Nat 2 } })
*)

open SCaml

type u = {x : int; y : nat}
type t = Foo of {u : u}

let[@entry] main () ((u1, u2) : u * u) = [], (u1, u2)
