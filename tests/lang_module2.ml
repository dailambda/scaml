(*
  TZ= lang_module2.Y.tz
*)
open SCaml

module X = struct
  let x = Int 1
end

module Y = struct
  let[@entry] default () () = [], assert (X.x = Int 1)
end
