(*
STORAGE=(None : timestamp option)
*)
[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main x _ =
  ( [],
    (assert (Global.get_now () = Global.get_now ()) ;
     Some (Global.get_now ())) )
