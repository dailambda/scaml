(* REJECT *)
(*
STORAGE= ([] : unit ticket list)
*)

open SCaml

let[@entry] main () (_ : unit ticket list) =
  let t = Ticket.create () (Nat 10) in
  let f (t : unit ticket) = [t; t] in
  [], f t
