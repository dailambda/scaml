(*
tezos-client run script uses the sender "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"

STORAGE=BigMap [Address "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx", Nat 10; Address "tz1a9GVLbdYxbJdJ3dTbP8H4g3A9s7JkoNK4", Nat 5]

INPUT=([Address "tz1a9GVLbdYxbJdJ3dTbP8H4g3A9s7JkoNK4", Nat 1;  Address "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx", Nat 5] : (address * nat) list)
*)
open SCaml

type storage = (address, nat) big_map

exception FA2_INSUFFICIENT_BALANCE

let do_tx from accounts ((to_, amount) : address * nat) =
  let from_amount = BigMap.get_with_default from accounts (Nat 0) in
  let from_amount' = from_amount -^ amount in
  if from_amount' < Int 0 then raise FA2_INSUFFICIENT_BALANCE ;
  let from_amount' = abs from_amount' in
  let accounts = BigMap.add from from_amount' accounts in
  let to_amount = BigMap.get_with_default to_ accounts (Nat 0) in
  let to_amount' = to_amount +^ amount in
  BigMap.add to_ to_amount' accounts

(* Bad version.  It works as same as [do_tx] when from <> to_ *)
let wrong_do_tx from accounts ((to_, amount) : address * nat) =
  let from_amount = BigMap.get_with_default from accounts (Nat 0) in
  let from_amount' = from_amount -^ amount in
  if from_amount' < Int 0 then raise FA2_INSUFFICIENT_BALANCE ;
  let from_amount' = abs from_amount' in
  let to_amount = BigMap.get_with_default to_ accounts (Nat 0) in
  let to_amount' = to_amount +^ amount in
  BigMap.add to_ to_amount' @@ BigMap.add from from_amount' accounts

(* execute transfers *)
let[@entry] transfer txs accounts =
  [], List.fold_left (do_tx (Global.get_sender ())) accounts txs
